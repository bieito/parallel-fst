#include "Profiler/Profiler.h"

Profiler::time_point Profiler::now() {
    return std::chrono::high_resolution_clock::now();
}

Profiler::time_point Profiler::start() {
    last = Profiler::now();

    return last;
}

Profiler::duration Profiler::stop() {
    return Profiler::from(this->last);
}

Profiler::duration Profiler::lap() {
    duration d = this->stop();
    this->start();

    return d;
}

Profiler::duration Profiler::from(Profiler::time_point timepoint) {
    return std::chrono::duration_cast<std::chrono::microseconds>(Profiler::now() - timepoint).count();
}

double Profiler::secondsFrom(Profiler::time_point timepoint) {
    return Profiler::from(timepoint) * 1e-6;
}

double Profiler::stopSeconds() {
    return Profiler::secondsFrom(this->last);
}

double Profiler::lapSeconds() {
    double d = this->stopSeconds();
    this->start();

    return d;
}
