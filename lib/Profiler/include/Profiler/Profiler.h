#ifndef PROFILER_H
#define PROFILER_H

#include <chrono>

class Profiler {
public:

    typedef std::chrono::system_clock::time_point time_point;
    typedef std::chrono::microseconds::rep duration;

    Profiler() {
        this->start();
    }

    static time_point now();

    static duration from(time_point timepoint);

    static double secondsFrom(time_point timepoint);

    time_point start();

    duration stop();

    duration lap();

    double stopSeconds();

    double lapSeconds();

private:
    time_point last;

};


#endif //PROFILER_H
