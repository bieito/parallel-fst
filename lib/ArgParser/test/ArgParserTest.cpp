#include "ArgParser/ArgParser.h"
#include "TestTools/TestTools.h"

Test(argParserPositionalOrder) {

    ArgParser ap;

    AssertNoThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addPositionalArg("pos1", "a1");
        ap.addPositionalArg("pos2", "a2");
        ap.addPositionalArg("pos3", "a3", {"3"});
        ap.addPositionalArg("pos4", "a4", {"4"});
    })

    AssertNoThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addPositionalArg("pos1", "a1");
        ap.addPositionalArg("pos2", "a2");
    })

    AssertNoThrown(std::invalid_argument, {
        ap = ArgParser();
        ap.addPositionalArg("pos3", "a3", {"3"});
        ap.addPositionalArg("pos4", "a4", {"4"});
    })

    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addPositionalArg("pos1", "a1");
        ap.addPositionalArg("pos3", "a3", {"3"});
        ap.addPositionalArg("pos2", "a2");
        ap.addPositionalArg("pos4", "a4", {"4"});
    })

}

Test(argParserNoProvidedExn) {
    ArgParser ap = ArgParser();

    ap.addPositionalArg("pos1", "a1");
    ap.addOptionArg("opt1", {'a'}, {}, "o1", false, std::optional<std::string>(), std::optional<std::string>());

    /* Positional not parsed */
    AssertIsThrown(std::invalid_argument, {
        ap.getPositionalArg("pos1");
    })

    /* Option not parsed */
    AssertIsThrown(std::invalid_argument, {
        ap.getOptionArg("opt1");
    })

    /* Arg not added */
    AssertIsThrown(std::invalid_argument, {
        ap.isFlagPresent("no_flag");
    })

}

Test(argParserKeyCollision) {

    ArgParser ap;

    /* Test collisions */
    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addFlagArg("flag", {'g'}, {});
        ap.addOptionArg("flag", {'f'}, {}, "file", false, std::optional<std::string>(), std::optional<std::string>());
    })

    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addPositionalArg("pos", "file");
        ap.addOptionArg("opt", {}, {"input", "file"}, "file", false, std::optional<std::string>(), std::optional<std::string>());
        ap.addFlagArg("pos", {}, {"pos"});
    })

}

Test(argParserFlagCollision) {

    ArgParser ap;

    /* Test collisions */
    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addFlagArg("flag", {'g', 'f'}, {});
        ap.addOptionArg("opt", {'f'}, {}, "file", false, std::optional<std::string>(), std::optional<std::string>());
    })

    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addFlagArg("flag", {}, {"file"});
        ap.addOptionArg("opt", {}, {"input", "file"}, "file", false, std::optional<std::string>(), std::optional<std::string>());
    })

}

Test(argParserFlagEmpty) {

    ArgParser ap;

    /* Test empty */
    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addFlagArg("flag", {}, {});
    })

    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addFlagArg("flag", {}, {""});
    })

    AssertNoThrown(std::invalid_argument, {
        ap = ArgParser();

        ap.addFlagArg("flag1", {'a'}, {});
        ap.addFlagArg("flag2", {}, {"aa"});
    })

}

Test(argParserParseAndGetValues) {
    ArgParser ap;
    char *argv[] = {(char *) "p1", (char *) "p2", (char *) "-u", (char *) "--help", (char *) "-a", (char *) "0",
                    (char *) "-a", (char *) "0", (char *) "-v", (char *) "1", (char *) "--bbb", (char *) "abc"};

    ap = ArgParser("test", "Description for this nice test command that allows kargs", true);
    ap.addPositionalArg("p1", "p1", {}, "This is an optional positional arg");
    ap.addPositionalArg("p2", "p2", {"AA"});
    ap.addPositionalArg("p3", "p3", {"BB"});
    ap.addFlagArg("h_true", {'h'}, {"help"}, "This is a flag");
    ap.addFlagArg("l_false", {'l'}, {}, "This is a flag with two short options only");
    ap.addFlagArg("lo_false", {}, {"list-options"}, "This is a flag with long option only");
    ap.addFlagArg("u_true", {'u', 'U'}, {"usage"}, "This is a flag with various options");
    ap.addOptionArg("v_1", {'v'}, {"verbose"}, "0|1|2", false, {}, "This is a mandatory switch/option");
    ap.addOptionArg("t_0", {'t'}, {"threads", "parallel"}, "int", false, {"4"},
                    "This is an optional switch/option with various options");
    ap.addOptionArg("a_0", {'a'}, {}, "0|1|2", false, {}, "This is a mandatory switch/option with short option only");
    ap.addOptionArg("b_1", {}, {"bbb"}, "char", false, {"b"},
                    "This is an optional switch/option with long option only");

    ap.parse(12, argv);

    AssertEquals(std::string("p1"), ap.getPositionalArg("p1"));
    AssertEquals(std::string("p2"), ap.getPositionalArg("p2"));
    AssertEquals(std::string("BB"), ap.getPositionalArg("p3"));

    AssertTrue(ap.isFlagPresent("h_true"));
    AssertTrue(!ap.isFlagPresent("l_false"));
    AssertTrue(!ap.isFlagPresent("lo_false"));
    AssertTrue(ap.isFlagPresent("u_true"));

    AssertEquals(1ul, ap.getOptionArgs("v_1").size());
    AssertEquals(std::string("1"), ap.getOptionArg("v_1"));

    AssertEquals(1ul, ap.getOptionArgs("t_0").size());
    AssertEquals(std::string("4"), ap.getOptionArg("t_0"));

    AssertEquals(2ul, ap.getOptionArgs("a_0").size());
    AssertEquals(std::string("0"), ap.getOptionArgs("a_0")[1]);

    AssertEquals(1ul, ap.getOptionArgs("b_1").size());
    AssertEquals(std::string("abc"), ap.getOptionArg("b_1"));

}

Test(argParserParsingKargsTest) {

    ArgParser ap;
    char *argv[] = {(char *) "valid", (char *) "invalid"};

    AssertNoThrown(std::invalid_argument, {
        ap = ArgParser("cmd", {}, true);
        ap.addPositionalArg("valid", "valid", {});

        ap.parse(2, argv);
    })

    AssertIsThrown(std::invalid_argument, {
        ap = ArgParser("cmd", {}, false);
        ap.addPositionalArg("valid", "valid", {});

        ap.parse(2, argv);
    })

    /* Capacity */ {
        ap = ArgParser("cmd", {}, true);
        ap.addPositionalArg("valid", "valid", {});

        AssertEquals(0ul, ap.getKargs().size());

        ap.parse(2, argv);
        AssertEquals(1ul, ap.getKargs().size());
        AssertEquals(std::string("invalid"), ap.getKargs()[0]);
    }

}

Test(argParserToStringTest) {

    ArgParser ap;

    /* To string */ {
        std::string result, expected;

        ap = ArgParser("testa", "Description", false);
        result = (std::string) ap;
        expected = "USAGE: testa\nDescription\n";
        AssertEquals(expected, result);

        ap = ArgParser("testb", {}, false);
        result = (std::string) ap;
        expected = "USAGE: testb\n";
        AssertEquals(expected, result);

        ap = ArgParser("testc", {}, true);
        result = (std::string) ap;
        expected = "USAGE: testc ...\n";
        AssertEquals(expected, result);


        ap = ArgParser("test", "Description for this nice test command that allows kargs", true);
        ap.addPositionalArg("posi_1", "p1", {}, "This is an optional positional arg");
        ap.addPositionalArg("posi_2", "p2", {"AA"});
        ap.addPositionalArg("posi_3", "p3", {"BB"});
        ap.addFlagArg("flag_1", {'h'}, {"help"}, "This is a flag\nwith a break");
        ap.addFlagArg("flag_2", {'l'}, {}, "This is a flag with two short options only");
        ap.addFlagArg("flag_3", {}, {"list-options"}, "This is a flag with long option only");
        ap.addFlagArg("flag_4", {'u', 'U'}, {"usage"}, "This is a flag with various options");
        ap.addOptionArg("option_1", {'v'}, {"verbose"}, "0|1|2", false, {}, "This is a mandatory switch/option");
        ap.addOptionArg("option_2", {'t'}, {"threads", "parallel"}, "int", false, {"4"},
                        "This is an optional switch/option with various options");
        ap.addOptionArg("option_3", {'a'}, {}, "0|1|2", false, {},
                        "This is a mandatory switch/option with short option only");
        ap.addOptionArg("option_4", {}, {"bbb"}, "char", false, {"b"},
                        "This is an optional switch/option with long option only");
        expected = "USAGE: test <p1> [<p2>] [<p3>] [-h] [-l] [--list-options] [-u] -v <0|1|2> [-t <int>] -a <0|1|2> [--bbb <char>] ...\n"
                   "Description for this nice test command that allows kargs\n"
                   "\n"
                   "OPTIONS:\n"
                   "  <p1>                               This is an optional positional arg\n"
                   "  <p2>                               [default = AA]\n"
                   "  <p3>                               [default = BB]\n"
                   "  -h, --help                         This is a flag\n"
                   "                                         with a break\n"
                   "  -l                                 This is a flag with two short options only\n"
                   "  --list-options                     This is a flag with long option only\n"
                   "  -u, -U, --usage                    This is a flag with various options\n"
                   "  -v, --verbose <0|1|2>              This is a mandatory switch/option\n"
                   "  -t, --threads, --parallel <int>    [default = 4] This is an optional switch/option with various options\n"
                   "  -a <0|1|2>                         This is a mandatory switch/option with short option only\n"
                   "  --bbb <char>                       [default = b] This is an optional switch/option with long option only\n";
        result = (std::string) ap;
        AssertEquals(expected, result);
    }

}

int main() {

    //TestManager::setVerbosityLevel(TestManager::FULL);

    AddTest(argParserPositionalOrder);
    AddTest(argParserNoProvidedExn);
    AddTest(argParserKeyCollision);
    AddTest(argParserFlagCollision);
    AddTest(argParserFlagEmpty);
    AddTest(argParserParseAndGetValues);
    AddTest(argParserParsingKargsTest);
    AddTest(argParserToStringTest);

    return TestManager::runTests() != 0;

}