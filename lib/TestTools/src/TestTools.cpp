#include "TestTools/TestTools.h"

std::vector<test> TestManager::tests = {};
TestManager::VerboseLevel TestManager::verbosity = REDUCED;

int TestManager::runTests() {

    unsigned int errors = 0;
    unsigned int align = std::to_string(tests.size()).size();

#pragma omp parallel for default(none) firstprivate(align) shared(std::cout) reduction(+: errors) schedule(dynamic)
    for (unsigned int i = 0; i < tests.size(); ++i) {
        auto &test = tests[i];
        std::string message;
        bool error = false;

        try {
            test();
        } catch (assertion_error &err) {
            errors += 1;

            message = err.what();
            error = true;
        } catch (std::exception &err) {
            errors += 1;

            std::stringstream sstr;
            sstr << UBOLDYELLOW << "Unexpected exception" << RESET << " " << err.what();
            message = sstr.str();
            error = true;
        }

        if (verbosity > QUIET) {
#pragma omp critical
            if (error) {
                std::cout << "[ ";
                std::cout.width(align);
                std::cout << (i + 1) << "/" << tests.size() << "  " << BOLDRED << "FAIL" << RESET << " ] "
                     << message << RESET << std::endl;
            } else if (verbosity > REDUCED) {
                std::cout << "[ ";
                std::cout.width(align);
                std::cout << (i + 1) << "/" << tests.size() << "  ";
                std::cout << BOLDGREEN << "PASS" << RESET << " ]" << std::endl;
            }

        }
    }

    if (errors) {
        std::cout << errors << "/" << tests.size() << " TESTS " << BOLDRED << "FAILED" << RESET << std::endl;
    } else {
        std::cout << "ALL " << tests.size() << " TESTS " << BOLDGREEN << "PASSED" << RESET << std::endl;
    }

    tests.clear();

    return -((int) errors);

}