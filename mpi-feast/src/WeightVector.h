#include "Utils.h"

#ifndef PARALLEL_FST_WEIGHTS_H
#define PARALLEL_FST_WEIGHTS_H

class WeightVector {
public:
    void fromFile(const char *path);

    inline std::vector<double> getWeightVector() {
        return _weightVector;
    };

    inline unsigned int getNSamples() {
        return _nSamples;
    };

protected:
    std::vector<double> _weightVector;
    int _nSamples;
};


#endif //PARALLEL_FST_WEIGHTS_H
