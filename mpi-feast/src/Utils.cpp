#include "Utils.h"
#include <sys/time.h>

void Utils::log(const char *args, ...) {
    va_list va_ptr;
    va_start(va_ptr, args);
    vfprintf(stderr, args, va_ptr);
    va_end(va_ptr);
}

void Utils::exit(const char *args, ...) {
    va_list va_ptr;
    fprintf(stderr, "[P%d] ", getpid());
    //print out the message
    va_start(va_ptr, args);
    vfprintf(stderr, args, va_ptr);
    va_end(va_ptr);

    //end MPI processes
    MPI_Abort(MPI_COMM_WORLD, -1);

    //exit the program
    ::exit(-1);
}

double Utils::getSysTime() {
    double dtime;
    struct timeval tv{};

    gettimeofday(&tv, nullptr);

    dtime = (double) tv.tv_sec;
    dtime += (double) (tv.tv_usec) / 1000000.0;

    return dtime;

}
