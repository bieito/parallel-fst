#include <ArgParser/ArgParser.h>

#include <mpi-feast/AlgorithmSelector.h>
#include <mpi-feast/mRMR_D.h>
#include <mpi-feast/CMIM.h>
#include <mpi-feast/JMI.h>
#include <mpi-feast/DISR.h>
#include <mpi-feast/ICAP.h>
#include <mpi-feast/CondMI.h>
#include <mpi-feast/MIM.h>
#include <mpi-feast/BetaGamma.h>
#include <algorithm>
#include "FileParser.h"
#include "Options.h"
#include "WeightVector.h"
#include "ArrayOps.h"

#include "ProjectConfig.h"

void setArgParserOptions(ArgParser &parser);

bool checkAndConvertOptions(ArgParser &parser, Options &options);

//TODO: check algorithms memory deletions
int main(int argc, char *argv[]) {

    MPI_Init(&argc, &argv);

    /* MPI */
    int numProcs, myRank;
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    /* Root proc data */
    double iniTime, endTime, matrixReadTime = 0, mapTime = 0, weightsReadTime = 0;

    /* Read and share options */
    Options options;
    bool stop_execution = true;
    if (IS_ROOT(myRank)) {
        ArgParser arg_parser("parallel-fst --mpi", "Run a hybrid MPI/multithreaded version of a FEAST method");
        setArgParserOptions(arg_parser);
        arg_parser.parse(argc - 1, &argv[1]);

        options.setNumProcs(numProcs);
        stop_execution = !checkAndConvertOptions(arg_parser, options);
    }
    MPI_Bcast(&stop_execution, 1, MPI_CXX_BOOL, ROOT_ID, MPI_COMM_WORLD);
    if (stop_execution) {
        // Graceful exit
        MPI_Finalize();
        return 0;
    } else {
        options.broadcast();
    }

    /* Check input file name */
    FileParser *parser = FileParser::getFileParser(options.getInputFileName());
    if (!parser) {
        if (IS_ROOT(myRank)) {
            std::cerr
                    << "ERROR: input file extension not recognized. Options: .arff, .ARFF, .csv, .CSV, .libsvm, .LIBSVM"
                    << std::endl;
        }

        MPI_Finalize();
        return 0;
    }

    /* Read matrix values & Measure time */
    InputMat *mat;
    if (IS_ROOT(myRank)) {
        iniTime = Utils::getSysTime();

        parser->readInput(mat, options.getNumDiscBins(), options.getNumProcs());

        matrixReadTime = Utils::getSysTime();
        Utils::log("Time to read input matrix: %lf seconds\n", matrixReadTime - iniTime);
    } else {
        parser->readInput(mat, options.getNumDiscBins(), options.getNumProcs());
    }
    delete parser;

    /* Check requested number of features to select */
    if (options.getNumSelecFeat() > mat->getNFeatures()) {
        options.setNumSelecFeat(mat->getNFeatures());

        if (IS_ROOT(myRank)) {
            options.setNumSelecFeat(mat->getNFeatures());
            std::cerr << "WARNING: Tried to select '" + std::to_string(options.getNumSelecFeat()) +
                         "' features, but dataset has only '" +
                         std::to_string(mat->getNFeatures()) + "'\n           -> using k=" +
                         std::to_string(mat->getNFeatures())
                      << std::endl;
        }
    }

    /* Check weighted version */
    double *weightVector;
    bool isWeighted = !options.getWeightsFileName().empty();
    if (isWeighted) {
        unsigned int weightNSamples;
        weightVector = new double[mat->getNSamples()];

        if (IS_ROOT(myRank)) {
            WeightVector *rootWeightVector;

            rootWeightVector = new WeightVector();
            iniTime = Utils::getSysTime();

            rootWeightVector->fromFile(options.getWeightsFileName().c_str());

            weightsReadTime = Utils::getSysTime();
            Utils::log("Time to read weight vector: %lf seconds\n", weightsReadTime - iniTime);

            weightNSamples = rootWeightVector->getNSamples();
            copyArray(rootWeightVector->getWeightVector().data(), weightVector, mat->getNSamples());

            MPI_Bcast(weightVector, mat->getNSamples(), MPI_DOUBLE, ROOT_ID, MPI_COMM_WORLD);

            delete rootWeightVector;

        } else {
            MPI_Bcast(weightVector, mat->getNSamples(), MPI_DOUBLE, ROOT_ID, MPI_COMM_WORLD);
        }

        MPI_Bcast(&weightNSamples, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
        if (weightNSamples != mat->getNSamples()) {
            if (IS_ROOT(myRank)) {
                std::cerr << "ERROR: number of weights do not match number of samples" << std::endl;
            }

            delete[] weightVector;
            delete mat;

            MPI_Finalize();
            return 0;
        }

    }

    /* Map matrix if needed && Measure time */
    if (options.doRangeCompression()) {
        if (IS_ROOT(myRank)) {
            iniTime = Utils::getSysTime();

            mat->mapValues();

            mapTime = Utils::getSysTime();
            Utils::log("Time to map initial values: %lf seconds\n", mapTime - iniTime);
        } else {
            mat->mapValues();
        }
    }

    FeatureSelectionAlgorithm *algorithm;
    switch (options.getAlgorithm()) {
        case AlgorithmSelector::MRMR_D:
            algorithm = new class mRMR_D(mat, options);
            break;
        case AlgorithmSelector::CMIM:
            algorithm = new class CMIM(mat, options);
            break;
        case AlgorithmSelector::JMI:
            algorithm = new class JMI(mat, options);
            break;
        case AlgorithmSelector::DISR:
            algorithm = new class DISR(mat, options);
            break;
        case AlgorithmSelector::COND_MI:
            algorithm = new class CondMI(mat, options);
            break;
        case AlgorithmSelector::ICAP:
            algorithm = new class ICAP(mat, options);
            break;
        case AlgorithmSelector::MIM:
            algorithm = new class MIM(mat, options);
            break;
        case AlgorithmSelector::BETA_GAMMA:
            algorithm = new class BetaGamma(mat, options);
            break;
        default:
            if (IS_ROOT(myRank)) {
                std::cerr << "ERROR: unknown algorithm" << std::endl;
            }

            MPI_Finalize();
            return 0;
    }

    /* Start time measurement */
    if (IS_ROOT(myRank)) {
        iniTime = Utils::getSysTime();
    }

    /* Execution */
    if (options.getOriginal() && !isWeighted) {
        algorithm->exec();
    } else if (options.getOriginal() && isWeighted) {
        algorithm->exec(weightVector);
    } else if (!options.getOriginal() && !isWeighted) {
        algorithm->exec(options.getNumTh(), myRank, mat->getMyOffset());
    } else if (!options.getOriginal() && isWeighted) {
        algorithm->exec(weightVector, options.getNumTh(), myRank, mat->getMyOffset());
    }

    if (IS_ROOT(myRank)) {
        /* Stop time measurement && Log total time */
        endTime = Utils::getSysTime();
        if (options.getOriginal()) {
            Utils::log("Time to execute %s algorithm: %lf seconds\n",
                       AlgorithmSelector::algorithmToStr(options.getAlgorithm()).c_str(), endTime - iniTime);
        } else {
            Utils::log("Time to execute %s algorithm (%d process%s; %d thread%s/process): %lf seconds\n",
                       AlgorithmSelector::algorithmToStr(options.getAlgorithm()).c_str(), options.getNumProcs(),
                       (options.getNumProcs() == 1) ? "" : "es",
                       options.getNumTh(), (options.getNumTh() == 1) ? "" : "s",
                       endTime - iniTime);
        }

        /* Write results to file or stdout */
        unsigned int *outputFeatures = algorithm->getResults()->getOutputFeatures();
        double *featureScores = algorithm->getResults()->getFeatureScores();

        if (options.getOutputFileName() == "-") {
            std::cout << "#feature_index,feature_score" << std::endl;
            std::cout << std::setprecision(6) << std::fixed;
            for (unsigned int i = 0; i < options.getNumSelecFeat(); i++) {
                std::cout << outputFeatures[i] << "," << featureScores[i] << std::endl;
            }
        } else {
            std::ofstream outFile;
            outFile.open((options.getOutputFileName()).c_str());

            outFile << "#feature_index,feature_score" << std::endl;
            outFile << std::setprecision(6) << std::fixed;
            for (unsigned int i = 0; i < options.getNumSelecFeat(); i++) {
                outFile << outputFeatures[i] << "," << featureScores[i] << std::endl;
            }

            outFile.close();
        }
    }

    if (isWeighted) {
        delete[] weightVector;
    }

    delete algorithm;
    delete mat;

    MPI_Finalize();
}

bool checkAndConvertOptions(ArgParser &parser, Options &options) {

    if (parser.noArgs()) {
        std::cout << parser.helpStr() << std::endl;
        return false;
    }

    /* First, check flags that invalidate execution */
    if (parser.isFlagPresent("help")) {
        std::cout << parser.helpStr() << std::endl;
        return false;
    }

    if (parser.isFlagPresent("print_version")) {
        std::stringstream sstr;
        sstr << "mrmr-cli " << BUILD_VERSION << " (" << BUILD_TAG;
        if (!std::string(BUILD_TYPE).empty()) {
            sstr << "[" << BUILD_TYPE << "]";
        }
        sstr << " " << BUILD_TIME << ")" << std::endl;
        sstr << "[" << BUILD_CXX << "] on " << BUILD_PLATFORM << std::endl;
        sstr << "MPI: " << BUILD_MPI << std::endl;
        std::cout << sstr.str();
        return false;
    }

    options.setOriginal(parser.isFlagPresent("use_feast"));

    options.setNumThreads(std::stoul(parser.getOptionArgOrDefault("num_threads")));

    if (parser.isOptionPresent("input")) {
        options.setInputFileName(parser.getOptionArg("input"));
    } else {
        std::cerr << "ERROR: Mandatory argument 'input path' not provided" << std::endl;
        return false;
    }

    if (parser.isOptionPresent("weights")) {
        options.setWeightsFileName(parser.getOptionArg("weights"));
    } else {
        options.setWeightsFileName("");
    }

    if (parser.isOptionPresent("output")) {
        options.setOutputFileName(parser.getOptionArg("output"));
    } else {
        options.setOutputFileName("-");
    }

    if (parser.isOptionPresent("algorithm")) {
        std::string s = parser.getOptionArg("algorithm");

        // patch if BetaGama
        if (s.rfind("BetaGamma", 0) == 0) {
            double beta, gamma;

            // Check for the presence of two ':'
            if (std::count(s.begin(), s.end(), ':') != 2) {
                std::cerr
                        << "ERROR: Invalid argument for BetaGamma parameters - it must be in format 'BetaGamma:<float>:<float>'"
                        << std::endl;
                return false;
            }
            // Parse beta and gamma
            std::string numbers = s.substr(s.find(':') + 1);
            try {
                beta = std::stod(numbers.substr(0, numbers.find(':')));
                gamma = std::stod(numbers.substr(numbers.find(':') + 1));
            } catch (std::invalid_argument &e) {
                std::cerr
                        << "ERROR: Invalid argument for BetaGamma parameters - it must be in format 'BetaGamma:<float>:<float>'"
                        << std::endl;
                return false;
            }

            options.setBetaGamma(beta, gamma);
            options.setAlgorithm("BetaGamma");
        } else {
            options.setAlgorithm(s);
        }
    } else {
        std::cerr << "ERROR: Mandatory argument 'algorithm' not provided" << std::endl;
        return false;
    }

    if (parser.isOptionPresent("select")) {
        options.setNumSelecFeat(std::stoul(parser.getOptionArg("select")));
    } else {
        std::cerr << "ERROR: Mandatory argument 'number of features to select' not provided" << std::endl;
        return false;
    }

    options.setNumDiscBins(std::stoul(parser.getOptionArgOrDefault("disc_bins")));

    /* Check last configurations */
    // TODO options.setVerboseLevel(std::stoul(parser.getOptionArg("verbose")));
    // TODO options.setDoValidate(parser.isFlagPresent("test"));

    if (options.getOriginal() && options.getNumProcs()) {
        std::cerr << "ERROR: Original version can only be launched with 1 MPI process" << std::endl;
        return false;
    }

    if (options.getOriginal() && options.getNumTh() > 1) {
        options.setNumThreads(1); // this is useless, it will never be used
        std::cerr << "WARNING: Original version is sequential\n"
                     "           -> Switched to 1 thread" << std::endl;
        return false;
    }

//
//    if (options.getImplementation() == CliConfig::FEAST && options.doValidate()) {
//        options.setDoValidate(false);
//        std::cerr << "WARNING: Validation is useless when running the original FEAST implementation\n"
//                     "           -> Validation disabled" << std::endl;
//    }
//
//    if (options.getImplementation() == CliConfig::FEAST && options.getAlgorithmConfig().getPrecision() != Config::DOUBLE) {
//        options.setPrecision("double");
//        std::cerr << "WARNING: Original FEAST implementation is only compatible with double precision data types\n"
//                     "           -> Switched to double precision" << std::endl;
//    }
//
//    /*if (options.doValidate() && options.getDataType() == CliConfig::) {
//        options.setDoValidate(false);
//        std::cerr << "WARNING: CPU algorithm is not compatible with half precision datatypes\n"
//                     "           -> Validation disabled" << std::endl;
//    }*/

    return true;
}

void setArgParserOptions(ArgParser &parser) {
    parser.addOptionArg("input", {'i'}, {"input"}, "path", true,
                        {}, "input file to read from\nallowed formats are arff, csv and libsvm");
    parser.addOptionArg("weights", {'w'}, {"weights"}, "path", false,
                        {}, "input file to read weights from\nallowed format is csv");
    parser.addOptionArg("output", {'o'}, {"output"}, "path", false,
                        {}, "output file to write results to\n"
                            "if not specified, write to stdout");

    parser.addOptionArg("algorithm", {'a'}, {"algorithm"}, "ALG", true,
                        {},
                        "algorithm to run - ALG = (MIM|CondMI|BetaGammma[:<float>:<float>]|JMI|mRMR|CMIM|ICAP|DISR)\n"
                        "MIM     Mutual Information Maximisation\n"
                        "CondMI  Conditional Mutual Information              [2012 - G. Brown, A. Pocock, M.-J. Zhao, M. Luján]\n"
                        "JMI     Joint Mutual Information                    [1999 - H. H. Yang, J. Moody]\n"
                        "mRMR    Minimum-Redundancy Max-Relevance            [2005 - H. Peng, F. Long, C. Ding]\n"
                        "CMIM    Conditional Mutual Information Maximisation [2004 - F. Fleuret]\n"
                        "ICAP    Interaction Capping                         [2005 - A. Jakulin]\n"
                        "DISR    Double Input Symmetrical Relevance          [2006 - P. E. Meyer, G. Bontempi]\n"
                        "BetaGamma[:<b>:<g>]  BetaGamma Space\n"
                        "    values for Beta <b> (weight of redundancy) and Gamma <g> (weight of conditional redundancy) can be provided\n"
                        "    well-known configurations are:\n"
                        "        beta    | gamma | method\n"
                        "    ------------+-------+--------\n"
                        "         0.0    |  0.0  |  MIM\n"
                        "     (0.0, 1.0] |  0.0  |  MIFS  Mutual Information Feature Selection  [1994 - R. Battiti]\n"
                        "         1.0    |  1.0  |  CIFE  Conditional Infomax Feature Selection [2006 - D. Lin, X. Tang]"
    );

    parser.addOptionArg("select", {'k'}, {"select"}, "int", true,
                        {}, "number of features to select");
    parser.addFlagArg("range_compression", {'c'}, {"rangecomp"},
                      "apply a range compression before running the algorithm\n"
                      "useful for datasets where number of feature values >> number of samples.");
    parser.addOptionArg("disc_bins", {'d'}, {"disc"}, "n_bins", false,
                        {},
                        "discretize the input dataset with the binning technique using the specified number of bins");

    parser.addOptionArg("num_processes", {'n'}, {"processes"}, "int", false,
                        {"1"}, "number of MPI processes to launch");
    parser.addOptionArg("num_threads", {'t'}, {"threads"}, "int", false,
                        {std::to_string(std::thread::hardware_concurrency())},
                        "number of threads that each MPI process will launch\n"
                        "defaults to hardware concurrency");

    parser.addFlagArg("use_feast", {}, {"feast"},
                      "original CPU version of algorithm using the FEAST library (sequential)");
    //parser.addFlagArg("test", {}, {"test", "validate"},
    //                  "in addition to other execution, run original FEAST implementation and validate results");
    parser.addFlagArg("profile", {}, {"profile"},
                      "measure times for any active execution (currently ignored)"); // TODO parse and use

    //parser.addOptionArg("verbose", {'v'}, {"verbose"}, "0|1|2",
    //                    {"0"}, "verbosity level\n"
    //                           "0 - error and warning (to stderr) and explicitly wanted info as validation, profiling, etc (to stdout)\n"
    //                           "1 - minimal\n"
    //                           "2 - extra info, as dataset dimensions or device characteristics");

    parser.addFlagArg("print_version",
                      {'V'}, {"version"},
                      "show the version of the program and other build info");
    parser.addFlagArg("help",
                      {'h'}, {"help", "usage"},
                      "show this help and exit");
}
