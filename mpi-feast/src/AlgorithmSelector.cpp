#include <mpi-feast/AlgorithmSelector.h>

struct NameAlgoPair {
    const char *name;
    AlgorithmSelector::AlgorithmType algorithm;
};

NameAlgoPair collection[] = {
        {"mRMR",    AlgorithmSelector::MRMR_D},
        {"CMIM",      AlgorithmSelector::CMIM},
        {"JMI",       AlgorithmSelector::JMI},
        {"DISR",      AlgorithmSelector::DISR},
        {"ICAP",      AlgorithmSelector::ICAP},
        {"CondMI",    AlgorithmSelector::COND_MI},
        {"MIM",       AlgorithmSelector::MIM},
        {"BetaGamma", AlgorithmSelector::BETA_GAMMA},
        {nullptr,     AlgorithmSelector::NONE}
};

AlgorithmSelector::AlgorithmType AlgorithmSelector::strToAlgorithm(const std::string &param) {
    AlgorithmType algorithm = NONE;

    for (int i = 0; collection[i].name; i++) {
        if (param == collection[i].name) {
            algorithm = collection[i].algorithm;
            break;
        }
    }

    return algorithm;
}

std::string AlgorithmSelector::algorithmToStr(AlgorithmSelector::AlgorithmType algorithm) {
    std::string name = "undefined";

    for (int i = 0; collection[i].name; i++) {
        if (algorithm == collection[i].algorithm) {
            name = collection[i].name;
            break;
        }
    }

    return name;
}