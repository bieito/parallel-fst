#include "AlgorithmResults.h"

AlgorithmResults::AlgorithmResults(unsigned int numFeatures) {
    outputFeatures = new unsigned int[numFeatures];
    featureScores = new double[numFeatures];
}

AlgorithmResults::~AlgorithmResults() {
    delete[] outputFeatures;
    delete[] featureScores;
}
