#include <mpi-feast/CMIM.h>

void cmimInitMITh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples, unsigned int noOfFeatures,
                  unsigned int *featureMatrix, unsigned int *classColumn, char *selectedFeatures,
                  double *classMI, unsigned int *maxMIFeature, double *maxMIValue) {
    double maxMI = -1.0;
    unsigned int maxMICounter = 0;
    unsigned int i;

    for (i = myId; i < noOfFeatures; i += numTh) {
        selectedFeatures[i] = 0;
        classMI[i] = calcMutualInformation(&featureMatrix[i * noOfSamples], classColumn, noOfSamples);

        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        } /* if bigger than current maximum */
    }

    *maxMIValue = maxMI;
    *maxMIFeature = maxMICounter;
}

void cmimTh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples,
            unsigned int noOfFeatures, unsigned int *featureMatrix,
            unsigned int *classColumn, unsigned int *outputFeatures,
            unsigned int nSelectedFeatures, const char *selectedFeatures,
            double *classMI, unsigned int *outputFeature, double *featureScore,
            unsigned int *selectedFeaturesMatrix) {

    double conditionalInfo;
    double score = -1;
    unsigned int lastUsedFeature;
    unsigned int i;

    *featureScore = -1; /* initialize to avoid returning last feature when all received were already selected */
    for (i = myId; i < noOfFeatures; i += numTh) {
        lastUsedFeature = 0;
        if (selectedFeatures[i] == 0) {
            while ((classMI[i] > score) && (lastUsedFeature < nSelectedFeatures)) {
                conditionalInfo = calcConditionalMutualInformation(&featureMatrix[i * noOfSamples], classColumn,
                                                                   &selectedFeaturesMatrix[lastUsedFeature *
                                                                                           noOfSamples], noOfSamples);
                if (classMI[i] > conditionalInfo) {
                    classMI[i] = conditionalInfo;
                }/*reset classMI*/
                lastUsedFeature += 1;
            } /* while partial score greater than score & not reached last feature */
            if (classMI[i] > score) {
                score = classMI[i];
                *featureScore = score;
                *outputFeature = i;
            } /* if partial score still greater than score */
        } /* if feature has not been selected yet */

    }
}

void cmimwInitMITh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples, unsigned int noOfFeatures,
                   unsigned int *featureMatrix, unsigned int *classColumn, char *selectedFeatures,
                   double *classMI, unsigned int *maxMIFeature, double *maxMIValue, double *weightVector) {
    double maxMI = -1.0;
    unsigned int maxMICounter = 0;
    unsigned int i;

    for (i = myId; i < noOfFeatures; i += numTh) {
        selectedFeatures[i] = 0;
        classMI[i] = calcWeightedMutualInformation(&featureMatrix[i * noOfSamples], classColumn, weightVector,
                                                   noOfSamples);

        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        } /* if bigger than current maximum */
    }

    *maxMIValue = maxMI;
    *maxMIFeature = maxMICounter;
}

void cmimwTh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples,
             unsigned int noOfFeatures, unsigned int *featureMatrix,
             unsigned int *classColumn, unsigned int *outputFeatures,
             unsigned int nSelectedFeatures, const char *selectedFeatures,
             double *classMI, unsigned int *outputFeature, double *featureScore,
             unsigned int *selectedFeaturesMatrix, double *weightVector) {

    double conditionalInfo;
    double score = -1;
    unsigned int lastUsedFeature;
    unsigned int i;

    *featureScore = -1; /* initialize to avoid returning last feature when all received were already selected */
    for (i = myId; i < noOfFeatures; i += numTh) {
        lastUsedFeature = 0;
        if (selectedFeatures[i] == 0) {
            while ((classMI[i] > score) && (lastUsedFeature < nSelectedFeatures)) {
                conditionalInfo = calcWeightedConditionalMutualInformation(&featureMatrix[i * noOfSamples], classColumn,
                                                                           &selectedFeaturesMatrix[lastUsedFeature *
                                                                                                   noOfSamples],
                                                                           weightVector, noOfSamples);
                if (classMI[i] > conditionalInfo) {
                    classMI[i] = conditionalInfo;
                }/*reset classMI*/
                lastUsedFeature += 1;
            } /* while partial score greater than score & not reached last feature */
            if (classMI[i] > score) {
                score = classMI[i];
                *featureScore = score;
                *outputFeature = i;
            } /* if partial score still greater than score */
        } /* if feature has not been selected yet */

    }
}

void CMIM::exec() {
    unsigned int **matrix = arrayToMatrix(featureMatrix, noOfFeatures, noOfSamples);

    ::CMIM(featuresToSelect, noOfSamples, noOfFeatures, matrix, classColumn, results->getOutputFeatures(),
           results->getFeatureScores());

    deleteMatrix(matrix, noOfFeatures);
}

void CMIM::exec(double *featureWeights) {
    unsigned int **matrix = arrayToMatrix(featureMatrix, noOfFeatures, noOfSamples);

    ::weightedCMIM(featuresToSelect, noOfSamples, noOfFeatures, matrix, classColumn, featureWeights,
                   results->getOutputFeatures(),
                   results->getFeatureScores());

    deleteMatrix(matrix, noOfFeatures);
}

void CMIM::exec(int numTh, int myRank, int myOffset) {

    char *selectedFeatures = new char[noOfFeatures];
    /* holds the class MI values as the partial score from the CMIM paper */
    auto *classMI = new double[noOfFeatures];
    double score;
    unsigned int feature;

    /* storage for thread results */
    std::vector<std::thread> threads;
    auto *outputFeaturesTh = new unsigned int[numTh];
    auto *featureScoresTh = new double[numTh];
    /* for init */
    auto *maxMITh = new double[numTh];
    auto *maxMICounterTh = new unsigned int[numTh];

    /* MPI communications */
    double_int_t sendbuf, recvbuf;
    auto *selectedFeaturesMatrix = new unsigned int[featuresToSelect * noOfSamples];
    unsigned int globalFeatureIndex;

    /* find feature with higher MI */
    /* ensure a feature is always picked*/
    double maxMI = -1.0;
    unsigned int maxMICounter = noOfFeatures;

    unsigned int *outputFeatures = results->getOutputFeatures();
    double *featureScores = results->getFeatureScores();

    int i, j;

    /* init threads */
    for (i = 0; i < numTh; i++) {
        threads.emplace_back(cmimInitMITh, i, numTh, noOfSamples,
                             noOfFeatures, featureMatrix, classColumn,
                             selectedFeatures, classMI, &maxMICounterTh[i], &maxMITh[i]);
    }

    for (i = 0; i < numTh; i++) {
        threads[i].join();

        if (maxMITh[i] > maxMI || ((maxMITh[i] == maxMI) && maxMICounterTh[i] < maxMICounter)) {
            maxMI = maxMITh[i];
            maxMICounter = maxMICounterTh[i];
        }

    }
    threads.clear();

    /* Send scores and rank to find which processor got the optimal feature*/
    sendbuf.rank = myRank;
    sendbuf.score = maxMI;
    MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

    /* Send feature data */
    if (myRank == recvbuf.rank) {
        copyArray(&featureMatrix[maxMICounter * noOfSamples], selectedFeaturesMatrix, noOfSamples);
        globalFeatureIndex = myOffset + maxMICounter;
        selectedFeatures[maxMICounter] = 1;
    }
    MPI_Bcast(&selectedFeaturesMatrix[0], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
    MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

    outputFeatures[0] = globalFeatureIndex;
    featureScores[0] = recvbuf.score;

    /*****************************************************************************
     ** We have populated the classMI array, and selected the highest
     ** MI feature as the first output feature
     ** Now we move into the CMIM algorithm
     *****************************************************************************/

    for (i = 1; i < featuresToSelect; i++) {
        score = -1;
        feature = noOfFeatures;
        /* init threads */
        for (j = 0; j < numTh; j++) {
            threads.emplace_back(cmimTh, j, numTh, noOfSamples,
                                 noOfFeatures, featureMatrix, classColumn,
                                 outputFeatures, i, selectedFeatures, classMI,
                                 &outputFeaturesTh[j], &featureScoresTh[j],
                                 selectedFeaturesMatrix);
        }

        /* wait for threads & find biggest score*/
        for (j = 0; j < numTh; j++) {
            threads[j].join();

            if (featureScoresTh[j] > score || ((featureScoresTh[j] == score) && (outputFeaturesTh[j] < feature))) {
                score = featureScoresTh[j];
                feature = outputFeaturesTh[j];
            }
        }
        threads.clear();

        /* Find best score */
        sendbuf.rank = myRank;
        sendbuf.score = score;
        MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

        /* Send feature data */
        if (myRank == recvbuf.rank) {
            copyArray(&featureMatrix[feature * noOfSamples], &selectedFeaturesMatrix[i * noOfSamples],
                      noOfSamples);
            globalFeatureIndex = myOffset + feature;
            selectedFeatures[feature] = 1;
        }
        MPI_Bcast(&selectedFeaturesMatrix[i * noOfSamples], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
        MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

        featureScores[i] = recvbuf.score;
        outputFeatures[i] = globalFeatureIndex;
    } /* for the number of features to select */

    delete[] selectedFeaturesMatrix;
    delete[] maxMICounterTh;
    delete[] maxMITh;
    delete[] featureScoresTh;
    delete[] outputFeaturesTh;
    delete[] classMI;
    delete[] selectedFeatures;


}

void CMIM::exec(double *featureWeights, int numTh, int myRank, int myOffset) {

    char *selectedFeatures = new char[noOfFeatures];
    /* holds the class MI values as the partial score from the CMIM paper */
    auto *classMI = new double[noOfFeatures];
    double score;
    unsigned int feature;

    /* storage for thread results */
    std::vector<std::thread> threads;
    auto *outputFeaturesTh = new unsigned int[numTh];
    auto *featureScoresTh = new double[numTh];
    /* for init */
    auto *maxMITh = new double[numTh];
    auto *maxMICounterTh = new unsigned int[numTh];

    /* MPI communications */
    double_int_t sendbuf, recvbuf;
    auto *selectedFeaturesMatrix = new unsigned int[featuresToSelect * noOfSamples];
    unsigned int globalFeatureIndex;

    /* find feature with higher MI */
    /* ensure a feature is always picked*/
    double maxMI = -1.0;
    unsigned int maxMICounter = noOfFeatures;

    unsigned int *outputFeatures = results->getOutputFeatures();
    double *featureScores = results->getFeatureScores();

    int i, j;

    /* init threads */
    for (i = 0; i < numTh; i++) {
        threads.emplace_back(cmimwInitMITh, i, numTh, noOfSamples,
                             noOfFeatures, featureMatrix, classColumn,
                             selectedFeatures, classMI, &maxMICounterTh[i], &maxMITh[i], featureWeights);
    }

    for (i = 0; i < numTh; i++) {
        threads[i].join();

        if (maxMITh[i] > maxMI || ((maxMITh[i] == maxMI) && maxMICounterTh[i] < maxMICounter)) {
            maxMI = maxMITh[i];
            maxMICounter = maxMICounterTh[i];
        }

    }
    threads.clear();

    /* Send scores and rank to find which processor got the optimal feature*/
    sendbuf.rank = myRank;
    sendbuf.score = maxMI;
    MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

    /* Send feature data */
    if (myRank == recvbuf.rank) {
        copyArray(&featureMatrix[maxMICounter * noOfSamples], selectedFeaturesMatrix, noOfSamples);
        globalFeatureIndex = myOffset + maxMICounter;
        selectedFeatures[maxMICounter] = 1;
    }
    MPI_Bcast(&selectedFeaturesMatrix[0], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
    MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

    outputFeatures[0] = globalFeatureIndex;
    featureScores[0] = recvbuf.score;

    /*****************************************************************************
     ** We have populated the classMI array, and selected the highest
     ** MI feature as the first output feature
     ** Now we move into the CMIM algorithm
     *****************************************************************************/

    for (i = 1; i < featuresToSelect; i++) {
        score = -1;
        feature = noOfFeatures;
        /* init threads */
        for (j = 0; j < numTh; j++) {
            threads.emplace_back(cmimwTh, j, numTh, noOfSamples,
                                 noOfFeatures, featureMatrix, classColumn,
                                 outputFeatures, i, selectedFeatures, classMI,
                                 &outputFeaturesTh[j], &featureScoresTh[j],
                                 selectedFeaturesMatrix, featureWeights);
        }

        /* wait for threads & find biggest score*/
        for (j = 0; j < numTh; j++) {
            threads[j].join();

            if (featureScoresTh[j] > score || ((featureScoresTh[j] == score) && (outputFeaturesTh[j] < feature))) {
                score = featureScoresTh[j];
                feature = outputFeaturesTh[j];
            }
        }
        threads.clear();

        /* Find best score */
        sendbuf.rank = myRank;
        sendbuf.score = score;
        MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

        /* Send feature data */
        if (myRank == recvbuf.rank) {
            copyArray(&featureMatrix[feature * noOfSamples], &selectedFeaturesMatrix[i * noOfSamples],
                      noOfSamples);
            globalFeatureIndex = myOffset + feature;
            selectedFeatures[feature] = 1;
        }
        MPI_Bcast(&selectedFeaturesMatrix[i * noOfSamples], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
        MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

        featureScores[i] = recvbuf.score;
        outputFeatures[i] = globalFeatureIndex;
    } /* for the number of features to select */

    delete[] selectedFeaturesMatrix;
    delete[] maxMICounterTh;
    delete[] maxMITh;
    delete[] featureScoresTh;
    delete[] outputFeaturesTh;
    delete[] classMI;
    delete[] selectedFeatures;

}
