#include "InputMat.h"
#include <fstream>

#ifndef PARSER_FILEPARSER_H_
#define PARSER_FILEPARSER_H_

class format_error : public std::exception {
public:
    explicit format_error(std::string msg) : msg(std::move(msg)) {};

    inline const char *what() const noexcept override {
        return msg.c_str();
    }

private:
    std::string msg;
};

class FileParser {
public:
    static FileParser *getFileParser(std::string const &path);

    FileParser(const char *path);

    virtual ~FileParser() { _file.close(); }

    virtual void readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs);

protected:
    std::ifstream _file;
    int workerId;
};

// Class to represent arff format nominal samples
class NominalParameter {
public:
    // The string obtained from the file with the options separated by commas
    NominalParameter(std::string options) {
        _options = NULL;
        _numOptions = 1;

        // First loop to check the number of options
        int auxPos = -1;

        while ((auxPos = options.find(",", auxPos + 1)) != std::string::npos) {
            _numOptions++;
        }

        // Allocate the memory
        _options = new std::string[_numOptions];

        // Copy the values of the strings
        int iniSub = 0;
        int endSub;
        for (int i = 0; i < _numOptions - 1; i++) {
            endSub = options.find(",", iniSub);
            std::string pattern = options.substr(iniSub, endSub - iniSub);
            if (pattern[0] == ' ') {
                pattern = pattern.substr(1);
            }

            if (pattern[pattern.length() - 1] == ' ') {
                pattern = pattern.substr(0, pattern.length());
            }

            _options[i] = pattern;

            iniSub = endSub + 1;
        }

        // Get the last one until the end
        std::string pattern = options.substr(iniSub);
        if (pattern[0] == ' ') {
            pattern = pattern.substr(1);
        }

        if (pattern[pattern.length() - 1] == ' ') {
            pattern = pattern.substr(0, pattern.length() - 1);
        }

        _options[_numOptions - 1] = pattern;
    }

    virtual ~NominalParameter() {
        _numOptions = 0;

        if (_options != NULL) {
            delete[] _options;
        }
    };

    inline unsigned int getNumOptions() {
        return _numOptions;
    }

    // Return the integer value for certain option
    // Otherwise, error
    inline unsigned int getVal(std::string option) {
        std::string aux = option;
        if (option[0] == ' ') {
            aux = option.substr(1);
        }

        for (int i = 0; i < _numOptions; i++) {
            if (aux.compare(_options[i]) == 0) {
                return _numOptions - i - 1;
            }
        }

        // This value does not exist
        Utils::exit("ERROR in NominalParameter: the option %s does not exist\n", option.c_str());
        throw std::runtime_error("ERROR in NominalParameter");
    }

private:
    unsigned int _numOptions;
    std::string *_options;
};

// An .arff file has the values ordered by samples, so we must mix them
class ArffFileParser : public FileParser {
public:
    ArffFileParser(const char *path) : FileParser(path) {};

    virtual ~ArffFileParser() { _file.close(); }

    void readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs);
};

// A .csv file has the values ordered by samples, so we must mix them
class CsvFileParser : public FileParser {
public:
    CsvFileParser(const char *path) : FileParser(path) {};

    virtual ~CsvFileParser() { _file.close(); }

    void readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs);
};

// A .libsvm file has the values ordered by samples, so we must mix them
class LibsvmFileParser : public FileParser {
public:
    LibsvmFileParser(const char *path) : FileParser(path) {};

    virtual ~LibsvmFileParser() { _file.close(); }

    void readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs);
};

#endif /* PARSER_FILEPARSER_H_ */
