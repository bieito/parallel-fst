#include "Utils.h"
#include "ArrayOps.h"

#ifndef INPUTMAT_H_
#define INPUTMAT_H_

/* Rows for features and columns for samples */

class InputMat {
public:
    InputMat();

    // Functions when the input is order by samples
    InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<unsigned int *> sampleData,
             std::vector<int> classes, unsigned int numProcs);

    InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<double *> sampleData, std::vector<int> classes,
             unsigned int discBins, unsigned int numProcs);

    // For sparse methods
    InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<unsigned int *> sampleData,
             std::vector<int> classes, std::vector<unsigned int *> indexes, unsigned int numProcs);

    InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<double *> sampleData, std::vector<int> classes,
             std::vector<unsigned int *> indexes, unsigned int discBins, unsigned int numProcs);

    virtual ~InputMat();

    inline unsigned int getNSamples() {
        return _nSamples;
    }

    inline unsigned int getNFeatures() {
        return _nFeat;
    }

    inline unsigned int getNLocalFeatures() {
        return _nLocalFeat;
    }

    inline unsigned int getMyOffset() {
        return _myOffset;
    }

    inline unsigned int getElement(unsigned int feature, unsigned int sample) {
        return _data[feature * _nSamples + sample];
    }

    inline unsigned int *getFeatureData(unsigned int feature) {
        return &_data[feature * _nSamples];
    }

    inline unsigned int *getAllData() {
        return _data;
    }

    inline unsigned int getClass(unsigned int iClass) {
        return _classes[iClass];
    }

    inline unsigned int *getAllClasses() {
        return _classes;
    }

    void mapValues();

    inline void print() {
        Utils::log("Data:");
        for (int i = 0; i < _nFeat; i++) {
            for (int j = 0; j < _nSamples - 1; j++) {
                Utils::log("%u,", _data[i * _nSamples + j]);
            }
            Utils::log("%u\n", _data[i * _nSamples + _nSamples - 1]);
        }
        Utils::log("Classes:");
        for (int j = 0; j < _nSamples - 1; j++) {
            Utils::log("%u,", _classes[j]);
        }
        Utils::log("%u\n", _classes[_nSamples - 1]);
    }

private:
    void _discretize(double *dData, unsigned int *data, unsigned int discBins);

    static unsigned int _maxState(unsigned int *vector, unsigned int vectorLength);

    unsigned int _nSamples;
    unsigned int _nFeat;
    unsigned int _nLocalFeat;
    unsigned int *_data;
    unsigned int *_classes;
    unsigned int _myOffset;
};

#endif /* INPUTMAT_H_ */
