#include "WeightVector.h"

void WeightVector::fromFile(const char *path) {
    std::ifstream file;
    std::string line;
    unsigned int index;

    // Open file
    file.open(path);
    if (!file.is_open()) {
        Utils::exit("ERROR in WeightVector: file %s could not be opened\n", path);
    }

    // Read line per line
    index = 0;
    while (!file.eof()) {
        getline(file, line);

        if ((line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
            _weightVector.push_back(stod(line));
            index++;
        }

        _nSamples = index;
    }

    // Close file
    file.close();
}