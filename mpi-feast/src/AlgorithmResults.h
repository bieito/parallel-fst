#ifndef PARALLEL_FST_ALGORITHMRESULTS_H
#define PARALLEL_FST_ALGORITHMRESULTS_H


class AlgorithmResults {
private:
    unsigned int *outputFeatures;
    double *featureScores;
public:
    explicit AlgorithmResults(unsigned int numFeatures);

    ~AlgorithmResults();

    unsigned int *getOutputFeatures() const {
        return outputFeatures;
    }

    double *getFeatureScores() const {
        return featureScores;
    }

};


#endif //PARALLEL_FST_ALGORITHMRESULTS_H
