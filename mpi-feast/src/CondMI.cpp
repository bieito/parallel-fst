#include <mpi-feast/CondMI.h>

void condmiInitMITh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples, unsigned int noOfFeatures,
                    unsigned int *featureMatrix, unsigned int *classColumn, char *selectedFeatures,
                    double *classMI, unsigned int *maxMIFeature, double *maxMIValue) {
    double maxMI = -1.0;
    unsigned int maxMICounter = 0;
    unsigned int i;

    for (i = myId; i < noOfFeatures; i += numTh) {
        selectedFeatures[i] = 0;
        classMI[i] = calcMutualInformation(&featureMatrix[i * noOfSamples], classColumn, noOfSamples);

        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        } /* if bigger than current maximum */
    }

    *maxMIValue = maxMI;
    *maxMIFeature = maxMICounter;
}

void condmiTh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples,
              unsigned int noOfFeatures, unsigned int *featureMatrix,
              unsigned int *classColumn, unsigned int *outputFeatures,
              unsigned int nSelectedFeatures, char *selectedFeatures,
              double *classMI,
              double *featureMIMatrix, unsigned int *outputFeature,
              double *featureScore, unsigned int *selectedFeaturesMatrix, unsigned int *conditionVector) {

    /* MI */
    double currentScore;

    unsigned int i = 0, x = 0;
    *featureScore = 0.0;
    *outputFeature = -1;

    for (i = myId; i < noOfFeatures; i += numTh) {
        /*if we haven't selected i*/
        if (selectedFeatures[i] == 0) {
            currentScore = 0.0;

            currentScore = calcConditionalMutualInformation(&featureMatrix[i * noOfSamples], classColumn,
                                                            conditionVector, noOfSamples);

            if (currentScore > *featureScore) {
                *featureScore = currentScore;
                *outputFeature = i;
            }
        }/*if j is unselected*/
    }/*for number of features*/
}


void condmiwInitMITh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples, unsigned int noOfFeatures,
                     unsigned int *featureMatrix, unsigned int *classColumn, char *selectedFeatures,
                     double *classMI, unsigned int *maxMIFeature, double *maxMIValue, double *weightVector) {
    double maxMI = -1.0;
    unsigned int maxMICounter = 0;
    unsigned int i;

    for (i = myId; i < noOfFeatures; i += numTh) {
        selectedFeatures[i] = 0;
        classMI[i] = calcWeightedMutualInformation(&featureMatrix[i * noOfSamples], classColumn, weightVector,
                                                   noOfSamples);

        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        } /* if bigger than current maximum */
    }

    *maxMIValue = maxMI;
    *maxMIFeature = maxMICounter;

}

void condmiwTh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples,
               unsigned int noOfFeatures, unsigned int *featureMatrix,
               unsigned int *classColumn, unsigned int *outputFeatures,
               unsigned int nSelectedFeatures, char *selectedFeatures,
               double *classMI,
               double *featureMIMatrix, unsigned int *outputFeature,
               double *featureScore, unsigned int *selectedFeaturesMatrix, unsigned int *conditionVector,
               double *weightVector) {

    /* MI */
    double currentScore;

    unsigned int i = 0, x = 0;
    *featureScore = 0.0;
    *outputFeature = -1;

    for (i = myId; i < noOfFeatures; i += numTh) {
        /*if we haven't selected i*/
        if (selectedFeatures[i] == 0) {
            currentScore = 0.0;

            currentScore = calcWeightedConditionalMutualInformation(&featureMatrix[i * noOfSamples], classColumn,
                                                                    conditionVector, weightVector, noOfSamples);

            if (currentScore > *featureScore) {
                *featureScore = currentScore;
                *outputFeature = i;
            }
        }/*if j is unselected*/
    }/*for number of features*/
}

void CondMI::exec() {
    unsigned int **matrix = arrayToMatrix(featureMatrix, noOfFeatures, noOfSamples);
    int *tempOutput = new int[featuresToSelect]();
    unsigned int *outputFeatures = results->getOutputFeatures();

    ::CondMI(featuresToSelect, noOfSamples, noOfFeatures, matrix, classColumn, tempOutput,
             results->getFeatureScores());

    for (int i = 0; i < featuresToSelect; ++i) {
        outputFeatures[i] = (unsigned int) tempOutput[i];
    }

    outputFeatures = nullptr;
    delete[] tempOutput;
    deleteMatrix(matrix, noOfFeatures);
}

void CondMI::exec(double *featureWeights) {
    unsigned int **matrix = arrayToMatrix(featureMatrix, noOfFeatures, noOfSamples);
    int *tempOutput = new int[featuresToSelect];
    unsigned int *outputFeatures = results->getOutputFeatures();

    ::weightedCondMI(featuresToSelect, noOfSamples, noOfFeatures, matrix, classColumn, featureWeights,
                     tempOutput, results->getFeatureScores());

    for (int i = 0; i < featuresToSelect; ++i) {
        outputFeatures[i] = (unsigned int) tempOutput[i];
    }

    outputFeatures = nullptr;
    delete[] tempOutput;
    deleteMatrix(matrix, noOfFeatures);
}

void CondMI::exec(int numTh, int myRank, int myOffset) {

    char *selectedFeatures = new char[noOfFeatures];
    /* holds the class MI values */
    double *classMI = new double[noOfFeatures];

    /* holds MI for each pair of features */
    int sizeOfMatrix = featuresToSelect * noOfFeatures;
    double *featureMIMatrix = new double[sizeOfMatrix];

    double score;
    unsigned int feature;

    /* storage for thread results */
    std::vector<std::thread> threads;
    unsigned int *outputFeaturesTh = new unsigned int[numTh];
    double *featureScoresTh = new double[numTh];

    /* find feature with higher MI */
    /* ensure a feature is always picked*/
    double maxMI = -1.0;
    unsigned int maxMICounter = noOfFeatures;
    /* hold previous values from each thread, then reduce */
    double *maxMITh = new double[numTh];
    unsigned int *maxMICounterTh = new unsigned int[numTh];

    /* MPI communications */
    double_int_t sendbuf, recvbuf;
    unsigned int *selectedFeaturesMatrix = new unsigned int[featuresToSelect * noOfSamples];
    unsigned int globalFeatureIndex;

    unsigned int *conditionVector = new unsigned int[noOfSamples];
    unsigned int *outputFeatures = results->getOutputFeatures();
    double *featureScores = results->getFeatureScores();

    unsigned int i, j;

    for (i = 0; i < sizeOfMatrix; i++) {
        featureMIMatrix[i] = -1;
    }/*for featureMIMatrix - blank to -1*/

    /* init threads */
    for (i = 0; i < numTh; i++) {
        threads.emplace_back(condmiInitMITh, i, numTh, noOfSamples,
                             noOfFeatures, featureMatrix, classColumn,
                             selectedFeatures, classMI, &maxMICounterTh[i], &maxMITh[i]);
    }

    for (i = 0; i < numTh; i++) {
        threads[i].join();

        if (maxMITh[i] > maxMI || ((maxMITh[i] == maxMI) && (maxMICounterTh[i] < maxMICounter))) {
            maxMI = maxMITh[i];
            maxMICounter = maxMICounterTh[i];
        }

    }
    threads.clear();

    /* Send scores and rank to find which processor got the optimal feature*/
    sendbuf.rank = myRank;
    sendbuf.score = maxMI;
    MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

    /* Send feature data */
    if (myRank == recvbuf.rank) {
        copyArray(&featureMatrix[maxMICounter * noOfSamples], selectedFeaturesMatrix, noOfSamples);
        globalFeatureIndex = myOffset + maxMICounter;
        selectedFeatures[maxMICounter] = 1;
    }
    MPI_Bcast(&selectedFeaturesMatrix[0], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
    MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

    outputFeatures[0] = globalFeatureIndex;
    featureScores[0] = recvbuf.score;

    copyArray(&selectedFeaturesMatrix[0], conditionVector, noOfSamples);


//    double t = Utils::getSysTime();
//    Utils::log("Time to populate classMI array with %d threads: %lfs\n", numTh, Utils::getSysTime() - t);

    /*****************************************************************************
     ** We have populated the classMI array, and selected the highest
     ** MI feature as the first output feature
     ** Now we move into the DISR algorithm
     *****************************************************************************/

    double t_f, t_i;
    for (i = 1; i < featuresToSelect; i++) {
        t_i = Utils::getSysTime();

        score = 0.0;
        feature = -1;

        for (j = 0; j < numTh; j++) {
            threads.emplace_back(condmiTh, j, numTh, noOfSamples,
                                 noOfFeatures, featureMatrix, classColumn,
                                 outputFeatures, i, selectedFeatures, classMI,
                                 featureMIMatrix, &outputFeaturesTh[j],
                                 &featureScoresTh[j], selectedFeaturesMatrix, conditionVector);
        }

        /* wait for threads & find biggest score*/
        for (j = 0; j < numTh; j++) {
            threads[j].join();

            if (featureScoresTh[j] > score || ((featureScoresTh[j] == score) && (outputFeaturesTh[j] < feature))) {
                score = featureScoresTh[j];
                feature = outputFeaturesTh[j];
            }
        }
        threads.clear();

        t_f = Utils::getSysTime();
        //Utils::log("Feature %d: Work %f\n", i, t_f-t_i);
        t_i = Utils::getSysTime();

        /* Find best score */
        sendbuf.rank = myRank;
        sendbuf.score = score;
        MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

        /* Send feature data */
        if (myRank == recvbuf.rank) {
            if (feature != -1) {
                copyArray(&featureMatrix[feature * noOfSamples], &selectedFeaturesMatrix[i * noOfSamples],
                          noOfSamples);
                globalFeatureIndex = myOffset + feature;
                selectedFeatures[feature] = 1;
            } else {
                globalFeatureIndex = -1;
            }
        }
        MPI_Bcast(&selectedFeaturesMatrix[i * noOfSamples], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
        MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

        if (globalFeatureIndex != -1) {
            mergeArrays(&selectedFeaturesMatrix[i * noOfSamples], conditionVector, conditionVector, noOfSamples);
        }

        t_f = Utils::getSysTime();
        //Utils::log("Feature %d: Comm %f\n\n", i, t_f-t_i);

        featureScores[i] = recvbuf.score;
        outputFeatures[i] = globalFeatureIndex;

    }/*for the number of features to select*/

    delete[] conditionVector;
    delete[] maxMICounterTh;
    delete[] maxMITh;
    delete[] featureScoresTh;
    delete[] outputFeaturesTh;
    delete[] classMI;
    delete[] selectedFeatures;
}

void CondMI::exec(double *featureWeights, int numTh, int myRank, int myOffset) {

    char *selectedFeatures = new char[noOfFeatures];
    /* holds the class MI values */
    double *classMI = new double[noOfFeatures];

    /* holds MI for each pair of features */
    int sizeOfMatrix = featuresToSelect * noOfFeatures;
    double *featureMIMatrix = new double[sizeOfMatrix];

    double score;
    unsigned int feature;

    /* storage for thread results */
    std::vector<std::thread> threads;
    unsigned int *outputFeaturesTh = new unsigned int[numTh];
    double *featureScoresTh = new double[numTh];

    /* find feature with higher MI */
    /* ensure a feature is always picked*/
    double maxMI = -1.0;
    unsigned int maxMICounter = noOfFeatures;
    /* hold previous values from each thread, then reduce */
    double *maxMITh = new double[numTh];
    unsigned int *maxMICounterTh = new unsigned int[numTh];

    /* MPI communications */
    double_int_t sendbuf, recvbuf;
    unsigned int *selectedFeaturesMatrix = new unsigned int[featuresToSelect * noOfSamples];
    unsigned int globalFeatureIndex;

    unsigned int *conditionVector = new unsigned int[noOfSamples];
    unsigned int *outputFeatures = results->getOutputFeatures();
    double *featureScores = results->getFeatureScores();

    unsigned int i, j;

    for (i = 0; i < sizeOfMatrix; i++) {
        featureMIMatrix[i] = -1;
    }/*for featureMIMatrix - blank to -1*/

    /* init threads */
    for (i = 0; i < numTh; i++) {
        threads.emplace_back(condmiwInitMITh, i, numTh, noOfSamples,
                             noOfFeatures, featureMatrix, classColumn,
                             selectedFeatures, classMI, &maxMICounterTh[i], &maxMITh[i], featureWeights);
    }

    for (i = 0; i < numTh; i++) {
        threads[i].join();

        if (maxMITh[i] > maxMI || ((maxMITh[i] == maxMI) && (maxMICounterTh[i] < maxMICounter))) {
            maxMI = maxMITh[i];
            maxMICounter = maxMICounterTh[i];
        }

    }
    threads.clear();

    /* Send scores and rank to find which processor got the optimal feature*/
    sendbuf.rank = myRank;
    sendbuf.score = maxMI;
    MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

    /* Send feature data */
    if (myRank == recvbuf.rank) {
        copyArray(&featureMatrix[maxMICounter * noOfSamples], selectedFeaturesMatrix, noOfSamples);
        globalFeatureIndex = myOffset + maxMICounter;
        selectedFeatures[maxMICounter] = 1;
    }
    MPI_Bcast(&selectedFeaturesMatrix[0], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
    MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

    outputFeatures[0] = globalFeatureIndex;
    featureScores[0] = recvbuf.score;

    copyArray(&selectedFeaturesMatrix[0], conditionVector, noOfSamples);


//    double t = Utils::getSysTime();
//    Utils::log("Time to populate classMI array with %d threads: %lfs\n", numTh, Utils::getSysTime() - t);

    /*****************************************************************************
     ** We have populated the classMI array, and selected the highest
     ** MI feature as the first output feature
     ** Now we move into the DISR algorithm
     *****************************************************************************/

    double t_f, t_i;
    for (i = 1; i < featuresToSelect; i++) {
        t_i = Utils::getSysTime();

        score = 0.0;
        feature = -1;

        for (j = 0; j < numTh; j++) {
            threads.emplace_back(condmiwTh, j, numTh, noOfSamples,
                                 noOfFeatures, featureMatrix, classColumn,
                                 outputFeatures, i, selectedFeatures, classMI,
                                 featureMIMatrix, &outputFeaturesTh[j],
                                 &featureScoresTh[j], selectedFeaturesMatrix, conditionVector, featureWeights);
        }

        /* wait for threads & find biggest score*/
        for (j = 0; j < numTh; j++) {
            threads[j].join();

            if (featureScoresTh[j] > score || ((featureScoresTh[j] == score) && (outputFeaturesTh[j] < feature))) {
                score = featureScoresTh[j];
                feature = outputFeaturesTh[j];
            }
        }
        threads.clear();

        t_f = Utils::getSysTime();
        //Utils::log("Feature %d: Work %f\n", i, t_f-t_i);
        t_i = Utils::getSysTime();

        /* Find best score */
        sendbuf.rank = myRank;
        sendbuf.score = score;
        MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

        /* Send feature data */
        if (myRank == recvbuf.rank) {
            if (feature != -1) {
                copyArray(&featureMatrix[feature * noOfSamples], &selectedFeaturesMatrix[i * noOfSamples],
                          noOfSamples);
                globalFeatureIndex = myOffset + feature;
                selectedFeatures[feature] = 1;
            } else {
                globalFeatureIndex = -1;
            }
        }
        MPI_Bcast(&selectedFeaturesMatrix[i * noOfSamples], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
        MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

        if (globalFeatureIndex != -1) {
            mergeArrays(&selectedFeaturesMatrix[i * noOfSamples], conditionVector, conditionVector, noOfSamples);
        }

        t_f = Utils::getSysTime();
        //Utils::log("Feature %d: Comm %f\n\n", i, t_f-t_i);

        featureScores[i] = recvbuf.score;
        outputFeatures[i] = globalFeatureIndex;

    }/*for the number of features to select*/

    delete[] conditionVector;
    delete[] maxMICounterTh;
    delete[] maxMITh;
    delete[] featureScoresTh;
    delete[] outputFeaturesTh;
    delete[] classMI;
    delete[] selectedFeatures;
}