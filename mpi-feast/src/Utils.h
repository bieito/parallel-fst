#ifndef UTILS_H_
#define UTILS_H_

#include "Macros.h"

/* MPI Communication */
typedef struct double_int_t {
    double score;
    int rank;
} double_int_t;

class Utils {
public:
    static void log(const char *args, ...);

    static void exit(const char *args, ...);

    static double getSysTime();
};

#endif /* UTILS_H_ */
