#include "InputMat.h"

InputMat::InputMat() {
    _nFeat = 0;
    _nSamples = 0;
    _data = NULL;
    _classes = NULL;
}

InputMat::InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<unsigned int *> sampleData,
                   std::vector<int> classes, unsigned int numProcs) {
    int myRank;
    unsigned int oneMore, blockSize;
    unsigned int *allData;
    int *sendcounts, *displs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    MPI_Bcast(&nSamples, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
    MPI_Bcast(&nFeat, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    _nFeat = nFeat;
    _nSamples = nSamples;
    _classes = new unsigned int[nSamples];

    if (IS_ROOT(myRank)) {
        allData = new unsigned int[nFeat * nSamples];
        for (unsigned int i = 0; i < nFeat; i++) {
            for (unsigned int j = 0; j < nSamples; j++) {
                allData[i * _nSamples + j] = sampleData[j][i];
            }
        }

        // All the classes start in 0
        int minClass = classes[0];
        for (unsigned int i = 1; i < nSamples; i++) {
            if (classes[i] < minClass) {
                minClass = classes[i];
            }
        }

        if (minClass > 0) {
            minClass = 0;
        }

        for (unsigned int i = 0; i < nSamples; i++) {
            _classes[i] = classes[i] - minClass;
        }
    }

    /* Share data */
    /* Block sizes & Offsets */
    oneMore = nFeat % numProcs;
    blockSize = (nFeat / numProcs);
    if (myRank < oneMore) {
        _nLocalFeat = blockSize + 1;
        _myOffset = (blockSize + 1) * myRank;
    } else {
        _nLocalFeat = blockSize;
        _myOffset = oneMore + blockSize * myRank;
    }

    /* Data */
    if (IS_ROOT(myRank)) {
        sendcounts = new int[numProcs];
        displs = new int[numProcs];

        for (int i = 0; i < oneMore; i++) {
            sendcounts[i] = (blockSize + 1) * nSamples;
            displs[i] = (blockSize + 1) * i * nSamples;
        }

        for (int i = oneMore; i < numProcs; i++) {
            sendcounts[i] = blockSize * nSamples;
            displs[i] = (oneMore + blockSize * i) * nSamples;
        }
    }

    MPI_Bcast(_classes, nSamples, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    _data = new unsigned int[_nLocalFeat * nSamples];
    MPI_Scatterv(allData, sendcounts, displs, MPI_UNSIGNED, _data, _nLocalFeat * nSamples, MPI_UNSIGNED,
                 ROOT_ID, MPI_COMM_WORLD);

    if (IS_ROOT(myRank)) {
        delete[] displs;
        delete[] sendcounts;
        delete[] allData;
    }
}

InputMat::InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<double *> sampleData, std::vector<int> classes,
                   unsigned int discBins, unsigned int numProcs) {
    int myRank;
    unsigned int oneMore, blockSize;
    unsigned int *allData;
    int *sendcounts, *displs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    MPI_Bcast(&nSamples, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
    MPI_Bcast(&nFeat, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    _nFeat = nFeat;
    _nSamples = nSamples;
    _classes = new unsigned int[nSamples];

    if (IS_ROOT(myRank)) {
        double *auxData = new double[nSamples];
        allData = new unsigned int[nFeat * nSamples];

        for (unsigned int i = 0; i < nFeat; i++) {
            for (unsigned int j = 0; j < nSamples; j++) {
                auxData[j] = sampleData[j][i];
            }
            _discretize(auxData, &allData[i * nSamples], discBins);
        }

        delete[] auxData;

        _classes = new unsigned int[nSamples];
        // All the classes start in 0
        int minClass = classes[0];
        for (unsigned int i = 1; i < nSamples; i++) {
            if (classes[i] < minClass) {
                minClass = classes[i];
            }
        }

        if (minClass > 0) {
            minClass = 0;
        }

        for (unsigned int i = 0; i < nSamples; i++) {
            _classes[i] = classes[i] - minClass;
        }
    }

    /* Share data */
    /* Block sizes & Offsets */
    oneMore = nFeat % numProcs;
    blockSize = (nFeat / numProcs);
    if (myRank < oneMore) {
        _nLocalFeat = blockSize + 1;
        _myOffset = (blockSize + 1) * myRank;
    } else {
        _nLocalFeat = blockSize;
        _myOffset = oneMore + blockSize * myRank;
    }

    /* Data */
    if (IS_ROOT(myRank)) {
        sendcounts = new int[numProcs];
        displs = new int[numProcs];

        for (int i = 0; i < oneMore; i++) {
            sendcounts[i] = (blockSize + 1) * nSamples;
            displs[i] = (blockSize + 1) * i * nSamples;
        }

        for (int i = oneMore; i < numProcs; i++) {
            sendcounts[i] = blockSize * nSamples;
            displs[i] = (oneMore + blockSize * i) * nSamples;
        }
    }

    MPI_Bcast(_classes, nSamples, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    _data = new unsigned int[_nLocalFeat * nSamples];
    MPI_Scatterv(allData, sendcounts, displs, MPI_UNSIGNED, _data, _nLocalFeat * nSamples, MPI_UNSIGNED,
                 ROOT_ID, MPI_COMM_WORLD);

    if (IS_ROOT(myRank)) {
        delete[] displs;
        delete[] sendcounts;
        delete[] allData;
    }
}

InputMat::InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<unsigned int *> sampleData,
                   std::vector<int> classes, std::vector<unsigned int *> indexes, unsigned int numProcs) {
    int myRank;
    unsigned int bufLen;
    unsigned int *dataBuf;
    unsigned int *indexBuf;
    std::vector<unsigned int *> sampleDataRec;
    std::vector<unsigned int *> indexesRec;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    MPI_Bcast(&nSamples, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
    MPI_Bcast(&nFeat, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    double st_time;
    if (IS_ROOT(myRank)) {
        st_time = Utils::getSysTime();
    }

    if (!IS_ROOT(myRank)) {
        dataBuf = new unsigned int[nFeat];
        indexBuf = new unsigned int[nFeat + 1];
    }

    for (unsigned int i = 0; i < nSamples; i++) {
        if (IS_ROOT(myRank)) {
            bufLen = indexes[i][0];
            dataBuf = sampleData[i];
            indexBuf = indexes[i];
        }
        MPI_Bcast(&bufLen, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
        MPI_Bcast(dataBuf, bufLen, MPI_DOUBLE, ROOT_ID, MPI_COMM_WORLD);
        MPI_Bcast(indexBuf, bufLen + 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

        sampleDataRec.push_back(copyArray(dataBuf, bufLen));
        indexesRec.push_back(copyArray(indexBuf, bufLen + 1));
    }

    if (!IS_ROOT(myRank)) {
        delete[] dataBuf;
        delete[] indexBuf;
    }

    if (IS_ROOT(myRank)) {
        Utils::log("INFO in InputMat: %fs to share all data\n", Utils::getSysTime() - st_time);
    }

    /* Select data & Expand it */
    /* Block sizes & Offsets */
    unsigned int oneMore, blockSize, localDataLength, myOffset;

    oneMore = nFeat % numProcs;
    blockSize = (nFeat / numProcs);
    if (myRank < oneMore) {
        localDataLength = blockSize + 1;
        myOffset = (blockSize + 1) * myRank;
    } else {
        localDataLength = blockSize;
        myOffset = oneMore + blockSize * myRank;
    }

    _nFeat = nFeat;
    _nLocalFeat = localDataLength;
    _myOffset = myOffset;
    _nSamples = nSamples;
    _data = new unsigned int[((unsigned long long int) localDataLength) * ((unsigned long long int) nSamples)];

    // Initialize everything to 0 in case sparse
    for (unsigned long long int i = 0; i < localDataLength; i++) {
        for (unsigned long long int j = 0; j < nSamples; j++) {
            _data[i * ((unsigned long long int) nSamples) + j] = 0;
        }
    }

    for (unsigned int j = 0; j < nSamples; j++) {
        unsigned int rowFeat = indexesRec[j][0];
        for (unsigned int i = 0; i < rowFeat; i++) {
            if (indexesRec[j][i + 1] < myOffset) {
                // skip to in range features
                continue;
            } else if (indexesRec[j][i + 1] >= (myOffset + localDataLength)) {
                // stop when out of range
                break;
            } else {
                _data[(indexesRec[j][i + 1] - myOffset) * _nSamples + j] = sampleDataRec[j][i];
            }
        }
    }

    _classes = new unsigned int[nSamples];
    if (IS_ROOT(myRank)) {
        // All the classes start in 0
        int minClass = classes[0];
        for (unsigned int i = 1; i < nSamples; i++) {
            if (classes[i] < minClass) {
                minClass = classes[i];
            }
        }

        if (minClass > 0) {
            minClass = 0;
        }

        for (unsigned int i = 0; i < nSamples; i++) {
            _classes[i] = classes[i] - minClass;
        }
    }
    MPI_Bcast(_classes, nSamples, MPI_INT, ROOT_ID, MPI_COMM_WORLD);

}

InputMat::InputMat(unsigned int nFeat, unsigned int nSamples, std::vector<double *> sampleData, std::vector<int> classes,
                   std::vector<unsigned int *> indexes, unsigned int discBins, unsigned int numProcs) {

    int myRank;
    unsigned int bufLen;
    double *dataBuf;
    unsigned int *indexBuf;
    std::vector<double *> sampleDataRec;
    std::vector<unsigned int *> indexesRec;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    MPI_Bcast(&nSamples, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
    MPI_Bcast(&nFeat, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    double st_time;
    if (IS_ROOT(myRank)) {
        st_time = Utils::getSysTime();
    }

    if (!IS_ROOT(myRank)) {
        dataBuf = new double[nFeat];
        indexBuf = new unsigned int[nFeat + 1];
    }

    for (unsigned int i = 0; i < nSamples; i++) {
        if (IS_ROOT(myRank)) {
            bufLen = indexes[i][0];
            dataBuf = sampleData[i];
            indexBuf = indexes[i];
        }
        MPI_Bcast(&bufLen, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
        MPI_Bcast(dataBuf, bufLen, MPI_DOUBLE, ROOT_ID, MPI_COMM_WORLD);
        MPI_Bcast(indexBuf, bufLen + 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

        sampleDataRec.push_back(copyArray(dataBuf, bufLen));
        indexesRec.push_back(copyArray(indexBuf, bufLen + 1));

    }

    if (!IS_ROOT(myRank)) {
        delete[] dataBuf;
        delete[] indexBuf;
    }

    if (IS_ROOT(myRank)) {
        Utils::log("INFO in InputMat: %fs to share all data\n", Utils::getSysTime() - st_time);
    }

    /* Select data & Expand it */
    /* Block sizes & Offsets */
    unsigned int oneMore, blockSize, localDataLength, myOffset;

    oneMore = nFeat % numProcs;
    blockSize = (nFeat / numProcs);
    if (myRank < oneMore) {
        localDataLength = blockSize + 1;
        myOffset = (blockSize + 1) * myRank;
    } else {
        localDataLength = blockSize;
        myOffset = oneMore + blockSize * myRank;
    }

    _nFeat = nFeat;
    _nLocalFeat = localDataLength;
    _myOffset = myOffset;
    _nSamples = nSamples;
//    Utils::log("[P %2d] SIZE %d features * %d samples * %d Bytes\n", myRank, _nLocalFeat, _nSamples, sizeof(double));
    double *auxData = new double[((unsigned long long int) localDataLength) * ((unsigned long long int) nSamples)];
    _data = new unsigned int[((unsigned long long int) localDataLength) * ((unsigned long long int) nSamples)];

    // Initialize everything to 0 in case sparse
    for (unsigned long long int i = 0; i < localDataLength; i++) {
        for (unsigned long long int j = 0; j < nSamples; j++) {
            auxData[i * ((unsigned long long int) nSamples) + j] = 0;
        }
    }

    for (unsigned int j = 0; j < nSamples; j++) {
        unsigned int rowFeat = indexesRec[j][0];
        for (unsigned int i = 0; i < rowFeat; i++) {
            if (indexesRec[j][i + 1] < myOffset) {
                // skip to in range features
                continue;
            } else if (indexesRec[j][i + 1] >= (myOffset + localDataLength)) {
                // stop when out of range
                break;
            } else {
                auxData[(indexesRec[j][i + 1] - myOffset) * _nSamples + j] = sampleDataRec[j][i];
            }
        }
    }

    for (unsigned long long int i = 0; i < localDataLength; i++) {
        _discretize(&auxData[i * ((unsigned long long int) nSamples)], &_data[i * ((unsigned long long int) nSamples)],
                    discBins);
    }

    delete[] auxData;

    _classes = new unsigned int[nSamples];
    if (IS_ROOT(myRank)) {
        // All the classes start in 0
        int minClass = classes[0];
        for (unsigned int i = 1; i < nSamples; i++) {
            if (classes[i] < minClass) {
                minClass = classes[i];
            }
        }

        if (minClass > 0) {
            minClass = 0;
        }

        for (unsigned int i = 0; i < nSamples; i++) {
            _classes[i] = classes[i] - minClass;
        }
    }
    MPI_Bcast(_classes, nSamples, MPI_INT, ROOT_ID, MPI_COMM_WORLD);

}

InputMat::~InputMat() {
    _nFeat = 0;
    _nLocalFeat = 0;
    _nSamples = 0;

    if (_data != NULL) {
        delete[] _data;
    }

    if (_classes != NULL) {
        delete[] _classes;
    }
}

unsigned int InputMat::_maxState(unsigned int *vector, unsigned int vectorLength) {
    unsigned int i, max;

    max = 0;
    for (i = 0; i < vectorLength; i++) {
        if (vector[i] > max) {
            max = vector[i];
        }
    }

    return max + 1;
}

void InputMat::mapValues() {
    unsigned int maxSt;
    unsigned int *featureMap;
    unsigned int mapCounter;

    for (unsigned int i = 0; i < _nLocalFeat; i++) {
        // For each feature
        maxSt = _maxState(&_data[i * _nSamples], _nSamples);
        featureMap = new unsigned int[maxSt]();
        mapCounter = 1;
        for (unsigned int j = 0; j < _nSamples; j++) {
            // For each sample
            if (featureMap[_data[i * _nSamples + j]] == 0) {
                featureMap[_data[i * _nSamples + j]] = mapCounter;
                mapCounter++;
            }
            _data[i * _nSamples + j] = featureMap[_data[i * _nSamples + j]] - 1;
        }
        delete[] featureMap;
    }

}

void InputMat::_discretize(double *dData, unsigned int *data, unsigned int discBins) {

    double minVal = dData[0];
    double maxVal = dData[0];

    // First detect the minimum and maximum values
    for (int i = 1; i < _nSamples; i++) {

        if (dData[i] < minVal) {
            minVal = dData[i];
        }

        if (dData[i] > maxVal) {
            maxVal = dData[i];
        }
    }

    double binSize = (maxVal - minVal) / ((double) discBins);

    for (int i = 0; i < _nSamples; i++) {
        unsigned int currentVal = 0;
        double binLim = minVal + binSize;
        while ((dData[i] > binLim) && (currentVal < discBins - 1)) {
            currentVal++;
            binLim += binSize;
        }

        data[i] = currentVal;
    }
}