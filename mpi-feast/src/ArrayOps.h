#ifndef PARALLEL_FST_ARRAYOPERATION_H
#define PARALLEL_FST_ARRAYOPERATION_H

/* Copy */
template<typename T>
inline void copyArray(T *src, T *dst, unsigned int size) {
    for (unsigned int i = 0; i < size; i++) {
        dst[i] = src[i];
    }
}

template<typename T>
inline T *copyArray(T *src, unsigned int size) {
    T *aux = new T[size];

    copyArray(src, aux, size);

    return aux;
}

/* Matrix operations */
template<typename T>
inline T **arrayToMatrix(T *src, unsigned int rows, unsigned int cols) {
    T **aux = new T *[rows];

    for (int i = 0; i < rows; i++) {
        aux[i] = new T[cols];
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            aux[i][j] = src[i * cols + j];
        }
    }

    return aux;
}

template<typename T>
inline T *matrixToArray(T **src, unsigned int rows, unsigned int cols) {
    T *aux = new T[rows * cols];

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            aux[i * cols + j] = src[i][j];
        }
    }

    return aux;
}

template<typename T>
inline void deleteMatrix(T **src, unsigned int rows) {
    for (int i = 0; i < rows; i++) {
        delete[] src[i];
    }
    delete[] src;
}

/* Print */
template<typename T>
inline void printArray(T *src, unsigned int size) {
    std::cout << "[Array at " << &src << "]" << std::endl;
    for (unsigned int i = 0; i < size; i++) {
        std::cout << src[i] << "  ";
    }
    std::cout << std::endl;
}

template<typename T>
inline void printArray(T *src, unsigned int rows, unsigned int cols) {
    std::cout << "[Array at " << &src << "]" << std::endl;
    for (unsigned int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << src[i * cols + j] << "  ";
        }
        std::cout << std::endl;
    }
}

#endif //PARALLEL_FST_ARRAYOPERATION_H
