#ifndef OPTIONS_H_
#define OPTIONS_H_

#include "Utils.h"
#include <mpi-feast/AlgorithmSelector.h>

class Options {
public:
    Options();

    virtual ~Options();

    /*virtual functions*/
    /*void printUsage();*/

    /*bool parse(int argc, char *argv[]);*/

    std::string getInputFileName() {
        return _inputFileName;
    }

    std::string getWeightsFileName() {
        return _weightsFileName;
    }

    std::string getOutputFileName() {
        return _outFileName;
    }

    unsigned int getNumDiscBins() const {
        return _discBins;
    }

    unsigned int getNumSelecFeat() const {
        return _numSelecFeat;
    }

    unsigned int getNumTh() const {
        return _numTh;
    }

    unsigned int getNumProcs() const {
        return _numProcs;
    }

    bool doRangeCompression() const {
        return _doRangeCompression;
    }

    bool getOriginal() const {
        return _original;
    }

    AlgorithmSelector::AlgorithmType getAlgorithm() {
        return _algorithm;
    }

    double getGamma() const {
        return _gamma;
    }

    double getBeta() const {
        return _beta;
    }

    void setInputFileName(std::string input_path) {
        _inputFileName = input_path;
    }

    void setWeightsFileName(std::string weights_path) {
        _weightsFileName = weights_path;
    }

    void setOutputFileName(std::string output_path) {
        _outFileName = output_path;
    }

    void setNumDiscBins(unsigned int disc_bins) {
        _discBins = disc_bins;
    }

    void setNumSelecFeat(unsigned int numSelecFeat) {
        _numSelecFeat = numSelecFeat;
    }

    void setNumThreads(unsigned int numThreads) {
        _numTh = numThreads;
    }

    void setNumProcs(unsigned int numProcs) {
        _numProcs = numProcs;
    }

    void setRangeCompression(bool doRangeCompression) {
        _doRangeCompression = doRangeCompression;
    }

    void setOriginal(bool useOriginalImplementation) {
        _original = useOriginalImplementation;
    }

    void setAlgorithm(const std::string &algorithm) {
        _algorithm = AlgorithmSelector::strToAlgorithm(algorithm);
        if (_algorithm == AlgorithmSelector::NONE) {
            throw std::invalid_argument("Algorithm name unknown");
        }
    }

    void setBetaGamma(double beta, double gamma) {
        _beta = beta;
        _gamma = gamma;
    }

    void broadcast();

private:
    std::string _inputFileName;
    std::string _weightsFileName;
    std::string _outFileName;

    AlgorithmSelector::AlgorithmType _algorithm; // Algorithm to be used
    unsigned int _discBins; // Number of bins if discretization is necessary
    unsigned int _numSelecFeat; // Number of features to select
    bool _doRangeCompression; // If state map should be done before applying algorithm
    bool _original; // If algorithm should be used with its original implementation

    unsigned int _numTh;
    unsigned int _numProcs;

    // some algorithms require parameters
    double _beta;
    double _gamma;
};

#endif /* OPTIONS_H_ */
