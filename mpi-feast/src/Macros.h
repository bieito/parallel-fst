#ifndef MACROS_H_
#define MACROS_H_

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <mpi.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#include <array>
#include <fstream>
#include <iostream>
#include <mutex>
#include <set>
#include <thread>
#include <vector>

#define PROGRAM_NAME "mpi-feast"
#define ROOT_ID 0
#define IS_ROOT(ID) (ID == ROOT_ID)

#endif /* MACROS_H_ */
