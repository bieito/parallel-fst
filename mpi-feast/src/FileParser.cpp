#include "FileParser.h"

FileParser *FileParser::getFileParser(const std::string &path) {
    FileParser *parser;

    // Check the extension
    if ((path.find(".arff") != std::string::npos) ||
        (path.find(".ARFF") != std::string::npos)) {
        parser = new ArffFileParser(path.c_str());
    } else if ((path.find(".csv") != std::string::npos) ||
               (path.find(".CSV") != std::string::npos)) {
        parser = new CsvFileParser(path.c_str());
    } else if ((path.find(".libsvm") != std::string::npos) ||
               (path.find(".LIBSVM") != std::string::npos)) {
        parser = new LibsvmFileParser(path.c_str());
    } else {
        parser = nullptr;
    }

    return parser;
}

FileParser::FileParser(const char *path) {
    MPI_Comm_rank(MPI_COMM_WORLD, &workerId);

    if (IS_ROOT(workerId)) {
        _file.open(path);

        if (!_file.is_open()) {
            Utils::exit("ERROR in FileParser: file %s could not be opened\n", path);
        }
    }
}

void FileParser::readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs) {
    Utils::exit("ERROR in FileParser: You should read a matrix with a file parser for an specific format\n");
}

void ArffFileParser::readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs) {
    // 0 -> Not even relation
    // 1 -> Found relation but not attributes
    // 2 -> Found attributes but not data
    // 3 -> All parts found
    int status = 0;
    std::string line;

    unsigned int nFeatures = 0;
    std::vector<NominalParameter *> nominals;

    // Type depends on whether the input is already discrete
    std::vector<unsigned int *> vectorU;
    std::vector<double *> vectorD;
    std::vector<int> classes;

    // Get data
    unsigned int nSamples = 0;
    std::string auxStr;
    unsigned int *samplesU;
    double *samplesD;

    if (IS_ROOT(workerId)) {
        // Jump until the relation is found
        while (!_file.eof() && (status == 0)) {
            getline(_file, line);

            if ((line[0] != '%') && (line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
                // In this point we should find the word relation
                if ((line.compare(0, 9, "@relation") != 0) && (line.compare(0, 9, "@RELATION") != 0)) {
                    Utils::exit("ERROR in ArffFileParser: The first section should be relation\n");
                }
                status = 1;
            }
        }

        // Count the attributes until data is found
        while (!_file.eof() && (status == 1)) {
            getline(_file, line);

            if ((line[0] != '%') && (line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
                if ((line.compare(0, 10, "@attribute") == 0) || (line.compare(0, 10, "@ATTRIBUTE") == 0)) {
                    // Numerical value
                    int discountLetters = 0;
                    if (line[line.length() - 1] == '\r') {
                        discountLetters++;
                    }

                    if (line[line.length() - 1 - discountLetters] == ' ') {
                        discountLetters++;
                    }

                    if ((line.compare(line.length() - 7 - discountLetters, 7, "NUMERIC") == 0) ||
                        (line.compare(line.length() - 7 - discountLetters, 7, "numeric") == 0) ||
                        (line.compare(line.length() - 4 - discountLetters, 4, "REAL") == 0) ||
                        (line.compare(line.length() - 4 - discountLetters, 4, "real") == 0) ||
                        (line.compare(line.length() - 7 - discountLetters, 7, "INTEGER") == 0) ||
                        (line.compare(line.length() - 7 - discountLetters, 7, "integer") == 0)) {

                        nominals.push_back(NULL);
                    } else {
                        int posBracket = line.find("{");

                        // It is not a valid attribute
                        if (posBracket == std::string::npos) {
                            Utils::exit("ERROR in ArffFileParser: The line %s does not represent a correct attribute\n",
                                        line.c_str());
                        } else { // Nominal attribute
                            if (line[line.length() - 1] == '\r') { // Windows file with \r at the end
                                nominals.push_back(
                                        new NominalParameter(
                                                line.substr(posBracket + 1, line.length() - posBracket - 3)));
                            } else {
                                nominals.push_back(
                                        new NominalParameter(
                                                line.substr(posBracket + 1, line.length() - posBracket - 2)));
                            }
                        }
                    }

                    nFeatures++;
                } else if ((line.compare("@data") == 0) || (line.compare("@DATA") == 0) ||
                           (line.compare("@data\r") == 0) ||
                           (line.compare("@DATA\r") == 0)) { // The attributes have finished
                    status = 2;
                    nFeatures--; // The last attribute is the class
                    Utils::log("INFO in ArffFileParser: %u features and 1 class found\n", nFeatures);
                } else {
                    Utils::exit("ERROR in ArffFileParser: The line %s when @attribute or @data is expected\n",
                                line.c_str());
                }
            }
        }

        while (!_file.eof()) {
            getline(_file, line);

            if ((line[0] != '%') && (line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
                int iniSub = 0;
                int endSub;

                if (discBins == 0) {
                    samplesU = new unsigned int[nFeatures];
                } else {
                    samplesD = new double[nFeatures];
                }

                for (unsigned int i = 0; i < nFeatures; i++) {
                    // Get the value as a string
                    endSub = line.find(',', iniSub);
                    if (endSub == std::string::npos) {
                        Utils::exit("ERROR in ArffFileParser: line %s has less features than %u\n", line.c_str(),
                                    nFeatures);
                    }
                    auxStr = line.substr(iniSub, endSub - iniSub);
                    iniSub = endSub + 1;

                    // In case it is a number
                    if (nominals[i] == NULL) {
                        if (discBins == 0) {
                            samplesU[i] = stoul(auxStr);
                        } else {
                            samplesD[i] = stod(auxStr);
                        }
                    } else {
                        unsigned int val = nominals[i]->getVal(auxStr);

                        if (discBins == 0) {
                            samplesU[i] = val;
                        } else {
                            samplesD[i] = val;
                        }
                    }
                }

                if (discBins == 0) {
                    vectorU.push_back(samplesU);
                } else {
                    vectorD.push_back(samplesD);
                }

                // The class
                auxStr = line.substr(iniSub);

                // Remove the \r in case of windows files
                if (auxStr[auxStr.length() - 1] == '\r') {
                    auxStr = auxStr.substr(0, auxStr.length() - 1);
                }

                // In case it is a number
                if (nominals[nFeatures] == NULL) {
                    classes.push_back(stoi(auxStr));
                } else {
                    unsigned int val = nominals[nFeatures]->getVal(auxStr);
                    classes.push_back(val);
                }

                nSamples++;
            }
        }

        Utils::log("INFO in ArffFileParser: %u samples\n", nSamples);
    }

    if (discBins == 0) {
        mat = new InputMat(nFeatures, nSamples, vectorU, classes, numProcs);
    } else {
        mat = new InputMat(nFeatures, nSamples, vectorD, classes, discBins, numProcs);
    }

    if (IS_ROOT(workerId)) {
        if (discBins == 0) {
            for (unsigned int i = 0; i < nSamples; i++) {
                delete[] vectorU[i];
            }
        } else {
            for (unsigned int i = 0; i < nSamples; i++) {
                delete[] vectorD[i];
            }
        }

        nominals.clear();
        classes.clear();
        vectorD.clear();
        vectorU.clear();
    }
}

void CsvFileParser::readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs) {

    std::string line;
    unsigned int nFeatures = 0, nSamples = 0;

    // Type depends on whether the input is already discrete
    std::vector<unsigned int *> vectorU;
    std::vector<double *> vectorD;
    std::vector<int> classes;

    std::string auxStr;
    unsigned int *samplesU;
    double *samplesD;

    if (IS_ROOT(workerId)) {
        while (!_file.eof()) {
            getline(_file, line);

            if ((line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
                if (nFeatures == 0) { // The first row to know the number of features
                    int auxPos = -1;
                    while ((auxPos = line.find(",", auxPos + 1)) != std::string::npos) {
                        nFeatures++;
                    }
                    Utils::log("INFO in CsvFileParser: %u features and 1 class found\n", nFeatures);
                }

                // Get the values of the line
                int iniSub = 0;
                int endSub;

                if (discBins == 0) {
                    samplesU = new unsigned int[nFeatures];
                } else {
                    samplesD = new double[nFeatures];
                }

                for (unsigned int i = 0; i < nFeatures; i++) {
                    // Get the value as a string
                    endSub = line.find(",", iniSub);
                    if (endSub == std::string::npos) {
                        Utils::exit("ERROR in CsvFileParser: line %s has less features than %u\n", line.c_str(),
                                    nFeatures);
                    }
                    auxStr = line.substr(iniSub, endSub - iniSub);
                    iniSub = endSub + 1;

                    if (discBins == 0) {
                        samplesU[i] = stoul(auxStr);
                    } else {
                        samplesD[i] = stod(auxStr);
                    }
                }

                if (discBins == 0) {
                    vectorU.push_back(samplesU);
                } else {
                    vectorD.push_back(samplesD);
                }

                // The class
                auxStr = line.substr(iniSub);

                // Remove the \r in case of windows files
                if (auxStr[auxStr.length() - 1] == '\r') {
                    auxStr = auxStr.substr(0, auxStr.length() - 1);
                }

                classes.push_back(stoi(auxStr));

                nSamples++;
            }
        }

        Utils::log("INFO in CsvFileParser: %u samples\n", nSamples);
    }

    if (discBins == 0) {
        mat = new InputMat(nFeatures, nSamples, vectorU, classes, numProcs);
    } else {
        mat = new InputMat(nFeatures, nSamples, vectorD, classes, discBins, numProcs);
    }

    if (IS_ROOT(workerId)) {
        if (discBins == 0) {
            for (unsigned int i = 0; i < nSamples; i++) {
                delete[] vectorU[i];
            }
        } else {
            for (unsigned int i = 0; i < nSamples; i++) {
                delete[] vectorD[i];
            }
        }

        classes.clear();
        vectorD.clear();
        vectorU.clear();
    }
}

void LibsvmFileParser::readInput(InputMat *&mat, unsigned int discBins, unsigned int numProcs) {

    std::string line;
    unsigned int rowFeatures, nSamples = 0;

    // Type depends on whether the input is already discrete
    std::vector<unsigned int *> vectorU;
    std::vector<double *> vectorD;
    std::vector<unsigned int *> vectorIndex;
    std::vector<int> classes;

    std::string auxStr;
    unsigned int *samplesU;
    double *samplesD;
    unsigned int *indexSamples;

    // This a format that can be sparse
    // In this variable we keep the maximum number of features
    unsigned int totalFeatures = 0;

    if (IS_ROOT(workerId)) {
        while (!_file.eof()) {
            getline(_file, line);

            if ((line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
                // The number of features expressed in the current row
                rowFeatures = 0;

                int auxPos = -1;
                while ((auxPos = line.find(":", auxPos + 1)) != std::string::npos) {
                    rowFeatures++;
                }

                if (discBins == 0) {
                    samplesU = new unsigned int[rowFeatures];
                } else {
                    samplesD = new double[rowFeatures];
                }
                // The first value of the indexes is the total number of features in the row
                indexSamples = new unsigned int[rowFeatures + 1];
                indexSamples[0] = rowFeatures;

                // First value of the row is the class
                auxStr = line.substr(0, line.find(' '));
                classes.push_back(stoi(auxStr));

                int iniPos = 0, endPos = 0;

                for (unsigned int i = 0; i < rowFeatures; i++) {
                    endPos = line.find(":", iniPos);

                    // Find the index of the sample
                    iniPos = endPos - 2;
                    while ((line[iniPos] != ' ') && (line[iniPos] != '\t')) {
                        iniPos--;
                    }

                    iniPos++;
                    auxStr = line.substr(iniPos, endPos - iniPos);
                    indexSamples[i + 1] = stoul(auxStr) - 1; // It starts in 1

                    // Find the value of the sample
                    iniPos = endPos + 1;
                    while ((line[endPos] != ' ') && (line[endPos] != '\t') && (line[endPos] != '\r') &&
                           (endPos < line.length())) {
                        endPos++;
                    }

                    auxStr = line.substr(iniPos, endPos - iniPos);

                    if (discBins == 0) {
                        samplesU[i] = stoul(auxStr);
                    } else {
                        samplesD[i] = stod(auxStr);
                    }
                }

                // The last index can be larger than in any previous row
                if (indexSamples[rowFeatures] + 1 > totalFeatures) {
                    totalFeatures = indexSamples[rowFeatures] + 1;
                }

                if (discBins == 0) {
                    vectorU.push_back(samplesU);
                } else {
                    vectorD.push_back(samplesD);
                }

                vectorIndex.push_back(indexSamples);

                nSamples++;
            }
        }

        Utils::log("INFO in LibsvmFileParser: %u features\n", totalFeatures);
        Utils::log("INFO in LibsvmFileParser: %u samples\n", nSamples);
    }

    if (discBins == 0) {
        mat = new InputMat(totalFeatures, nSamples, vectorU, classes, vectorIndex, numProcs);
    } else {
        mat = new InputMat(totalFeatures, nSamples, vectorD, classes, vectorIndex, discBins, numProcs);
    }

    if (IS_ROOT(workerId)) {
        if (discBins == 0) {
            for (unsigned int i = 0; i < nSamples; i++) {
                delete[] vectorU[i];
            }
        } else {
            for (unsigned int i = 0; i < nSamples; i++) {
                delete[] vectorD[i];
            }
        }

        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorIndex[i];
        }

        classes.clear();
        vectorD.clear();
        vectorU.clear();
        vectorIndex.clear();
    }
}
