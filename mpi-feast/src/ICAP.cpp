#include <mpi-feast/ICAP.h>

void icapInitMITh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples, unsigned int noOfFeatures,
                  unsigned int *featureMatrix, unsigned int *classColumn, char *selectedFeatures,
                  double *classMI, unsigned int *maxMIFeature, double *maxMIValue) {
    double maxMI = -1.0;
    unsigned int maxMICounter = 0;
    unsigned int i;

    for (i = myId; i < noOfFeatures; i += numTh) {
        selectedFeatures[i] = 0;
        classMI[i] = calcMutualInformation(&featureMatrix[i * noOfSamples], classColumn, noOfSamples);

        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        } /* if bigger than current maximum */
    }

    *maxMIValue = maxMI;
    *maxMIFeature = maxMICounter;
}

void icapTh(unsigned int myId, unsigned int numTh, unsigned int noOfSamples,
            unsigned int noOfFeatures, unsigned int *featureMatrix,
            unsigned int *classColumn, unsigned int *outputFeatures,
            unsigned int nSelectedFeatures, char *selectedFeatures,
            double *classMI,
            double *featureMIMatrix, unsigned int *outputFeature,
            double *featureScore, unsigned int *selectedFeaturesMatrix) {

    /* MI */
    unsigned int arrayPosition;
    double mi, cmi, totalFeatureMI;
    double currentScore;

    unsigned int *mergedVector = new unsigned int[noOfSamples]();

    unsigned int i = 0, x = 0;
    *featureScore = -DBL_MAX;

    for (i = myId; i < noOfFeatures; i += numTh) {
        /*if we haven't selected i*/
        if (selectedFeatures[i] == 0) {
            currentScore = classMI[i];
            totalFeatureMI = 0.0;

            for (x = 0; x < nSelectedFeatures; x++) {
                arrayPosition = x * noOfFeatures + i;
                if (featureMIMatrix[arrayPosition] == -1) {
                    /*
                     **double calcMutualInformation(uint *firstVector, uint *secondVector, int vectorLength);
                     **double calcJointEntropy(uint *firstVector, uint *secondVector, int vectorLength);
                     */

                    mi = calcMutualInformation(&selectedFeaturesMatrix[x * noOfSamples],
                                               &featureMatrix[i * noOfSamples],
                                               noOfSamples);

                    cmi = calcConditionalMutualInformation(&selectedFeaturesMatrix[x * noOfSamples],
                                                           &featureMatrix[i * noOfSamples],
                                                           classColumn,
                                                           noOfSamples);

                    featureMIMatrix[arrayPosition] = cmi - mi;

                }/*if not already known*/

                if (featureMIMatrix[arrayPosition] < 0) {
                    currentScore += featureMIMatrix[arrayPosition];
                }

            }/*for the number of already selected features*/

            if (currentScore > *featureScore) {
                *featureScore = currentScore;
                *outputFeature = i;
            }
        }/*if j is unselected*/
    }/*for number of features*/

    delete[] mergedVector;
}

void ICAP::exec() {
    unsigned int **matrix = arrayToMatrix(featureMatrix, noOfFeatures, noOfSamples);

    ::ICAP(featuresToSelect, noOfSamples, noOfFeatures, matrix, classColumn, results->getOutputFeatures(),
           results->getFeatureScores());

    deleteMatrix(matrix, noOfFeatures);
}

void ICAP::exec(double *featureWeights) {
    Utils::log(
            "INFO in ICAP: There does not exist a weighted version of ICAP, using unweighted implementation\n");
    exec();
}

void ICAP::exec(int numTh, int myRank, int myOffset) {

    char *selectedFeatures = new char[noOfFeatures];
    /* holds the class MI values */
    double *classMI = new double[noOfFeatures];

    /* holds MI for each pair of features */
    int sizeOfMatrix = featuresToSelect * noOfFeatures;
    double *featureMIMatrix = new double[sizeOfMatrix];

    double score;
    unsigned int feature;

    /* storage for thread results */
    std::vector<std::thread> threads;
    unsigned int *outputFeaturesTh = new unsigned int[numTh];
    double *featureScoresTh = new double[numTh];

    /* find feature with higher MI */
    /* ensure a feature is always picked*/
    double maxMI = -1.0;
    unsigned int maxMICounter = noOfFeatures;
    /* hold previous values from each thread, then reduce */
    double *maxMITh = new double[numTh];
    unsigned int *maxMICounterTh = new unsigned int[numTh];

    /* MPI communications */
    double_int_t sendbuf, recvbuf;
    unsigned int *selectedFeaturesMatrix = new unsigned int[featuresToSelect * noOfSamples];
    unsigned int globalFeatureIndex;

    unsigned int *outputFeatures = results->getOutputFeatures();
    double *featureScores = results->getFeatureScores();

    unsigned int i, j;

    for (i = 0; i < sizeOfMatrix; i++) {
        featureMIMatrix[i] = -1;
    }/*for featureMIMatrix - blank to -1*/

    /* init threads */
    for (i = 0; i < numTh; i++) {
        threads.emplace_back(icapInitMITh, i, numTh, noOfSamples,
                             noOfFeatures, featureMatrix, classColumn,
                             selectedFeatures, classMI, &maxMICounterTh[i], &maxMITh[i]);
    }

    for (i = 0; i < numTh; i++) {
        threads[i].join();

        if (maxMITh[i] > maxMI || ((maxMITh[i] == maxMI) && (maxMICounterTh[i] < maxMICounter))) {
            maxMI = maxMITh[i];
            maxMICounter = maxMICounterTh[i];
        }

    }
    threads.clear();

    /* Send scores and rank to find which processor got the optimal feature*/
    sendbuf.rank = myRank;
    sendbuf.score = maxMI;
    MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

    /* Send feature data */
    if (myRank == recvbuf.rank) {
        copyArray(&featureMatrix[maxMICounter * noOfSamples], selectedFeaturesMatrix, noOfSamples);
        globalFeatureIndex = myOffset + maxMICounter;
        selectedFeatures[maxMICounter] = 1;
    }
    MPI_Bcast(&selectedFeaturesMatrix[0], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
    MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

    outputFeatures[0] = globalFeatureIndex;
    featureScores[0] = recvbuf.score;

//    double t = Utils::getSysTime();
//    Utils::log("Time to populate classMI array with %d threads: %lfs\n", numTh, Utils::getSysTime() - t);

    /*****************************************************************************
     ** We have populated the classMI array, and selected the highest
     ** MI feature as the first output feature
     ** Now we move into the DISR algorithm
     *****************************************************************************/

    double t_f, t_i;
    for (i = 1; i < featuresToSelect; i++) {
        t_i = Utils::getSysTime();

        score = -DBL_MAX;
        feature = noOfFeatures;

        for (j = 0; j < numTh; j++) {
            threads.emplace_back(icapTh, j, numTh, noOfSamples,
                                 noOfFeatures, featureMatrix, classColumn,
                                 outputFeatures, i, selectedFeatures, classMI,
                                 featureMIMatrix, &outputFeaturesTh[j],
                                 &featureScoresTh[j], selectedFeaturesMatrix);
        }

        /* wait for threads & find biggest score*/
        for (j = 0; j < numTh; j++) {
            threads[j].join();

            if (featureScoresTh[j] > score || ((featureScoresTh[j] == score) && (outputFeaturesTh[j] < feature))) {
                score = featureScoresTh[j];
                feature = outputFeaturesTh[j];
            }
        }
        threads.clear();

        t_f = Utils::getSysTime();
        //Utils::log("Feature %d: Work %f\n", i, t_f-t_i);
        t_i = Utils::getSysTime();

        /* Find best score */
        sendbuf.rank = myRank;
        sendbuf.score = score;
        MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

        /* Send feature data */
        if (myRank == recvbuf.rank) {
            copyArray(&featureMatrix[feature * noOfSamples], &selectedFeaturesMatrix[i * noOfSamples],
                      noOfSamples);
            globalFeatureIndex = myOffset + feature;
            selectedFeatures[feature] = 1;
        }
        MPI_Bcast(&selectedFeaturesMatrix[i * noOfSamples], noOfSamples, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);
        MPI_Bcast(&globalFeatureIndex, 1, MPI_UNSIGNED, recvbuf.rank, MPI_COMM_WORLD);

        t_f = Utils::getSysTime();
        //Utils::log("Feature %d: Comm %f\n\n", i, t_f-t_i);

        featureScores[i] = recvbuf.score;
        outputFeatures[i] = globalFeatureIndex;

    }/*for the number of features to select*/

    delete[] maxMICounterTh;
    delete[] maxMITh;
    delete[] featureScoresTh;
    delete[] outputFeaturesTh;
    delete[] classMI;
    delete[] selectedFeatures;
}

void ICAP::exec(double *featureWeights, int numTh, int myRank, int myOffset) {
    if (IS_ROOT(myRank)) {
        Utils::log(
                "INFO in ICAP: There does not exist a weighted version of ICAP, using unweighted implementation\n");
    }
    exec(numTh, myRank, myOffset);
}