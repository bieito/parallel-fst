#include "Options.h"

Options::Options() {
    _discBins = 0;
    _numSelecFeat = 0;
    _numTh = std::thread::hardware_concurrency();
    _numProcs = 1;
    _doRangeCompression = false;
    _original = false;
    _algorithm = AlgorithmSelector::NONE;
}

Options::~Options() = default;

/*
 * To avoid some communications in addition to not being used by more than root process,
 * output file name is not shared
 *
 * Algorithm-specific parameters must be shared by the algorithm implementation (if needed)
 */
void Options::broadcast() {
    int myRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    /* Input file name */
    unsigned int stringLen;
    char *stringBuf;

    if (IS_ROOT(myRank)) {
        stringLen = _inputFileName.length();
    }
    MPI_Bcast(&stringLen, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    stringBuf = new char[stringLen];
    if (IS_ROOT(myRank)) {
        strcpy(stringBuf, _inputFileName.c_str());
    }
    MPI_Bcast(stringBuf, stringLen, MPI_CHAR, ROOT_ID, MPI_COMM_WORLD);

    _inputFileName = std::string(stringBuf);

    delete[] stringBuf;

    /* Weights file name */
    stringLen = 0;
    stringBuf = nullptr;

    if (IS_ROOT(myRank)) {
        stringLen = _weightsFileName.length();
    }
    MPI_Bcast(&stringLen, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    if (stringLen > 0) {
        stringBuf = new char[stringLen];
        if (IS_ROOT(myRank)) {
            strcpy(stringBuf, _weightsFileName.c_str());
        }
        MPI_Bcast(stringBuf, stringLen, MPI_CHAR, ROOT_ID, MPI_COMM_WORLD);

        _weightsFileName = std::string(stringBuf);

        delete[] stringBuf;
    } else {
        _weightsFileName.clear();
    }

    /* Algorithm */
    MPI_Bcast(&_algorithm, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    /* Number of bins to discretize */
    MPI_Bcast(&_discBins, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    /* Number of features to select */
    MPI_Bcast(&_numSelecFeat, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    /* Mapping */
    MPI_Bcast(&_doRangeCompression, 1, MPI_CXX_BOOL, ROOT_ID, MPI_COMM_WORLD);

    /* Original */
    MPI_Bcast(&_original, 1, MPI_CXX_BOOL, ROOT_ID, MPI_COMM_WORLD);

    /* Number of threads */
    MPI_Bcast(&_numTh, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);

    /* Number of procs */
    MPI_Bcast(&_numProcs, 1, MPI_UNSIGNED, ROOT_ID, MPI_COMM_WORLD);
}
