#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef ICAP_H_
#define ICAP_H_

class ICAP : public FeatureSelectionAlgorithm {
public:
    ICAP(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {};

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;
};

#endif //ICAP_H_
