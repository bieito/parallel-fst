#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef CMIM_H_
#define CMIM_H_

class CMIM : public FeatureSelectionAlgorithm {
public:
    CMIM(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {};

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;
};

#endif // CMIM_H_
