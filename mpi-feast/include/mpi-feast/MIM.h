#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef MIM_H_
#define MIM_H_

class MIM : public FeatureSelectionAlgorithm {
public:
    MIM(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {};

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;
};

#endif //MIM_H_
