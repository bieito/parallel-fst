#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef DISR_H_
#define DISR_H_

class DISR : public FeatureSelectionAlgorithm {
public:
    DISR(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {};

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;
};

#endif /* DISR_H_ */
