#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef BETAGAMMA_H_
#define BETAGAMMA_H_

class BetaGamma : public FeatureSelectionAlgorithm {
private:
    double beta;
    double gamma;
public:
    BetaGamma(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {
        beta = options.getBeta();
        gamma = options.getGamma();
    };

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;

};


#endif //BETAGAMMA_H_
