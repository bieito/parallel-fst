#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef JMI_H_
#define JMI_H_

class JMI : public FeatureSelectionAlgorithm {
public:
    JMI(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {};

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;
};

#endif /* JMI_H_ */
