#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef CONDMI_H_
#define CONDMI_H_

class CondMI : public FeatureSelectionAlgorithm {
public:
    CondMI(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {};

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;
};

#endif //CONDMI_H_
