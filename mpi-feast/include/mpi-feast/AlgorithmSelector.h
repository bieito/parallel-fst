#ifndef PARALLEL_FST_ALGORITHMSELECTOR_H
#define PARALLEL_FST_ALGORITHMSELECTOR_H

#include <string>

class AlgorithmSelector {
public:
    enum AlgorithmType {
        NONE, MRMR_D, CMIM, JMI, DISR, ICAP, COND_MI, MIM, BETA_GAMMA
    };

    static AlgorithmType strToAlgorithm(const std::string &param);

    static std::string algorithmToStr(AlgorithmType algorithm);
};

#endif //PARALLEL_FST_ALGORITHMSELECTOR_H
