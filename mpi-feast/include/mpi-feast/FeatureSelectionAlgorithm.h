#ifndef PARALLEL_FST_FEATURESELECTIONALGORITHM_H
#define PARALLEL_FST_FEATURESELECTIONALGORITHM_H

extern "C" {
#include <FEAST/FSAlgorithms.h>
#include <FEAST/WeightedFSAlgorithms.h>
}

#include <FEAST/FSToolbox.h>

/* MIToolbox includes */
#include <MIToolbox/ArrayOperations.h>
#include <MIToolbox/MutualInformation.h>
#include <MIToolbox/Entropy.h>
#include <MIToolbox/WeightedMutualInformation.h>
#include <MIToolbox/WeightedEntropy.h>

#include "InputMat.h"
#include "AlgorithmResults.h"
#include "Options.h"
#include "Utils.h"
#include "ArrayOps.h"

class FeatureSelectionAlgorithm {
protected:
    unsigned int featuresToSelect;
    unsigned int noOfSamples;
    unsigned int noOfFeatures;
    unsigned int *featureMatrix;
    unsigned int *classColumn;
    AlgorithmResults *results;
public:
    FeatureSelectionAlgorithm(InputMat *mat, const Options &options) {
        featuresToSelect = options.getNumSelecFeat();
        noOfSamples = mat->getNSamples();
        noOfFeatures = mat->getNLocalFeatures();
        featureMatrix = mat->getAllData();
        classColumn = mat->getAllClasses();
        results = new AlgorithmResults(featuresToSelect);
    }

    virtual ~FeatureSelectionAlgorithm() {
        delete results;
    }

    AlgorithmResults *getResults() const {
        return results;
    }

    /* Original function */
    virtual void exec() = 0;

    /* Original weighted function */
    virtual void exec(double *featureWeights) = 0;

    /* Parallel version */
    virtual void exec(int numTh, int myRank, int myOffset) = 0;

    /* Parallel weighted version */
    virtual void exec(double *featureWeights, int numTh, int myRank, int myOffset) = 0;
};


#endif //PARALLEL_FST_FEATURESELECTIONALGORITHM_H
