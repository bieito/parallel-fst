#include <mpi-feast/FeatureSelectionAlgorithm.h>

#ifndef MRMR_D_H_
#define MRMR_D_H_

class mRMR_D : public FeatureSelectionAlgorithm {
public:
    mRMR_D(InputMat *mat, const Options &options) : FeatureSelectionAlgorithm(mat, options) {};

    void exec() override;

    void exec(double *featureWeights) override;

    void exec(int numTh, int myRank, int myOffset) override;

    void exec(double *featureWeights, int numTh, int myRank, int myOffset) override;
};

#endif //MRMR_D_H_
