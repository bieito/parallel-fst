# MPI-FEAST

## MPI Feature Selection Toolbox
Feature selection toolbox based on [FEAST](https://github.com/Craigacp/FEAST) and the [MIToolbox](https://github.com/Craigacp/MIToolbox) parallelized with **Threads** and MPI **Processors**.

## Requirements
Versions listed here were used to configure, build and execute the project. However, previous versions could work as well, but a correct result is not guaranteed.
+ GCC   >= 5.4.0
+ make  >= 4.1
+ CMake >= 2.6
+ MPI *(only one needed)*:
 + MPICH >= 3.2
 + OpenMPI >= 3.1.0

## Build
### Linux
```shell
# Configuration and building
./configure
make

# (optional) Delete all generated files
./clean.sh
```

## Usage
Parallel-FST is intended to be run with MPI. This way, the number of processors to be used can be specified with the ```-n numProcs``` option from ```mpiexec```.

```shell
mpiexec -n numProcs ./Parallel-FST -a algorithm [--original] -i inputFile [-w weightsFile] -o outputFile -s numSelecFeat [-d] [-t numTh] [-m] [--beta b --gamma g]
```

> **WARNING**: Original (sequential) versions of all algorithms have been implemented to enable testing and comparisons. However, they can only be launched using 1 MPI processor and 1 thread.

#### Parameters
+ **-a (string)**    algorithm (mRMR_D | CMIM | JMI | DISR | ICAP | CondMI | MIM | BetaGamma)  
+ **-i (string)**        inputFile  
+ **-w (string)**        (optional) weightsFile  
+ **-o (string)**        outputFile  
+ **-s (int)**           number of features to select  
+ **-d (int)**           (optional) indicates that the input must be discretized and the number of bins used. By default no discretization is required  
+ **-t (int)**           (optional) number of threads. By default: (hardware concurrency)  
+ **-m**                 (optional) if present, map feature values before applying algorithm. Useful for datasets where feature values >> number of samples  
+ **--original**         (optional) if present, use original sequential implementation of the specified algorithm  
+ **--beta (double)**    (BetaGamma only) value of beta parameter for BetaGamma algorithm  
+ **--gamma (double)**   (BetaGamma only) value of gamma parameter for BetaGamma algorithm  
+ **-h**             print out the usage of the program  

> **NOTE**: If both MPI processors and threads are used, ```numTh``` is the number of threads each processor will launch.
