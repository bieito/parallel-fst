#!/bin/bash

# Compilation stuff
FEAST=lib/FEAST/libFSToolbox.so;
FEAST_t=$(mktemp);
cp $FEAST $FEAST_t;
make -C lib/FEAST clean;
mv $FEAST_t $FEAST;

MITB=lib/MIToolbox/libMIToolbox.so;
MITB_t=$(mktemp);
cp $MITB $MITB_t;
make -C lib/MIToolbox clean;
mv $MITB_t $MITB;

# CMake stuff
rm -rf cmake-build-debug/ CMakeFiles/ cmake_install.cmake CMakeCache.txt Parallel-FST.cbp Makefile;
