#ifndef WEIGHTS_H
#define WEIGHTS_H

#include <vector>
#include <stdexcept>
#include <fstream>

class WeightVector {
public:
    explicit WeightVector(const std::string &path);

    inline std::vector<double> getWeightVector() {
        return _weightVector;
    };

    inline unsigned int getNSamples() const {
        return _nSamples;
    };

protected:
    std::vector<double> _weightVector;
    int _nSamples;
};


#endif //WEIGHTS_H
