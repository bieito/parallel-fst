#include <Profiler/Profiler.h>
#include <ArgParser/ArgParser.h>
#include <FEASTDataset.h>
#include <WeightVector/WeightVector.h>
#include <algorithm>

namespace FEAST {
    extern "C" {

#include <FEAST/FSAlgorithms.h>
#include <FEAST/WeightedFSAlgorithms.h>

    }
}

void setupArgParser(ArgParser &argparser);

int main(int argc, char *argv[]) {
    ArgParser argparser("cli-feast", "Command line wrapper for FEAST library", false);
    setupArgParser(argparser);
    argparser.parse(argc - 1, &argv[1]);

    if (argparser.isFlagPresent("help")) {
        argparser.printHelp();
        return 0;
    }

    Profiler profiler;
    bool do_profile = argparser.isFlagPresent("profile");

    // Read Dataset
    if (do_profile) {
        profiler.start();
        std::cout << std::setprecision(6) << std::fixed;
    }
    FEASTDataset<unsigned int> dataset(argparser.getOptionArg("input"), std::stoul(argparser.getOptionArg("disc_bins")),
                                       do_profile);
    if (argparser.isFlagPresent("range_compression")) {
        Profiler::time_point rc_tp;
        if (do_profile) {
            rc_tp = Profiler::now();
        }
        dataset.compress_range();
        if (do_profile) {
            std::cout << "              " << Profiler::secondsFrom(rc_tp) << "s  Range compression"
                      << std::endl;
        }
    }
    if (do_profile) {
        std::cout << "              " << profiler.lapSeconds() << "s  DATASET LOAD" << std::endl;
    }

    // Read weights
    WeightVector *wvector = nullptr;
    bool weighted = argparser.isOptionPresent("weights");//getOptionArg("weights").empty();
    if (weighted &&
        (argparser.getOptionArg("algorithm") == "mRMR" ||
         argparser.getOptionArg("algorithm") == "ICAP" ||
         argparser.getOptionArg("algorithm").rfind("BetaGamma", 0) == 0)) {
        std::cerr << "Warning: The selected algorithm has not a weighted version" << std::endl;
        std::cerr << "           -> Weights will be ignored" << std::endl;
        weighted = false;
    }
    if (weighted) {
        if (do_profile) {
            profiler.start();
        }
        wvector = new WeightVector(argparser.getOptionArg("weights"));
        if (wvector->getNSamples() != dataset.num_samp) {
            throw std::runtime_error(
                    "Error: main: Number of samples in WeightVector not equal to number of samples in Dataset");
        }
        if (do_profile) {
            std::cout << "WeightVector: " << profiler.stopSeconds() << "s  LOAD" << std::endl;
        }
    }

    std::cout << "Dataset: '" << argparser.getOptionArg("input") << "' " << dataset;
    if (weighted) {
        std::cout << "Weights: '" << argparser.getOptionArg("weights") << "'" << std::endl;
    }

    // Get k
    unsigned int k = std::stoul(argparser.getOptionArg("select"));
    int out_features[k];
    unsigned int uout_features[k];
    double out_scores[k];

    // Get algorithm
    std::string alg_names[] = {"MIM", "JMI", "mRMR", "CMIM", "ICAP", "DISR", "error"};
    std::string walg_names[] = {"MIM", "JMI", "CMIM", "DISR", "error"};
    FEAST::uint *(*alg_functions[])(FEAST::uint, FEAST::uint, FEAST::uint, FEAST::uint **, FEAST::uint *, FEAST::uint *,
                                    double *) = {FEAST::MIM, FEAST::JMI, FEAST::mRMR_D, FEAST::CMIM, FEAST::ICAP,
                                                 FEAST::DISR};
    FEAST::uint *
    (*alg_wfunctions[])(FEAST::uint, FEAST::uint, FEAST::uint, FEAST::uint **, FEAST::uint *, double *, FEAST::uint *,
                        double *) = {FEAST::weightedMIM, FEAST::weightedJMI, FEAST::weightedCMIM, FEAST::weightedDISR};

    bool no_algorithm = true;
    double algo_time = 0.0;
    if (!weighted) {
        unsigned int alg_index = 0;
        while (alg_names[alg_index] != argparser.getOptionArg("algorithm") && alg_names[alg_index] != "error") {
            alg_index += 1;
        }
        if (alg_names[alg_index] != "error") {
            no_algorithm = false;
            std::cout << "Algorithm: " << argparser.getOptionArg("algorithm") << std::endl;
            profiler.start();

            // call algorithm
            alg_functions[alg_index](k, dataset.num_samp, dataset.num_feat, dataset.feast_raw_data, dataset.classes,
                                     uout_features, out_scores);

            algo_time = profiler.stopSeconds();
        }
    } else {
        unsigned int alg_index = 0;
        while (walg_names[alg_index] != argparser.getOptionArg("algorithm") && walg_names[alg_index] != "error") {
            alg_index += 1;
        }
        if (walg_names[alg_index] != "error") {
            no_algorithm = false;
            std::cout << "Algorithm: " << argparser.getOptionArg("algorithm") << " (weighted)" << std::endl;
            profiler.start();

            // call algorithm
            alg_wfunctions[alg_index](k, dataset.num_samp, dataset.num_feat, dataset.feast_raw_data, dataset.classes,
                                      wvector->getWeightVector().data(), uout_features, out_scores);

            algo_time = profiler.stopSeconds();
        }
    }

    // BetaGamma
    if (argparser.getOptionArg("algorithm").rfind("BetaGamma", 0) == 0) {
        std::string s = argparser.getOptionArg("algorithm");
        double beta, gamma;

        // Check for the presence of two ':'
        if (std::count(s.begin(), s.end(), ':') != 2) {
            throw std::invalid_argument(
                    "Error: main: Invalid argument for BetaGamma parameters - it must be in format 'BetaGamma:<float>:<float>'");
        }
        // Parse beta and gamma
        std::string numbers = s.substr(s.find(':') + 1);
        try {
            beta = std::stod(numbers.substr(0, numbers.find(':')));
            gamma = std::stod(numbers.substr(numbers.find(':') + 1));
        } catch (std::invalid_argument &e) {
            throw std::invalid_argument(
                    "Error: main: Invalid argument for BetaGamma parameters - it must be in format 'BetaGamma:<float>:<float>'");
        }

        no_algorithm = false;
        std::cout << "Algorithm: BetaGamma with beta=" << beta << " and gamma=" << gamma << std::endl;
        profiler.start();

        FEAST::BetaGamma(k, dataset.num_samp, dataset.num_feat, dataset.feast_raw_data, dataset.classes, uout_features,
                         out_scores, beta, gamma);

        algo_time = profiler.stopSeconds();
    }

    if (argparser.getOptionArg("algorithm") == "CondMI") {
        no_algorithm = false;
        std::cout << "Algorithm: " << argparser.getOptionArg("algorithm") << (weighted ? " (weighted)" : "") << std::endl;
        profiler.start();

        if (!weighted) {
            FEAST::CondMI(k, dataset.num_samp, dataset.num_feat, dataset.feast_raw_data, dataset.classes, out_features,
                          out_scores);
        } else {
            FEAST::weightedCondMI(k, dataset.num_samp, dataset.num_feat, dataset.feast_raw_data, dataset.classes,
                                  wvector->getWeightVector().data(), out_features, out_scores);
        }

        algo_time = profiler.stopSeconds();
    }

    if (do_profile) {
        // TODO dont measure if profiling disabled
        std::cout << "           " << algo_time << "s  algorithm execution" << std::endl;
    }

    if (no_algorithm) {
        throw std::runtime_error(
                "Error: main: Algorithm '" + argparser.getOptionArg("algorithm") + "' unknown");
    }

    if (argparser.getOptionArg("algorithm") != "CondMI") {
        // CondMI uses int[] for out features, but all the other use uint[] -> copy uint[] to int[] if algorithm was not CondMI
        for (unsigned int i = 0; i < k; ++i) {
            out_features[i] = (int) uout_features[i];
        }
    }

    if (!argparser.isOptionPresent("output")) {
        std::cout << "Results: " << k << " feature" << (k != 1 ? "s" : "") << std::endl;
        std::cout << "#feature_index,feature_score" << std::endl;
        std::cout << std::setprecision(6) << std::fixed;
        for (unsigned int i = 0; i < k; ++i) {
            std::cout << out_features[i] << "," << out_scores[i] << std::endl;
        }
    } else {
        std::ofstream file(argparser.getOptionArg("output"));

        if (file.is_open()) {
            file << std::setprecision(6) << std::fixed;
            file << "#feature_index,feature_score" << std::endl;
            for (unsigned int i = 0; i < k; ++i) {
                file << out_features[i] << "," << out_scores[i] << std::endl;
            }
            file.flush();
        } else {
            throw std::invalid_argument("Error opening output file");
        }
    }

    if (weighted) {
        delete wvector;
    }
}

void setupArgParser(ArgParser &parser) {
    parser.addOptionArg("input", {'i'}, {"input"}, "path", true,
                        {}, "input file to read from\nallowed formats are arff, csv and libsvm");
    parser.addOptionArg("weights", {'w'}, {"weights"}, "path", false,
                        {}, "input file to read weights from\nallowed format is csv");
    parser.addOptionArg("output", {'o'}, {"output"}, "path", false,
                        {}, "output file to write results to\n"
                            "if not specified, write to stdout");

    parser.addOptionArg("algorithm", {'a'}, {"algorithm"}, "ALG", true,
                        {},
                        "algorithm to run - ALG = (MIM|CondMI|BetaGammma:<float>:<float>|JMI|mRMR|CMIM|ICAP|DISR)\n"
                        "MIM     Mutual Information Maximisation\n"
                        "CondMI  Conditional Mutual Information              [2012 - G. Brown, A. Pocock, M.-J. Zhao, M. Luján]\n"
                        "JMI     Joint Mutual Information                    [1999 - H. H. Yang, J. Moody]\n"
                        "mRMR    Minimum-Redundancy Max-Relevance            [2005 - H. Peng, F. Long, C. Ding]\n"
                        "CMIM    Conditional Mutual Information Maximisation [2004 - F. Fleuret]\n"
                        "ICAP    Interaction Capping                         [2005 - A. Jakulin]\n"
                        "DISR    Double Input Symmetrical Relevance          [2006 - P. E. Meyer, G. Bontempi]\n"
                        "BetaGamma:<b>:<g>  BetaGamma Space\n"
                        "    values for Beta <b> (weight of redundancy) and Gamma <g> (weight of conditional redundancy) can be provided\n"
                        "    well-known configurations are:\n"
                        "        beta    | gamma | method\n"
                        "    ------------+-------+--------\n"
                        "         0.0    |  0.0  |  MIM\n"
                        "     (0.0, 1.0] |  0.0  |  MIFS  Mutual Information Feature Selection  [1994 - R. Battiti]\n"
                        "         1.0    |  1.0  |  CIFE  Conditional Infomax Feature Selection [2006 - D. Lin, X. Tang]"
    );

    parser.addOptionArg("select", {'k'}, {"select"}, "int", true,
                        {}, "number of features to select");
    parser.addFlagArg("range_compression", {'c'}, {"rangecomp"},
                      "apply a range compression before running the algorithm\n"
                      "useful for datasets where number of feature values >> number of samples.");
    parser.addOptionArg("disc_bins", {'d'}, {"disc"}, "n_bins", false,
                        {},
                        "discretize the input dataset with the binning technique using the specified number of bins");

    parser.addFlagArg("profile", {}, {"profile"},
                      "measure times for any active execution");

    /*parser.addFlagArg("print_version",
                      {'V'}, {"version"},
                      "show the version of the program and other build info");*/
    parser.addFlagArg("help",
                      {'h'}, {"help", "usage"},
                      "show this help and exit");
}