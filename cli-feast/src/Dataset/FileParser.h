#ifndef FILEPARSER_H
#define FILEPARSER_H

#include <fstream>
#include <vector>

#include "Dataset.h"

class format_error : public std::exception {
public:
    explicit format_error(std::string msg) : msg(std::move(msg)) {};

    inline const char *what() const noexcept override {
        return msg.c_str();
    }

private:
    std::string msg;
};


class FileParser {
public:
    static FileParser *getFileParser(std::string const &path);

    FileParser() = default;

    explicit FileParser(std::string const &path) {
        file.open(path);

        if (!file.is_open()) {
            throw std::invalid_argument("ERROR in FileParser: file " + path + " could not be opened");
        }
    }

    virtual ~FileParser() { file.close(); }

    virtual void readInput(Dataset<unsigned int> &dataset, unsigned int discBins) = 0;

protected:
    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                  std::vector<unsigned int *> sample_data, std::vector<int> classes);

    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                  std::vector<double *> sample_data, std::vector<int> classes,
                                  unsigned int disc_bins);

    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                  std::vector<unsigned int *> sample_data, std::vector<int> classes,
                                  std::vector<unsigned int *> indexes);

    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                  std::vector<double *> sample_data, std::vector<int> classes,
                                  std::vector<unsigned int *> indexes,
                                  unsigned int disc_bins);

protected:
    std::ifstream file;
};


#endif //FILEPARSER_H
