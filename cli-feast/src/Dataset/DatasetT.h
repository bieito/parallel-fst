#include <Dataset.h>
#include <FileParser.h>

template<typename T>
Dataset<T>::Dataset(const std::string &path, unsigned int disc_bins) {
    FileParser *fp = FileParser::getFileParser(path);
    fp->readInput(*this, disc_bins);
    delete fp;
}

template<typename T>
void Dataset<T>::compress_range() {
    for (unsigned int i = 0; i < this->num_feat; ++i) {
        // Find max state
        T max_state = 0;
        for (unsigned int j = 0; j < this->num_samp; ++j) {
            if (max_state < this->feature(i)[j]) {
                max_state = this->feature(i)[j];
            }
        }
        max_state += 1;

        // Create feature relationship structure
        int feature_map[max_state];
        for (int j = 0; j < (int) max_state; ++j) {
            feature_map[j] = -1;
        }
        unsigned int map_counter = 0;

        for (unsigned int j = 0; j < this->num_samp; ++j) {
            if (feature_map[this->feature(i)[j]] == -1) {
                // Value has not appeared yet
                feature_map[this->feature(i)[j]] = map_counter;
                map_counter += 1;
            }
            this->feature(i)[j] = feature_map[this->feature(i)[j]];
        }
    }
}

template<typename T>
void Dataset<T>::copy(T *raw_data, unsigned int num_feat, unsigned int num_samp, unsigned int num_clas) {
    initAllocate(num_feat, num_samp, num_clas);
    for (unsigned int i = 0; i < numElements(); ++i) {
        this->raw_data[i] = raw_data[i];
    }
}

template<typename T>
template<typename R, typename D>
void Dataset<T>::discretizeDataset(Dataset<R> &real_dat, Dataset<D> &disc_dat, unsigned int disc_bins) {

    if (real_dat.numElements() != disc_dat.numElements()) {
        throw std::invalid_argument("Error in discretizeDataset: dimensions of datasets do not match");
    }

    for (unsigned int f_i = 0; f_i < real_dat.num_feat; ++f_i) {

        R *real_data = &real_dat.raw_data[f_i * real_dat.num_samp];
        D *disc_data = &disc_dat.raw_data[f_i * real_dat.num_samp];

        // First detect the minimum and maximum values
        R min_val = real_data[0];
        R max_val = real_data[0];
        for (unsigned int i = 1; i < real_dat.num_samp; i++) {
            if (real_data[i] < min_val) {
                min_val = real_data[i];
            }
            if (real_data[i] > max_val) {
                max_val = real_data[i];
            }
        }

        double bin_size = (max_val - min_val) / ((double) disc_bins);
        for (unsigned int i = 0; i < real_dat.num_samp; i++) {
            disc_data[i] = (D) floor((real_data[i] - min_val) / bin_size);
            /*// TODO check
            unsigned int current_val = 0;
            double bin_lim = min_val + bin_size;
            while ((real_data[i] > bin_lim) && (current_val < disc_bins - 1)) {
                current_val++;
                bin_lim += bin_size;
            }
            if (disc_data[i] != current_val) {
                throw std::runtime_error("NON EQUIVALENTES");
            }*/
            /*disc_data[i] = current_val;*/
        }
    }
}