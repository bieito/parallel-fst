#ifndef CUDA_MRMR_ARFFPARSER_H
#define CUDA_MRMR_ARFFPARSER_H

#include "FileParser.h"

// An .arff file has the values ordered by samples, so we must mix them
class ArffParser : public FileParser {
public:
    explicit ArffParser(std::string const &path) : FileParser(path) {};

    ~ArffParser() override { file.close(); }

    void readInput(Dataset<unsigned int> &dataset, unsigned int discBins) override;
};

// Class to represent arff format nominal samples
class NominalParameter {
public:
    // The string obtained from the file with the options separated by commas
    explicit NominalParameter(const std::string &options);

    virtual ~NominalParameter() {
        num_options = 0;
        delete[] options;
    };

    inline unsigned int getNumOptions() const { return num_options; }

    // Return the integer value for certain option
    // Otherwise, error
    unsigned int getVal(std::string option) const;

private:
    unsigned int num_options;
    std::string *options;
};

#endif //CUDA_MRMR_ARFFPARSER_H
