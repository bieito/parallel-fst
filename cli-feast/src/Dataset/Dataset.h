#ifndef DATASET_H
#define DATASET_H

#include <cmath>
#include <stdexcept>
#include <iostream>

template<typename T>
class Dataset {
public:
    /* Dataset operations */
    template<typename R, typename D>
    static void discretizeDataset(Dataset<R> &real_dat, Dataset<D> &disc_dat, unsigned int disc_bins = 0);

    Dataset() = default;

    explicit Dataset(const std::string &path, unsigned int disc_bins = 0);

    void copy(T *raw_data, unsigned int num_feat, unsigned int num_samp, unsigned int num_clas = 1);

    virtual ~Dataset() {
        if (raw_data != nullptr) {
            delete[] raw_data;
        }
    }

    inline unsigned int numElements() const { return (num_feat + num_clas) * num_samp; }

    inline std::size_t numBytes() const { return numElements() * sizeof(T); }

    virtual inline void allocate() {
        if (raw_data != nullptr) {
            throw std::runtime_error("Error: Dataset: Memory already allocated");
        }
        raw_data = new T[numElements()];
    }

    inline T *feature(unsigned int feature_id) const {
        if (feature_id >= num_feat) {
            throw std::invalid_argument("Error: Dataset: Feature '" + std::to_string(feature_id) + "' not in dataset");
        }
        return &raw_data[feature_id * num_samp];
    }

    inline void initAllocate(unsigned int num_feat, unsigned int num_samp, unsigned int num_clas) {
        this->num_feat = num_feat;
        this->num_samp = num_samp;
        this->num_clas = num_clas;
        allocate();
        this->classes = &raw_data[num_feat * num_samp];
    }

    inline void print() {
        for (int i = 0; i < num_feat + 1; ++i) {
            for (int j = 0; j < num_samp; ++j) {
                std::cout << raw_data[i * num_samp + j] << " ";
            }
            std::cout << std::endl;
        }
    }

    void compress_range();

public:
    unsigned int num_feat = 0;
    unsigned int num_samp = 0;
    unsigned int num_clas = 0;

    T *raw_data = nullptr;
    T *classes = nullptr;
};

#endif //DATASET_H
