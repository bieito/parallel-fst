#ifndef CUDA_MRMR_CSVPARSER_H
#define CUDA_MRMR_CSVPARSER_H

#include "FileParser.h"

// A .csv file has the values ordered by samples, so we must mix them
class CsvParser : public FileParser {
public:
    explicit CsvParser(std::string const &path) : FileParser(path) {};

    ~CsvParser() override { file.close(); }

    void readInput(Dataset<unsigned int> &dataset, unsigned int discBins) override;
};

#endif //CUDA_MRMR_CSVPARSER_H
