#include "FileParser.h"

#include "ArffParser.h"
#include "CsvParser.h"
#include "LibSvmParser.h"

FileParser *FileParser::getFileParser(const std::string &path) {
    FileParser *parser;

    // Check the extension
    if ((path.find(".arff") != std::string::npos) ||
        (path.find(".ARFF") != std::string::npos)) {
        parser = new ArffParser(path);
    } else if ((path.find(".csv") != std::string::npos) ||
               (path.find(".CSV") != std::string::npos)) {
        parser = new CsvParser(path);
    } else if ((path.find(".libsvm") != std::string::npos) ||
               (path.find(".LIBSVM") != std::string::npos)) {
        parser = new LibSvmParser(path);
    } else {
        throw std::invalid_argument(
                "ERROR in FileParser: file extension not recognized. Options: .arff, .ARFF, .csv, .CSV, .libsvm, .LIBSVM\n");
    }

    return parser;
}

void discretize(const double *real_data, unsigned int *disc_data, unsigned int num_samp, unsigned int disc_bins) {

    // First detect the minimum and maximum values
    double min_val = real_data[0];
    double max_val = real_data[0];
    for (unsigned int i = 1; i < num_samp; i++) {
        if (real_data[i] < min_val) {
            min_val = real_data[i];
        }
        if (real_data[i] > max_val) {
            max_val = real_data[i];
        }
    }

    double bin_size = (max_val - min_val) / ((double) disc_bins);
    for (unsigned int i = 0; i < num_samp; i++) {
        unsigned int tmp = std::floor((real_data[i] - min_val) / bin_size);
        disc_data[i] = tmp - ((tmp == disc_bins) ? 1 : 0);
    }

}


void FileParser::initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                   std::vector<unsigned int *> sample_data, std::vector<int> classes) {

    dataset.initAllocate(num_feat, num_samp, 1);

    unsigned int aux;
    for (unsigned int i = 0; i < num_feat; i++) {
        for (unsigned int j = 0; j < num_samp; j++) {
            aux = sample_data[j][i];
            dataset.raw_data[i * num_samp + j] = aux;
        }
    }

    // All the classes start in 0
    int minClass = classes[0];
    for (unsigned int i = 1; i < num_samp; i++) {
        if (classes[i] < minClass) {
            minClass = classes[i];
        }
    }

    if (minClass > 0) {
        minClass = 0;
    }

    unsigned int maxClass = 0;
    for (unsigned int i = 0; i < num_samp; i++) {
        aux = classes[i] - minClass;
        dataset.classes[i] = aux;
        if (aux > maxClass) {
            maxClass = aux;
        }
    }
}

void FileParser::initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                   std::vector<double *> sample_data, std::vector<int> classes,
                                   unsigned int disc_bins) {

    dataset.initAllocate(num_feat, num_samp, 1);

    auto *auxData = new double[num_samp];

    for (unsigned int i = 0; i < num_feat; i++) {
        for (unsigned int j = 0; j < num_samp; j++) {
            auxData[j] = sample_data[j][i];
        }
        discretize(auxData, &dataset.raw_data[i * num_samp], num_samp, disc_bins);
    }

    delete[] auxData;


    // All the classes start in 0
    int minClass = classes[0];
    for (unsigned int i = 1; i < num_samp; i++) {
        if (classes[i] < minClass) {
            minClass = classes[i];
        }
    }

    if (minClass > 0) {
        minClass = 0;
    }

    unsigned int aux;
    unsigned int maxClass = 0;
    for (unsigned int i = 0; i < num_samp; i++) {
        aux = classes[i] - minClass;
        dataset.classes[i] = aux;
        if (aux > maxClass) {
            maxClass = aux;
        }
    }
}

void FileParser::initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                   std::vector<unsigned int *> sample_data, std::vector<int> classes,
                                   std::vector<unsigned int *> indexes) {

    dataset.initAllocate(num_feat, num_samp, 1);
    unsigned int aux;

    // Initialize everything to 0 in case sparse
    for (unsigned int i = 0; i < num_feat; i++) {
        for (unsigned int j = 0; j < num_samp; j++) {
            dataset.raw_data[i * num_samp + j] = 0;
        }
    }

    for (unsigned int j = 0; j < num_samp; j++) {
        unsigned int rowFeat = indexes[j][0];
        for (unsigned int i = 0; i < rowFeat; i++) {
            aux = sample_data[j][i];
            dataset.raw_data[indexes[j][i + 1] * num_samp + j] = aux;
        }
    }

    // All the classes start in 0
    int minClass = classes[0];
    for (unsigned int i = 1; i < num_samp; i++) {
        if (classes[i] < minClass) {
            minClass = classes[i];
        }
    }

    if (minClass > 0) {
        minClass = 0;
    }

    double maxClass = 0;
    for (unsigned int i = 0; i < num_samp; i++) {
        aux = classes[i] - minClass;
        dataset.classes[i] = aux;
        if (aux > maxClass) {
            maxClass = aux;
        }
    }

}

void FileParser::initializeDataset(Dataset<unsigned int> &dataset, unsigned int num_feat, unsigned int num_samp,
                                   std::vector<double *> sample_data, std::vector<int> classes,
                                   std::vector<unsigned int *> indexes,
                                   unsigned int disc_bins) {

    dataset.initAllocate(num_feat, num_samp, 1);

    auto *auxData = new double[num_feat * num_samp];

    // Initialize everything to 0 in case sparse
    for (unsigned int i = 0; i < num_feat; i++) {
        for (unsigned int j = 0; j < num_samp; j++) {
            auxData[i * num_samp + j] = 0;
        }
    }

    for (unsigned int j = 0; j < num_samp; j++) {
        unsigned int rowFeat = indexes[j][0];
        for (unsigned int i = 0; i < rowFeat; i++) {
            auxData[indexes[j][i + 1] * num_samp + j] = sample_data[j][i];
        }
    }

    for (unsigned int i = 0; i < num_feat; i++) {
        discretize(&auxData[i * num_samp], &dataset.raw_data[i * num_samp], num_samp, disc_bins);
    }

    delete[] auxData;


    // All the classes start in 0
    int minClass = classes[0];
    for (unsigned int i = 1; i < num_samp; i++) {
        if (classes[i] < minClass) {
            minClass = classes[i];
        }
    }

    if (minClass > 0) {
        minClass = 0;
    }

    unsigned int aux;
    unsigned int maxClass = 0;
    for (unsigned int i = 0; i < num_samp; i++) {
        aux = classes[i] - minClass;
        dataset.classes[i] = aux;
        if (aux > maxClass) {
            maxClass = aux;
        }
    }
}


