#include <Dataset/DatasetT.h>
#include <Profiler/Profiler.h>

template<typename T>
class FEASTDataset : public Dataset<T> {
public:
    explicit FEASTDataset(const std::string &path, unsigned int disc_bins = 0, bool do_profile = false)
            : Dataset<T>(path, disc_bins) {
        this->do_profile = do_profile;
        this->convert_data();
    }

    T **convert_data() {
        if (feast_raw_data != nullptr) {
            throw std::runtime_error("Error: FEASTDataset: Data already converted");
        }

        Profiler::time_point start;
        if (do_profile) {
            start = Profiler::now();
        }

        feast_raw_data = new T *[this->num_feat];
        for (unsigned int i = 0; i < this->num_feat; ++i) {
            feast_raw_data[i] = this->feature(i);
        }

        if (do_profile) {
            std::cout << "FEASTDataset: " << Profiler::secondsFrom(start) << "s  Data conversion 1D->2D" << std::endl;
        }

        return feast_raw_data;
    }

    ~FEASTDataset() override {
        if (feast_raw_data != nullptr) {
            delete[] feast_raw_data;
        }
    }

    explicit operator std::string() const {
        std::stringstream sstr;

        // get human readable size
        std::string hr_size;
        {
            double total = this->numBytes();
            std::string prefix[] = {"", "ki", "Mi", "Gi", "Ti", "Pi"};
            unsigned int i = 0;
            while (total >= 1024 && i <= 5) {
                total /= 1024;
                ++i;
            }

            std::stringstream priv_sstr;
            priv_sstr << std::fixed << std::setprecision(3) << total << " " << prefix[i];
            hr_size = priv_sstr.str();
        }
        sstr << this->num_samp << " samples | " << this->num_feat << " features | "
             << this->num_clas << " classes (" << hr_size << "B)" << std::endl;

        return sstr.str();
    }

public:
    T **feast_raw_data = nullptr;
    bool do_profile = false;
};

template<typename T>
std::ostream &operator<<(std::ostream &strm, FEASTDataset<T> const &obj) {
    return strm << (std::string) obj;
}