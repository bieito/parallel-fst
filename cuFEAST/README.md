# cuFEAST (Library & CLI)

CUDA-accelerated feature selection for NVIDIA GPUs.

## Building

Configuration, building and testing is automated with the CMake suite tools. Moreover, this is a C/C++/CUDA project, so
compilers for all of them are required. The versions of this software used during the development are:

* **CMake** 3.19.1
* **C/C++** (GNU) gcc 10.2.0
* **CUDA** (NVIDIA) nvcc 11.1.105

Some helper scripts (files under the ```scripts/``` directory) are provided to simplify the process. CMake option
```-DCMAKE_INSTALL_PREFIX=<path>``` can be passed to specify a custom installation path (note that installation IS NOT
performed now, but that variable has to be set at the configuration step). From the project root directory, execute:

TODO: explain cuda archs [ref table](https://arnon.dk/matching-sm-architectures-arch-and-gencode-for-various-nvidia-cards/)

```shell
$ scripts/configure [-DCMAKE_INSTALL_PREFIX=<path>] [-DCMAKE_CUDA_ARCHITECTURES=<CC>] && scripts/build
```

Then, validation tests can be run with

```shell
$ scripts/test  # optional
```

Library and CLI command can be installed like follows. Root permissions are needed if installing to a system path
(by default, if path not specified during configuration)

```shell
$ [sudo] scripts/install  # optional
```

If installed, the build directory ```dist/``` is no longer needed. It can also be removed to perform a latter clean
build.

```shell
$ scripts/clean  # optional
```

## Usage

### Library

Add the files in ```cuda-mrmr/include/```  to your C++ project, and the next line to the code (implicitly includes the
rest):

```C++
#include "cuda-mrmr/cuda-mrmr.h"
```

Then link with the generated shared library ```libcuda-mrmr.so```, located at ```dist/``` or at the system library
directory (if the installation step was performed).

### Command Line Interface

The CLI program offers a wrapper for the library, and can be run as follows:

```shell
$ ./dist/mrmr-cli --help  # if not installed
$ mrmr-cli --help  # if installed
```
