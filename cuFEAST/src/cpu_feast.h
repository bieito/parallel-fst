#ifndef CUFEAST_CPU_FEAST_H
#define CUFEAST_CPU_FEAST_H

#include <cuFEAST/FSAlgorithm.h>
#include <Profiler/Profiler.h>

#include "LUT/lut.h"

#define CPU_LOG_BASE 2

namespace CPU {
    template<typename T>
    void mrmr(unsigned int k, Dataset<T> const &dataset, Results &results);

    template<typename T>
    void mrmr(unsigned int k, Dataset<T> const &dataset, Results &results, Config &config);
}

#endif
