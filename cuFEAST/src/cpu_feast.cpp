#include "cpu_feast.h"

typedef struct {
    double *jointProbabilityVector;
    unsigned int numJointStates;
    double *firstProbabilityVector;
    unsigned int numFirstStates;
    double *secondProbabilityVector;
    unsigned int numSecondStates;
} JointProbabilityState;

typedef struct {
    unsigned int *jointCountsVector;
    unsigned int numJointStates;
    unsigned int *firstCountsVector;
    unsigned int numFirstStates;
    unsigned int *secondCountsVector;
    unsigned int numSecondStates;
} JointCountsState;

// TODO D must be discrete
template<typename D>
unsigned int maxState(const D *vector, unsigned int vectorLength) {
    D max = 0;

    for (unsigned int i = 0; i < vectorLength; i++) {
        if (vector[i] > max) {
            max = vector[i];
        }
    }

    return max + 1;
}

// TODO D must be discrete
template<typename D>
JointProbabilityState calculateJointProbability(D *firstVector, D *secondVector, unsigned int vectorLength) {
    double length = vectorLength;
    JointProbabilityState state;

    unsigned int firstNumStates = maxState(firstVector, vectorLength);
    unsigned int secondNumStates = maxState(secondVector, vectorLength);
    unsigned int jointNumStates = firstNumStates * secondNumStates;

    int *firstStateCounts = new int[firstNumStates]();
    int *secondStateCounts = new int[secondNumStates]();
    int *jointStateCounts = new int[jointNumStates]();

    double *firstStateProbs = new double[firstNumStates]();
    double *secondStateProbs = new double[secondNumStates]();
    double *jointStateProbs = new double[jointNumStates]();

    /* Optimised for number of FP operations now O(states) instead of O(vectorLength) */
    for (unsigned int i = 0; i < vectorLength; i++) {
        firstStateCounts[firstVector[i]] += 1;
        secondStateCounts[secondVector[i]] += 1;
        jointStateCounts[secondVector[i] * firstNumStates + firstVector[i]] += 1;
    }

    for (unsigned int i = 0; i < firstNumStates; i++) {
        firstStateProbs[i] = firstStateCounts[i] / length;
    }

    for (unsigned int i = 0; i < secondNumStates; i++) {
        secondStateProbs[i] = secondStateCounts[i] / length;
    }

    for (unsigned int i = 0; i < jointNumStates; i++) {
        jointStateProbs[i] = jointStateCounts[i] / length;
    }

    delete[] firstStateCounts;
    delete[] secondStateCounts;
    delete[] jointStateCounts;

    state.jointProbabilityVector = jointStateProbs;
    state.numJointStates = jointNumStates;
    state.firstProbabilityVector = firstStateProbs;
    state.numFirstStates = firstNumStates;
    state.secondProbabilityVector = secondStateProbs;
    state.numSecondStates = secondNumStates;

    return state;
}

template<typename D>
JointCountsState calculateJointCounts(D *firstVector, D *secondVector, unsigned int vectorLength) {
    //double length = vectorLength;
    JointCountsState state;

    unsigned int firstNumStates = maxState(firstVector, vectorLength);
    unsigned int secondNumStates = maxState(secondVector, vectorLength);
    unsigned int jointNumStates = firstNumStates * secondNumStates;

    unsigned int *firstStateCounts = new unsigned int[firstNumStates]();
    unsigned int *secondStateCounts = new unsigned int[secondNumStates]();
    unsigned int *jointStateCounts = new unsigned int[jointNumStates]();

    /* Optimised for number of FP operations now O(states) instead of O(vectorLength) */
    for (unsigned int i = 0; i < vectorLength; i++) {
        firstStateCounts[firstVector[i]] += 1;
        secondStateCounts[secondVector[i]] += 1;
        jointStateCounts[secondVector[i] * firstNumStates + firstVector[i]] += 1;
    }

    for (unsigned int i = 0; i < firstNumStates; i++) {
        firstStateCounts[i] = (firstStateCounts[i]);
    }

    for (unsigned int i = 0; i < secondNumStates; i++) {
        secondStateCounts[i] = (secondStateCounts[i]);
    }

    for (unsigned int i = 0; i < jointNumStates; i++) {
        jointStateCounts[i] = (jointStateCounts[i]);
    }

    state.jointCountsVector = jointStateCounts;
    state.numJointStates = jointNumStates;
    state.firstCountsVector = firstStateCounts;
    state.numFirstStates = firstNumStates;
    state.secondCountsVector = secondStateCounts;
    state.numSecondStates = secondNumStates;

    return state;
}

void freeJointProbabilityState(JointProbabilityState state) {
    delete[] state.firstProbabilityVector;
    delete[] state.secondProbabilityVector;
    delete[] state.jointProbabilityVector;
}

void freeJointCountsState(JointCountsState state) {
    delete[] state.firstCountsVector;
    delete[] state.secondCountsVector;
    delete[] state.jointCountsVector;
}

double mi(JointProbabilityState state) {
    double mutualInformation = 0.0;

    for (unsigned int i = 0; i < state.numJointStates; ++i) {
        unsigned int firstIndex = i % state.numFirstStates;
        unsigned int secondIndex = i / state.numFirstStates;

        if ((state.jointProbabilityVector[i] > 0) && (state.firstProbabilityVector[firstIndex] > 0) &&
            (state.secondProbabilityVector[secondIndex] > 0)) {
            /*double division is probably more stable than multiplying two small numbers together
            ** mutualInformation += state.jointProbabilityVector[i] * log(state.jointProbabilityVector[i] / (state.firstProbabilityVector[firstIndex] * state.secondProbabilityVector[secondIndex]));
            */
            mutualInformation += state.jointProbabilityVector[i] *
                                 log(state.jointProbabilityVector[i] / state.firstProbabilityVector[firstIndex] /
                                     state.secondProbabilityVector[secondIndex]);
        }
    }

    mutualInformation /= log(CPU_LOG_BASE);

    return mutualInformation;
}

template<typename F>
double mi(JointCountsState state, LUT<F> const &lut, unsigned int numSamples) {
    double mutualInformation = 0.0;

    for (unsigned int i = 0; i < state.numJointStates; ++i) {
        unsigned int firstIndex = i % state.numFirstStates;
        unsigned int secondIndex = i / state.numFirstStates;

        if ((state.jointCountsVector[i] > 0) && (state.firstCountsVector[firstIndex] > 0) &&
            (state.secondCountsVector[secondIndex] > 0)) {
            /*double division is probably more stable than multiplying two small numbers together
            ** mutualInformation += state.jointProbabilityVector[i] * log(state.jointProbabilityVector[i] / (state.firstProbabilityVector[firstIndex] * state.secondProbabilityVector[secondIndex]));
            */
            /*unsigned int jci = state.jointCountsVector[i], fcf = state.firstCountsVector[firstIndex], scs = state.secondCountsVector[secondIndex];
            F jlut = lut.get(jci), flut = lut.get(fcf), slut = lut.get(scs);
            std::cout << std::setprecision(2) << std::setw(3) << jci << ", " << std::setw(3) << fcf << ", "
                      << std::setw(3) << scs <<
                      " -> " << std::setw(5) << jlut << ", " << std::setw(5) << flut << ", " << std::setw(5) << slut <<
                      " = " << std::setw(6) << jlut - flut - slut <<
                      " = " << std::setw(6) << (F) ((int) jlut - (int) flut - (int) slut) <<
                      " = " << std::setw(5) << lut.to_double(jlut - flut - slut) <<
                      " | " << std::setw(5)
                      << log2((jci / (double) numSamples) / ((fcf / (double) numSamples) * (scs / (double) numSamples)))
                      << std::endl;*/
            mutualInformation += ((double) state.jointCountsVector[i] / numSamples) *
                                 lut.to_double(
                                         lut.get(state.jointCountsVector[i])
                                         - lut.get(state.firstCountsVector[firstIndex])
                                         - lut.get(state.secondCountsVector[secondIndex]));
        }

        /*
        if ((lut.transform(state.jointCountsVector[i]) > 0) && (lut.transform(state.firstCountsVector[firstIndex]) > 0) &&
            (lut.transform(state.secondCountsVector[secondIndex]) > 0)) {
        mutualInformation += ((double) lut.transform(state.jointCountsVector[i]) / lut.getNumSamples()) *
                             lut.to_double(
                                     lut.get(lut.transform(state.jointCountsVector[i]), lut.getNumSamples())
                                     - lut.get(lut.transform(state.firstCountsVector[firstIndex]), lut.getNumSamples())
                                     - lut.get(lut.transform(state.secondCountsVector[secondIndex]), lut.getNumSamples()));
        }*/
    }

    return mutualInformation;
}

// TODO D must be discrete
template<typename D>
double calcMutualInformation(D *dataVector, D *targetVector, unsigned int vectorLength) {
    JointProbabilityState state = calculateJointProbability(dataVector, targetVector, vectorLength);

    double mutualInformation = mi(state);

    freeJointProbabilityState(state);

    return mutualInformation;
}

template<typename D, typename F>
double calcMutualInformation(D *dataVector, D *targetVector, unsigned int vectorLength, LUT<F> const &lut) {
    JointCountsState state = calculateJointCounts(dataVector, targetVector, vectorLength);

    double mutualInformation = mi(state, lut, vectorLength);

    freeJointCountsState(state);

    return mutualInformation;
}

template<typename D, typename R>
void cpu_mrmr_base(unsigned int k, Dataset<D> const &dat, Results &results, [[maybe_unused]] Config &config) {

    R *classMI = new R[dat.num_feat]();
    bool *selectedFeatures = new bool[dat.num_feat]();
    const unsigned int sizeOfMatrix = k * dat.num_feat;
    R *featureMIMatrix = new R[sizeOfMatrix];

    /* Ensure it always picks a feature*/
    double maxMI = -1.0;
    int maxMICounter = -1;

    for (unsigned int i = 0; i < sizeOfMatrix; ++i) {
        featureMIMatrix[i] = -1;
    } /*blank to -1*/

    for (unsigned int i = 0; i < dat.num_feat; ++i) {
        classMI[i] = calcMutualInformation(dat.feature(i), dat.classes, dat.num_samp);
        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        }
    }

    selectedFeatures[maxMICounter] = true;
    results.push_back({maxMICounter, maxMI});

    for (unsigned int i = 1; i < k; i++) {
        double score = -std::numeric_limits<R>::max();
        unsigned int currentHighestFeature = 0;
        double currentScore = 0.0;
        double totalFeatureMI = 0.0;

        for (unsigned int j = 0; j < dat.num_feat; j++) {
            /*if we haven't selected j*/
            if (selectedFeatures[j] == 0) {
                currentScore = classMI[j];
                totalFeatureMI = 0.0;

                for (unsigned int x = 0; x < i; x++) {
                    unsigned int arrayPosition = x * dat.num_feat + j;
                    if (featureMIMatrix[arrayPosition] == -1) {
                        featureMIMatrix[arrayPosition] = calcMutualInformation(dat.feature(results[x].first),
                                                                               dat.feature(j), dat.num_samp);
                    }

                    totalFeatureMI += featureMIMatrix[arrayPosition];
                }

                currentScore -= (totalFeatureMI / i);
                if (currentScore > score) {
                    score = currentScore;
                    currentHighestFeature = j;
                }
            }
        }

        selectedFeatures[currentHighestFeature] = true;
        results.push_back({currentHighestFeature, score});

    }/*for the number of features to select*/

    delete[] classMI;
    delete[] featureMIMatrix;
    delete[] selectedFeatures;
}

template<typename D, typename R, typename F>
void cpu_mrmr_base(unsigned int k, Dataset<D> const &dat, Results &results, [[maybe_unused]] Config &config, LUT<F> const &lut) {

    R *classMI = new R[dat.num_feat]();
    bool *selectedFeatures = new bool[dat.num_feat]();
    const unsigned int sizeOfMatrix = k * dat.num_feat;
    R *featureMIMatrix = new R[sizeOfMatrix];

    /* Ensure it always picks a feature*/
    double maxMI = -1.0;
    int maxMICounter = -1;

    for (unsigned int i = 0; i < sizeOfMatrix; ++i) {
        featureMIMatrix[i] = -1;
    } /*blank to -1*/

    for (unsigned int i = 0; i < dat.num_feat; ++i) {
        classMI[i] = calcMutualInformation(dat.feature(i), dat.classes, dat.num_samp, lut);
        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        }
    }

    selectedFeatures[maxMICounter] = true;
    results.push_back({maxMICounter, maxMI});

    for (unsigned int i = 1; i < k; i++) {
        double score = -std::numeric_limits<R>::max();
        unsigned int currentHighestFeature = 0;
        double currentScore = 0.0;
        double totalFeatureMI = 0.0;

        for (unsigned int j = 0; j < dat.num_feat; j++) {
            /*if we haven't selected j*/
            if (selectedFeatures[j] == 0) {
                currentScore = classMI[j];
                totalFeatureMI = 0.0;

                for (unsigned int x = 0; x < i; x++) {
                    unsigned int arrayPosition = x * dat.num_feat + j;
                    if (featureMIMatrix[arrayPosition] == -1) {
                        featureMIMatrix[arrayPosition] = calcMutualInformation(dat.feature(results[x].first),
                                                                               dat.feature(j), dat.num_samp, lut);
                    }

                    totalFeatureMI += featureMIMatrix[arrayPosition];
                }

                currentScore -= (totalFeatureMI / i);
                if (currentScore > score) {
                    score = currentScore;
                    currentHighestFeature = j;
                }
            }
        }

        selectedFeatures[currentHighestFeature] = true;
        results.push_back({currentHighestFeature, score});

    }/*for the number of features to select*/

    delete[] classMI;
    delete[] featureMIMatrix;
    delete[] selectedFeatures;
}

template<typename T>
void CPU::mrmr(unsigned int k, Dataset<T> const &dat/*dataset*/, Results &results, Config &config) {

    Profiler profiler;
    profiler.start();

    // From now on, we suppose there is only 1 row for classes

    // Set k to min(k, num_feat)
    if (k > dat.num_feat) {
        k = dat.num_feat;
    }

    if (config.getPrecision() == Config::DOUBLE) {
        cpu_mrmr_base<T, double>(k, dat, results, config);

        if (config.doProfile()) {
            config.appendProfileEntry(profiler.stop_seconds(), "Algorithm execution");
        }
    } else if (config.getPrecision() == Config::SINGLE) {
        cpu_mrmr_base<T, float>(k, dat, results, config);

        if (config.doProfile()) {
            config.appendProfileEntry(profiler.stop_seconds(), "Algorithm execution");
        }
    } else if (config.getPrecision() == Config::FIXED) {
        LUT<uint16_t> lut(dat.num_samp);
        //lut.print();

        if (config.doProfile()) {
            config.appendProfileEntry(profiler.lap_seconds(), "Creation of lookup table");
        }

        cpu_mrmr_base<T, double>(k, dat, results, config, lut);

        if (config.doProfile()) {
            config.appendProfileEntry(profiler.stop_seconds(), "Algorithm execution");
        }
    }
}

template<>
void CPU::mrmr(unsigned int k, Dataset<unsigned int> const &dataset, Results &results) {
    Config default_config;
    return CPU::mrmr<unsigned int>(k, dataset, results, default_config);
}


