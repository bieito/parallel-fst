#ifndef CUDA_MRMR_LIBSVMPARSER_H
#define CUDA_MRMR_LIBSVMPARSER_H

#include "FileParser/FileParser.h"

// A .libsvm file has the values ordered by samples, so we must mix them
class LibSvmParser : public FileParser {
public:
    explicit LibSvmParser(std::string const &path) : FileParser(path) {};

    ~LibSvmParser() override { file.close(); }

    void readInput(Dataset<unsigned int> &dataset, unsigned int discBins) override;
};

#endif //CUDA_MRMR_LIBSVMPARSER_H
