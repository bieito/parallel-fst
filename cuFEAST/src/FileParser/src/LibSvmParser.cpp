#include <vector>

#include "LibSvmParser.h"

void LibSvmParser::readInput(Dataset<unsigned int> &dataset, unsigned int discBins) {
    std::string line;
    unsigned int rowFeatures, nSamples = 0;

    // Type depends on whether the input is already discrete
    std::vector<unsigned int *> vectorU;
    std::vector<double *> vectorD;
    std::vector<unsigned int *> vectorIndex;
    std::vector<int> classes;

    std::string auxStr;
    unsigned int *samplesU;
    double *samplesD;
    unsigned int *indexSamples;

    // This a format that can be sparse
    // In this variable we keep the maximum number of features
    unsigned int totalFeatures = 0;

    int lastReadLine = -1;
    int lastProcessedLine = -1;

    #pragma omp parallel default(none) shared(discBins, nSamples, totalFeatures, classes, vectorU, vectorD, vectorIndex, lastReadLine, lastProcessedLine) private(rowFeatures, samplesU, samplesD, indexSamples, auxStr) firstprivate(line)
    while (true) {
        bool stop = false;
        int currentThreadLine;

        #pragma omp critical
        {
            stop = file.eof();
            if (!stop) {
                getline(file, line);
            }
            lastReadLine += 1;
            currentThreadLine = lastReadLine;
        }
        if (stop) {
            break;
        }

        if ((line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
            // The number of features expressed in the current row
            rowFeatures = 0;

            size_t auxPos = -1;
            while ((auxPos = line.find(':', auxPos + 1)) != std::string::npos) {
                rowFeatures++;
            }

            if (discBins == 0) {
                samplesU = new unsigned int[rowFeatures];
            } else {
                samplesD = new double[rowFeatures];
            }
            // The first value of the indexes is the total number of features in the row
            indexSamples = new unsigned int[rowFeatures + 1];
            indexSamples[0] = rowFeatures;

            // First value of the row is the class
            auxStr = line.substr(0, line.find(' '));
            int classIdx = stoi(auxStr);

            size_t iniPos = 0, endPos = 0;

            for (unsigned int i = 0; i < rowFeatures; i++) {
                endPos = line.find(':', iniPos);

                // Find the index of the sample
                iniPos = endPos - 2;
                while ((line[iniPos] != ' ') && (line[iniPos] != '\t')) {
                    iniPos--;
                }

                iniPos++;
                auxStr = line.substr(iniPos, endPos - iniPos);
                indexSamples[i + 1] = stoul(auxStr) - 1; // It starts in 1

                // Find the value of the sample
                iniPos = endPos + 1;
                while ((line[endPos] != ' ') && (line[endPos] != '\t') && (line[endPos] != '\r') &&
                       (endPos < line.length())) {
                    endPos++;
                }

                auxStr = line.substr(iniPos, endPos - iniPos);

                if (discBins == 0) {
                    samplesU[i] = stoul(auxStr);
                } else {
                    samplesD[i] = stod(auxStr);
                }
            }

            #pragma omp critical
            // The last index can be larger than in any previous row
            if (indexSamples[rowFeatures] + 1 > totalFeatures) {
                totalFeatures = indexSamples[rowFeatures] + 1;
            }

            // Wait to write
            bool completed = false;
            do {
                #pragma omp critical
                if (lastProcessedLine == currentThreadLine - 1) {
                    classes.push_back(classIdx);
                    if (discBins == 0) {
                        vectorU.push_back(samplesU);
                    } else {
                        vectorD.push_back(samplesD);
                    }

                    vectorIndex.push_back(indexSamples);
                    lastProcessedLine = currentThreadLine;

                    completed = true;
                }
            } while (!completed);

            #pragma omp atomic
            nSamples++;
        } else {
            bool completed = false;
            do {
                #pragma omp critical
                if (lastProcessedLine == currentThreadLine - 1) {
                    lastProcessedLine = currentThreadLine;
                    completed = true;
                }
            } while (!completed);
        }
    }

    //Utils::log("INFO in LibsvmFileParser: %u features\n", totalFeatures);
    //Utils::log("INFO in LibsvmFileParser: %u samples\n", nSamples);

    if (discBins == 0) {
        FileParser::initializeDataset(dataset, totalFeatures, nSamples, vectorU, classes, vectorIndex);
    } else {
        FileParser::initializeDataset(dataset, totalFeatures, nSamples, vectorD, classes, vectorIndex, discBins);
    }

    if (discBins == 0) {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorU[i];
        }
    } else {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorD[i];
        }
    }

    for (unsigned int i = 0; i < nSamples; i++) {
        delete[] vectorIndex[i];
    }

    classes.clear();
    vectorD.clear();
    vectorU.clear();
    vectorIndex.clear();
}