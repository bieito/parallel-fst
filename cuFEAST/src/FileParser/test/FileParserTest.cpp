#include "TestTools/TestTools.h"

#include "FileParser/FileParser.h"
#include "FileParserTest.h"

Test(fileParserFactoryTest) {

    /* File not found exception */
    AssertIsThrown(std::invalid_argument, {
        FileParser::getFileParser("nofile.arff");
    })

    /* Unknown format exception */
    AssertIsThrown(std::invalid_argument, {
        FileParser::getFileParser("nofile.txt");
    })

}

Test(fileParserArffTest) {
    unsigned int reference[] = {0, 0, 1, 1,
                            1, 1, 0, 0,
                            1, 0, 1, 0};
    FileParser *fp;

    AssertNoThrown(std::invalid_argument, {
        fp = FileParser::getFileParser(std::string(FPTEST_PATH) + "test.arff");
    })

    Dataset<unsigned int> dat;
    fp->readInput(dat, 2);
    delete fp;

    AssertEquals(2u, dat.num_feat);
    AssertEquals(4u, dat.num_samp);
    AssertEquals(1u, dat.num_clas);
    for (unsigned int i = 0; i < dat.numElements(); ++i) {
        AssertEquals(reference[i], dat.raw_data[i]);
    }
}

Test(fileParserCsvTest) {
    unsigned int reference[] = {0, 1, 2,
                            3, 4, 5,
                            6, 7, 8};
    FileParser *fp;

    AssertNoThrown(std::invalid_argument, {
        fp = FileParser::getFileParser(std::string(FPTEST_PATH) + "test.csv");
    })

    Dataset<unsigned int> dat;
    fp->readInput(dat, 0);
    delete fp;

    AssertEquals(2u, dat.num_feat);
    AssertEquals(3u, dat.num_samp);
    AssertEquals(1u, dat.num_clas);
    for (unsigned int i = 0; i < dat.numElements(); ++i) {
        AssertEquals(reference[i], dat.raw_data[i]);
    }

}

Test(fileParserLibSvmTest) {
    unsigned int reference[] = {0, 0, 0, 2,
                            0, 0, 2, 0,
                            0, 0, 2, 2,
                            1, 2, 8, 9};
    FileParser *fp;

    AssertNoThrown(std::invalid_argument, {
        fp = FileParser::getFileParser(std::string(FPTEST_PATH) + "test.libsvm");
    })

    Dataset<unsigned int> dat;
    fp->readInput(dat, 3);
    delete fp;

    AssertEquals(3u, dat.num_feat);
    AssertEquals(4u, dat.num_samp);
    AssertEquals(1u, dat.num_clas);
    for (unsigned int i = 0; i < dat.numElements(); ++i) {
        AssertEquals(reference[i], dat.raw_data[i]);
    }

}

Test(fileParserFormatError) {
    FileParser *fp;

    fp = FileParser::getFileParser(std::string(FPTEST_PATH) + "test_csv.arff");

    Dataset<unsigned int> dat;
    try {
        fp->readInput(dat, 3);
    } catch (format_error &err) {
        /* Expected */
        AssertEquals(std::string("ERROR in ArffFileParser: The first section should be relation"),
                     std::string(err.what()));
    }

    delete fp;

}

int main() {

    AddTest(fileParserFactoryTest);
    AddTest(fileParserArffTest);
    AddTest(fileParserCsvTest);
    AddTest(fileParserLibSvmTest);
    AddTest(fileParserFormatError);

    return TestManager::runTests();
}