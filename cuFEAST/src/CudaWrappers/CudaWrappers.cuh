#ifndef CUFEAST_CUDADWRAPPERS_CUH
#define CUFEAST_CUDADWRAPPERS_CUH

#include <stdexcept>
#include <string>
#include <cstdio>

extern size_t MAX_SHARED_MEMORY_PER_BLOCK;

void checkCudaError();

void checkCudaError(unsigned int error);

void listCudaDevices();

int getCudaDefaultDevice();

int getCudaNumDevices();

void getCudaDefaultDeviceInfo(char buffer[]);

void configureCudaConstants(int device);

void resetCudaDevice();

void getMemInfo(size_t *free, size_t *total);

void getPointerAttributes(char buffer[], const void *ptr);

#endif //CUFEAST_CUDADWRAPPERS_CUH
