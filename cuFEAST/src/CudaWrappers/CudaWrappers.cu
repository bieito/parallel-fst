#include "CudaWrappers.cuh"

void checkCudaError() {
    checkCudaError(cudaGetLastError());
}

void checkCudaError(unsigned int error) {
    if (error != cudaError::cudaSuccess) {
        auto e = static_cast<cudaError_t>(error);
        throw std::runtime_error(
                std::string(cudaGetErrorName(e)) + ": " + std::string(cudaGetErrorString(e)));
    }
}

void listCudaDevices() {
    int count;

    cudaGetDeviceCount(&count);
    checkCudaError();

    if (count > 0) {
        printf("CUDA Device\n");
        cudaDeviceProp devp{};
        for (int i = 0; i < count; ++i) {
            cudaGetDeviceProperties(&devp, i);
            printf("  %2d. %s [%d.%d] %dSM @ %dMHz %zuGB @ %dMHz\n", i,
                   devp.name,
                   devp.major, devp.minor,
                   devp.multiProcessorCount,
                   devp.clockRate / (int) (1e3),
                   devp.totalGlobalMem / (size_t) (1e9),
                   devp.memoryClockRate / (int) (1e3));
        }
    } else {
        printf("No CUDA Devices available\n");
    }
}

int getCudaDefaultDevice() {
    int index;
    cudaGetDevice(&index);
    checkCudaError();
    return index;
}

int getCudaNumDevices() {
    int count;
    cudaGetDeviceCount(&count);
    checkCudaError();
    return count;
}

void getCudaDefaultDeviceInfo(char buffer[]) {
    cudaDeviceProp devp{};

    cudaGetDeviceProperties(&devp, getCudaDefaultDevice());
    for (int i = 0; i < 256; ++i) {
        buffer[i] = devp.name[i];
    }
}

void configureCudaConstants(int device) {
    cudaSetDevice(device);
    checkCudaError();

    cudaDeviceProp properties{};
    cudaGetDeviceProperties(&properties, device);
    checkCudaError();

    /* Set max memory size value */
    MAX_SHARED_MEMORY_PER_BLOCK = properties.sharedMemPerBlock;
}

void resetCudaDevice() {
    cudaDeviceReset();
    checkCudaError();
}

void getMemInfo(size_t *free, size_t *total) {
    cudaMemGetInfo(free, total);
    checkCudaError();
}

void getPointerAttributes(char buffer[], const void *ptr) {
    const char *types[] = {"Unregistered", "Host", "Device", "Managed"};

    cudaPointerAttributes attributes{};
    cudaPointerGetAttributes(&attributes, ptr);

    sprintf(buffer, "%s pointer %p | Device %d | Host @%p  Device @%p", types[attributes.type], ptr,
            attributes.device, attributes.hostPointer, attributes.devicePointer);
}