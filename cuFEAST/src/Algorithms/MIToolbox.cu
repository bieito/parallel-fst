#include "MIToolbox.h"

size_t MAX_SHARED_MEMORY_PER_BLOCK = 32 << 10;
__device__ __constant__ char const_data[64 << 10];

__device__ void memSet(void *__restrict__ s, char c, size_t n) {

    for (size_t pos = threadIdx.x; pos < n; pos += blockDim.x) {
        static_cast<char *>(s)[pos] = c;
    }
    __syncthreads();

}

__device__ void *globalOrShared(void *global, void **shmem_end, size_t size) {
    void *result;

    if (global == nullptr) {
        result = *shmem_end;

        char *shmem_end_as_byte = static_cast<char *>(*shmem_end);
        shmem_end_as_byte += size;

        *shmem_end = static_cast<void *>(shmem_end_as_byte);
    } else {
        result = global;
    }

    return result;
}

#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
#else

__device__ double atomicAdd(double *address, double val) {
    auto *address_as_ull =
            (unsigned long long int *) address;
    unsigned long long int old = *address_as_ull, assumed;

    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                                             __longlong_as_double(assumed)));

        // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
    } while (assumed != old);

    return __longlong_as_double(old);
}

#endif

__device__ uint16_t atomicAdd(uint16_t *address, uint16_t val) {
    /* If we load u16 memory as u32:
     * u16: [00] [01] [02] [03] ...
     * u32: [     00] [     01]
     * To add to the first  u16 -> + 0x0001 0000
     * To add to the second u16 -> + 0x0000 0001
     * u16 is 4B -> offset between 2 addresses is 0b0010 -> 0x0 [0000], 0x2 [0010], 0x4 [0100], 0x6 [0110] ...
     *                                                   -> xx0x (first), xx1x (second) -> mask with ~0b0010
     * u32 is 8B -> offset between 2 addresses is 0b0100 -> 0x0 [0000], 0x4 [0100], 0x8 [1000], 0xC [1100]
     * */
    auto address_as_u = (unsigned long int) address;
    auto value = (unsigned int) val;

    // We must assert we access memory in a 32bit fashion -> We have to use &[00] for both [00] or [01]
    auto *address_u32 = (unsigned int *) ((size_t) address & (~0b0011));

    if (address_as_u & 0b0010) {
        // "Lower"/First 2 bytes
        value = value << 16;
    } else {
        // "Upper"/Second 2 bytes
        value = value & 0xFFFF;
    }

    auto res = (unsigned int) atomicAdd(address_u32, value);

    if (address_as_u & 0b0010) {
        // "Lower"/First 2 bytes
        res = res >> 16;
    } else {
        // "Upper"/Second 2 bytes
    }

    return res;
}

__device__ uint16_t atomicAdd_block(uint16_t *address, uint16_t val) {
    /* If we load u16 memory as u32:
     * u16: [00] [01] [02] [03] ...
     * u32: [     00] [     01]
     * To add to the first  u16 -> + 0x0001 0000
     * To add to the second u16 -> + 0x0000 0001
     * u16 is 4B -> offset between 2 addresses is 0b0010 -> 0x0 [0000], 0x2 [0010], 0x4 [0100], 0x6 [0110] ...
     *                                                   -> xx0x (first), xx1x (second) -> mask with ~0b0010
     * u32 is 8B -> offset between 2 addresses is 0b0100 -> 0x0 [0000], 0x4 [0100], 0x8 [1000], 0xC [1100]
     * */
    auto address_as_u = (unsigned long int) address;
    auto value = (unsigned int) val;

    // We must assert we access memory in a 32bit fashion -> We have to use &[00] for both [00] or [01]
    auto *address_u32 = (unsigned int *) ((size_t) address & (~0b0011));

    if (address_as_u & 0b0010) {
        // "Lower"/First 2 bytes
        value = value << 16;
    } else {
        // "Upper"/Second 2 bytes
        value = value & 0xFFFF;
    }

    auto res = (unsigned int) atomicAdd_block(address_u32, value);

    if (address_as_u & 0b0010) {
        // "Lower"/First 2 bytes
        res = res >> 16;
    } else {
        // "Upper"/Second 2 bytes
    }

    return res;
}