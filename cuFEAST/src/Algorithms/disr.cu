#include <cuFEAST/FSAlgorithm.h>
#include <Profiler/Profiler.h>

#include "LUT/lut.h"
#include "CudaWrappers/CudaWrappers.cuh"
#include "FSToolbox.h"
#include "MIToolbox.h"
#include "MIToolboxLUT.h"

void addProfileEntriesDISR(Config &config, double *times) {
    config.appendProfileEntry(times[9], "+-- DISR setup & Allocs");
    config.appendProfileEntry(times[10], "|   Computation of Max States");
    config.appendProfileEntry(times[11], "|   Compute class MI and select first");
    config.appendProfileEntry(times[1], "|   +-- Initial setup & Allocs");
    if (config.getNumStreams() > 0) {
        config.appendProfileEntry(times[2], "|   |   Quasi-Async work (transferences and kernels)");
    } else {
        config.appendProfileEntry(times[7], "|   |   Memory transfers && resets");
        config.appendProfileEntry(times[8],
                                  "|   |   Merge Arrays & Compute Hist2D & Compute MI (launch & wait for kernels)");
    }
    config.appendProfileEntry(times[3], "|   |   Wait for last kernel, copy results from GPU and divide");
    config.appendProfileEntry(times[4], "|   |   Deallocs");
    config.appendProfileEntry(times[0], "|   Computation of score");
    config.appendProfileEntry(times[12] - times[0], "|   DISR calculations");
}

template<typename D, typename R>
void disr_base(unsigned int k, Dataset<D> const &dat, Results &results, Config &config, D *d_data) {
    Profiler profiler;
    profiler.start();

    /* Array for time measurement */
    double times[16] = {0};

    /* MI values */
    R *class_mi = new R[dat.num_feat]; // MI of each feature with class
    R *feature_mi_matrix = new R[k * dat.num_feat]; // MI of each feature with selected

    /* Selected features map */
    auto *selected_features = new bool[dat.num_feat]();

    /* Cache for maxState of all features and class */
    auto *max_states = new unsigned int[dat.num_feat + 1];
    unsigned int max_state = 0, min_state = 2 << 16;

    if (config.doProfile()) {
        times[9] += profiler.lapSeconds();
    }

    analyzeStates(dat.raw_data, dat.num_feat, dat.num_samp, max_states, &max_state, &min_state);

    // Compute class histogram -> constant for all iterations
    unsigned int *d_class_sc;
    classHistogramToSymbol(dat.classes, max_states[dat.num_feat], dat.num_samp, &d_class_sc);

    if (config.doProfile()) {
        times[10] += profiler.lapSeconds();
    }

    /* Select first based only on relevance with class */
    calcMutualInformationAll<D, R>(dat.raw_data, d_data, dat.num_feat, dat.num_samp, dat.num_feat, times, class_mi,
                                   config, selected_features, max_states, min_state, max_state, false);

    selectFeature(dat.num_feat, selected_features, results, fsCriterionMIM, fs_criterion_params<R>(class_mi));

    if (config.doProfile()) {
        times[11] += profiler.lapSeconds();
    }

    for (unsigned int i = 1; i < k; i++) {
        // First use kernel, then sum cached values
        mergeAndCalcMutualInformationAndJointEntropyAll<D, R>(d_data, results[i - 1].first, dat.num_samp, dat.num_feat,
                                                              times, &feature_mi_matrix[(i - 1) * dat.num_feat], config,
                                                              selected_features, max_states, max_state,
                                                              config.doProfile(), d_class_sc);

        selectFeature(dat.num_feat, selected_features, results, fsCriterionDISR,
                      fs_criterion_params<R>(class_mi, feature_mi_matrix, i));
    }

    if (config.doProfile()) {
        times[12] += profiler.lapSeconds();
        addProfileEntriesDISR(config, times);
    }

    delete[] max_states;
    delete[] selected_features;
    delete[] feature_mi_matrix;
    delete[] class_mi;
}

template<typename D, typename R>
void disr_base_lut(unsigned int k, Dataset<D> const &dat, Results &results, Config &config, D *d_data,
                   uint16_t *d_lut_data, double lut_bf) {
    Profiler profiler;
    profiler.start();

    /* Array for time measurement */
    double times[16] = {0};

    /* MI values */
    R *class_mi = new R[dat.num_feat]; // MI of each feature with class
    R *feature_mi_matrix = new R[k * dat.num_feat]; // MI of each feature with selected

    /* Selected features map */
    auto *selected_features = new bool[dat.num_feat]();

    /* Cache for maxState of all features and class */
    auto *max_states = new unsigned int[dat.num_feat + 1];
    unsigned int max_state = 0, min_state = 2 << 16;

    if (config.doProfile()) {
        times[9] += profiler.lapSeconds();
    }

    analyzeStates(dat.raw_data, dat.num_feat, dat.num_samp, max_states, &max_state, &min_state);

    // Compute class histogram -> constant for all iterations
    unsigned int *d_class_sc;
    classHistogramToSymbol(dat.classes, max_states[dat.num_feat], dat.num_samp, &d_class_sc);

    if (config.doProfile()) {
        times[10] += profiler.lapSeconds();
    }

    /* Select first based only on relevance with class */
    calcMutualInformationAll<D, R>(dat.raw_data, d_data, dat.num_feat, dat.num_samp, dat.num_feat, times, class_mi,
                                   config, selected_features, max_states, min_state, max_state, false, d_lut_data,
                                   lut_bf);

    selectFeature(dat.num_feat, selected_features, results, fsCriterionMIM, fs_criterion_params<R>(class_mi));

    if (config.doProfile()) {
        times[11] += profiler.lapSeconds();
    }

    for (unsigned int i = 1; i < k; i++) {
        // First use kernel, then sum cached values
        mergeAndCalcMutualInformationAndJointEntropyAll<D, R>(d_data, results[i - 1].first, dat.num_samp, dat.num_feat,
                                                              times, &feature_mi_matrix[(i - 1) * dat.num_feat], config,
                                                              selected_features, max_states, max_state,
                                                              config.doProfile(), d_class_sc, d_lut_data, lut_bf);

        selectFeature(dat.num_feat, selected_features, results, fsCriterionDISR,
                      fs_criterion_params<R>(class_mi, feature_mi_matrix, i));
    }

    if (config.doProfile()) {
        times[12] += profiler.lapSeconds();
        addProfileEntriesDISR(config, times);
    }

    delete[] max_states;
    delete[] selected_features;
    delete[] feature_mi_matrix;
    delete[] class_mi;
}

/****************************************************************************** With Lookup Table and fixed data type */

template<typename T>
void disr(unsigned int k, Dataset<T> const &dat/*dataset*/, Results &results, Config &config) {

    // From now on, we suppose there is only 1 row for classes

    // Set k to min(k, num_feat)
    if (k > dat.num_feat) {
        k = dat.num_feat;
    }

    configureCudaConstants(config.getGpuIndex());

    /* Profiling with events */
    cudaEvent_t start, stop;
    if (config.doProfile()) {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        checkCudaError();
    }

    /* LookUp Table */
    double lut_bf = 1;
    uint16_t *d_lut_data = nullptr;
    if (config.getPrecision() == Config::FIXED) {
        if (config.doProfile()) {
            cudaEventRecord(start);
        }

        LUT<uint16_t> h_lut(dat.num_samp);
        lut_bf = -h_lut.toDouble(1);

        cudaMalloc(&d_lut_data, h_lut.getSize());
        cudaMemcpy(d_lut_data, h_lut.getRawData(), h_lut.getSize(), cudaMemcpyHostToDevice);

        if (config.doProfile()) {
            cudaEventRecord(stop);
            cudaEventSynchronize(stop);
            float ms;
            cudaEventElapsedTime(&ms, start, stop);
            checkCudaError();
            config.appendProfileEntry(ms * 1e-3, "Lookup Table creation & transfer");
        }
    }

    /* Alloc memory */
    T *d_data;
    if (config.doProfile()) {
        cudaEventRecord(start);
    }
    cudaMalloc(&d_data, dat.numBytes());
    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Memory alloc");
    }

    /* Copy data host -> device */
    if (config.doProfile()) {
        cudaEventRecord(start);
    }
    cudaMemcpy(d_data, dat.raw_data, dat.numBytes(), cudaMemcpyHostToDevice);
    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Copy dataset Host->Device (" + std::to_string(dat.numBytes()) + " B)");
    }

    /* Launch kernel */
    if (config.doProfile()) {
        cudaEventRecord(start);
    }

    // reset last error if any
    cudaGetLastError();
    if (config.getPrecision() == Config::DOUBLE) {
        disr_base<T, double>(k, dat, results, config, d_data);
    } else if (config.getPrecision() == Config::SINGLE) {
        disr_base<T, float>(k, dat, results, config, d_data);
    } else if (config.getPrecision() == Config::FIXED) {
        disr_base_lut<T, double>(k, dat, results, config, d_data, d_lut_data, lut_bf);
    }

    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Algorithm execution");
    }

    /* Free memory */
    if (config.doProfile()) {
        cudaEventRecord(start);
    }
    if (config.getPrecision() == Config::FIXED) {
        cudaFree(d_lut_data);
    }
    cudaFree(d_data);
    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Memory dealloc");
    }

    /* Free events */
    if (config.doProfile()) {
        cudaEventDestroy(stop);
        cudaEventDestroy(start);
        checkCudaError();
    }
}

template<typename T>
void cuFEAST::DISR<T>::run(const Dataset<T> &dataset, Results &results, Config &config) {
    disr<T>(this->getK(), dataset, results, config);
}