#ifndef CUFEAST_MITOOLBOX_H
#define CUFEAST_MITOOLBOX_H

#include <cuFEAST/FSAlgorithm.h>
#include <Profiler/Profiler.h>
#include "CudaWrappers/CudaWrappers.cuh"

#define LOG_BASE 2.0

extern __constant__ char const_data[];

#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
#else
__device__ double atomicAdd(double *address, double val);
#endif

__device__ void memSet(void *s, char c, size_t n);

__device__ uint16_t atomicAdd(uint16_t *address, uint16_t val);

__device__ uint16_t atomicAdd_block(uint16_t *address, uint16_t val);

template<typename T>
int maxState(const T *__restrict__ vector, int length) {
    T max = 0;

    for (int i = 0; i < length; i++) {
        if (vector[i] > max) {
            max = vector[i];
        }
    }

    return max + 1;
}

template<typename T>
void analyzeStates(const T *__restrict__ dataset, unsigned int num_features, unsigned int num_samples,
                   T *__restrict__ num_states_v, T *max_state, T *min_state) {
    T max_state_res = 0;
    T min_state_res = 2 << 16;

#pragma omp parallel for default(none) shared(dataset, num_features, num_samples, num_states_v) reduction(min: min_state_res) reduction(max: max_state_res)
    for (unsigned int i = 0; i < num_features; ++i) {
        num_states_v[i] = maxState(&dataset[i * num_samples], num_samples);

        if (num_states_v[i] > max_state_res) {
            max_state_res = num_states_v[i];
        }
        if (num_states_v[i] < min_state_res) {
            min_state_res = num_states_v[i];
        }
    }

    // states of class
    num_states_v[num_features] = maxState(&dataset[num_features * num_samples], num_samples);

    // store results
    *max_state = max_state_res;
    *min_state = min_state_res;
}

template<typename D>
void classHistogramToSymbol(const D *class_data, unsigned int class_ns, unsigned int num_samples,
                            unsigned int **d_class_sc) {
    auto *class_histogram = new unsigned int[class_ns]();

    for (unsigned int i = 0; i < num_samples; i++) {
        class_histogram[class_data[i]] += 1;
    }

    cudaGetSymbolAddress((void **) d_class_sc, const_data);
    cudaMemcpy(*d_class_sc, class_histogram, class_ns * sizeof(unsigned int), cudaMemcpyHostToDevice);

    delete[] class_histogram;
}

template<typename T>
__device__ T *globalOrShared(T *__restrict__ global, T *__restrict__ *shmem_end, size_t size) {
    T *result;

    if (global == nullptr) {
        result = *shmem_end;
        *shmem_end = reinterpret_cast<T *>((reinterpret_cast<char *>(*shmem_end)) + size);
    } else {
        result = global;
    }

    return result;
}

template<typename D>
__device__ void dataJointHistograms(const D *__restrict__ target_vector, const D *__restrict__ data_vector,
                                    uint16_t *__restrict__ data_histogram, uint16_t *__restrict__ joint_histogram,
                                    size_t vector_length, unsigned int hist_width) {
    D target_value, data_value;

    for (unsigned int pos = threadIdx.x; pos < vector_length; pos += blockDim.x) {
        target_value = target_vector[pos];
        data_value = data_vector[pos];

        atomicAdd_block(&data_histogram[data_value], 1);
        atomicAdd_block(&joint_histogram[target_value * hist_width + data_value], 1);
    }

    __syncthreads();
}

template<typename D>
__device__ void mergedDataJointHistograms(const D *__restrict__ target_vector, const D *__restrict__ first_vector,
                                          const D *__restrict__ second_vector, unsigned int *__restrict__ state_map,
                                          uint16_t *__restrict__ data_histogram, uint16_t *__restrict__ joint_histogram,
                                          size_t vector_length, unsigned int state_map_width, unsigned int hist_width,
                                          unsigned int *state_count) {
    if (threadIdx.x == 0) {
        *state_count = 1;
    }
    __syncthreads();

    for (unsigned int pos = threadIdx.x; pos < vector_length; pos += blockDim.x) {
        // Step 1: All threads load data from global -> local
        unsigned int map_pos = first_vector[pos] + (second_vector[pos] * state_map_width);
        D target_value = target_vector[pos];

        // Step 2: Critical section
        unsigned int map_value;
        // if state_map[pos] == 0 -> new value, lock position until we have state_count
        // if state_map[pos] in [1, vector_length) -> not a new value, take value for output vector
        // if state_map[pos] >= vector_length -> new value, but other thread is computing it at the moment -> wait
        do {
            map_value = atomicCAS_block(&state_map[map_pos], 0, vector_length + 1);
        } while (map_value > vector_length);

        if (map_value == 0) {
            // this thread has read a value of 0 at the memory position, and changed it to V+1 (a value that could not be reached with state_count)
            // as it read a 0, this thread is the first to access this position, so it has to give it a value (state_count + 1)
            unsigned int previous_state_count = atomicAdd_block(state_count, 1);
            state_map[map_pos] = previous_state_count;
            map_value = previous_state_count;
        } //else {
        // this thread read a value between 1 and V at the memory position, but it did not change the value
        // as it read something between 1 and V, this thread is not the first to access this position, so it only had to read the value (map_value)
        //}

        // Step 3: Instead of writing to an intermediate mergedArray -> increment histograms
        D data_value = map_value - 1;
        atomicAdd_block(&data_histogram[data_value], 1);
        atomicAdd_block(&joint_histogram[target_value * hist_width + data_value], 1);
    }

    __syncthreads();
}

template<typename R>
__device__ void computeMI(const unsigned *__restrict__ target_histogram, const uint16_t *__restrict__ data_histogram,
                          const uint16_t *__restrict__ joint_histogram, uint16_t hist_width, uint16_t hist_height,
                          uint16_t hist_wpad, R vector_length_inv, R *__restrict__ mutual_information) {
    R partial_mi = 0;

    for (unsigned int col = threadIdx.x; col < hist_width; col += blockDim.x) {
        if (data_histogram[col] > 0) {
            R dSP_fI_dI = data_histogram[col] * vector_length_inv;

            for (unsigned int row = 0; row < hist_height; ++row) {
                unsigned int i = row * (hist_width + hist_wpad) + col;
                R jSP_fI_i = joint_histogram[i] * vector_length_inv;

                if ((jSP_fI_i > 0) && (target_histogram[row] > 0)) {
                    partial_mi += jSP_fI_i * log2((jSP_fI_i / dSP_fI_dI) / (target_histogram[row] * vector_length_inv));
                }
            }
        }
    }

    if (threadIdx.x < hist_width) {
        atomicAdd_block(mutual_information, partial_mi);
    }
}

template<typename R>
__device__ void computeMIJE(const unsigned *__restrict__ target_histogram, const uint16_t *__restrict__ data_histogram,
                            const uint16_t *__restrict__ joint_histogram, uint16_t hist_width, uint16_t hist_height,
                            uint16_t hist_wpad, R vector_length_inv, R *__restrict__ mutual_information) {
    __shared__ R shared_mi, shared_je;
    R partial_mi = 0, partial_je = 0;

    if (threadIdx.x == 0) {
        shared_je = 0;
        shared_mi = 0;
    }
    __syncthreads();

    for (unsigned int col = threadIdx.x; col < hist_width; col += blockDim.x) {
        if (data_histogram[col] > 0) {
            R dSP_fI_dI = data_histogram[col] * vector_length_inv;

            for (unsigned int row = 0; row < hist_height; ++row) {
                unsigned int i = row * (hist_width + hist_wpad) + col;
                R jSP_fI_i = joint_histogram[i] * vector_length_inv;

                if ((jSP_fI_i > 0) && (target_histogram[row] > 0)) {
                    partial_je -= jSP_fI_i * log2(jSP_fI_i);
                    partial_mi += jSP_fI_i * log2((jSP_fI_i / dSP_fI_dI) / (target_histogram[row] * vector_length_inv));
                }
            }
        }
    }

    if (threadIdx.x < hist_width) {
        atomicAdd_block(&shared_je, partial_je);
        atomicAdd_block(&shared_mi, partial_mi);
    }
    __syncthreads();

    if (threadIdx.x == 0) {
        *mutual_information = shared_mi / shared_je;
    }
}

template<typename D, typename R>
__global__ void k_hist_and_cMI_batch(D *device_data, unsigned int target_idx, unsigned int *data_idx_v,
                                     unsigned int vector_length, uint16_t **data_histogram_v, uint16_t *data_ns_v,
                                     unsigned int *target_histogram, uint16_t target_ns, uint16_t **joint_histogram_v,
                                     R *mutual_information, R vector_length_inv) {
    extern __shared__ uint16_t data[];
    const unsigned int data_idx = data_idx_v[blockIdx.x];
    const uint16_t data_ns = data_ns_v[blockIdx.x];

    // Step 1: Choose global or shared memory
    uint16_t *shmem_end = data;
    uint16_t *data_histogram = globalOrShared(data_histogram_v[blockIdx.x], &shmem_end, data_ns * sizeof(uint16_t));
    uint16_t *joint_histogram = globalOrShared(joint_histogram_v[blockIdx.x], &shmem_end,
                                               data_ns * target_ns * sizeof(uint16_t));
    // Reset shared memory
    memSet(data, 0, (shmem_end - data) * sizeof(uint16_t));

    // Step 2: Create histograms
    dataJointHistograms(&device_data[target_idx * vector_length], &device_data[data_idx * vector_length],
                        data_histogram, joint_histogram, vector_length, data_ns);

    // Step 3: Compute MI
    computeMI(target_histogram, data_histogram, joint_histogram, data_ns, target_ns, 0, vector_length_inv,
              &mutual_information[data_idx]);
}

template<typename D, typename R>
__global__ void k_merge_and_hist_and_cMI_batch(D *device_data, D *device_class_data, unsigned int target_idx,
                                               unsigned int *data_idx_v, unsigned int vector_length,
                                               unsigned int **mergemap_v, uint16_t **merged_histogram_v,
                                               uint16_t *data_ns_v, unsigned int *class_histogram,
                                               unsigned int class_ns, uint16_t **joint_histogram_v,
                                               unsigned int target_ns, R *mutual_information,
                                               R vector_length_inv) {
    __shared__ unsigned int state_count;
    extern __shared__ uint16_t data[];
    const unsigned int data_idx = data_idx_v[blockIdx.x];
    const uint16_t data_ns = data_ns_v[blockIdx.x];
    const unsigned int max_merged_ns = min(data_ns * target_ns, vector_length);

    // Step 1: Choose global or shared memory
    uint16_t *shmem_end = data;
    uint16_t *merged_histogram = globalOrShared(merged_histogram_v[blockIdx.x], &shmem_end,
                                                max_merged_ns * sizeof(uint16_t));
    uint16_t *joint_histogram = globalOrShared(joint_histogram_v[blockIdx.x], &shmem_end,
                                               max_merged_ns * class_ns * sizeof(uint16_t));
    unsigned int *state_map = globalOrShared(mergemap_v[blockIdx.x], (unsigned int **) &shmem_end,
                                             data_ns * target_ns * sizeof(unsigned int));

    // Reset shared memory
    memSet(data, 0, (shmem_end - data) * sizeof(uint16_t));

    // Step 2: Merge vectors "on the fly" and create histograms
    mergedDataJointHistograms(device_class_data, &device_data[target_idx * vector_length],
                              &device_data[data_idx * vector_length], state_map, merged_histogram,
                              joint_histogram, vector_length, data_ns, max_merged_ns, &state_count);

    // Step 3: Compute MI
    computeMI(class_histogram, merged_histogram, joint_histogram, state_count - 1, class_ns,
              max_merged_ns - (state_count - 1), vector_length_inv, &mutual_information[data_idx]);
}

template<typename D, typename R>
__global__ void k_merge_and_hist_and_cMIJE_batch(D *device_data, D *device_class_data, unsigned int target_idx,
                                                 unsigned int *data_idx_v, unsigned int vector_length,
                                                 unsigned int **mergemap_v, uint16_t **merged_histogram_v,
                                                 uint16_t *data_ns_v, unsigned int *class_histogram,
                                                 unsigned int class_ns, uint16_t **joint_histogram_v,
                                                 unsigned int target_ns, R *mutual_information,
                                                 R vector_length_inv) {
    __shared__ unsigned int state_count;
    extern __shared__ uint16_t data[];
    const unsigned int data_idx = data_idx_v[blockIdx.x];
    const uint16_t data_ns = data_ns_v[blockIdx.x];
    const unsigned int max_merged_ns = min(data_ns * target_ns, vector_length);

    // Step 1: Choose global or shared memory
    uint16_t *shmem_end = data;
    uint16_t *merged_histogram = globalOrShared(merged_histogram_v[blockIdx.x], &shmem_end,
                                                max_merged_ns * sizeof(uint16_t));
    uint16_t *joint_histogram = globalOrShared(joint_histogram_v[blockIdx.x], &shmem_end,
                                               max_merged_ns * class_ns * sizeof(uint16_t));
    unsigned int *state_map = globalOrShared(mergemap_v[blockIdx.x], (unsigned int **) &shmem_end,
                                             data_ns * target_ns * sizeof(unsigned int));

    // Reset shared memory
    memSet(data, 0, (shmem_end - data) * sizeof(uint16_t));

    // Step 2: Merge vectors "on the fly" and create histograms
    mergedDataJointHistograms(device_class_data, &device_data[target_idx * vector_length],
                              &device_data[data_idx * vector_length], state_map, merged_histogram,
                              joint_histogram, vector_length, data_ns, max_merged_ns, &state_count);

    // Step 3: Compute MI and JE
    computeMIJE(class_histogram, merged_histogram, joint_histogram, state_count - 1, class_ns,
                max_merged_ns - (state_count - 1), vector_length_inv, &mutual_information[data_idx]);
}

template<typename D, typename R>
__host__ void calcMutualInformationAll(D *host_data, D *device_data, unsigned int target_idx,
                                       unsigned int vector_length, unsigned int num_features, double *times,
                                       R *mutual_information, Config const &config, const bool *selected_features,
                                       unsigned int *num_states, unsigned int min_state, unsigned int max_state,
                                       bool profile) {

    /***
     * Calc mutual information between feature *target_idx* and all the other
     ***/

    const unsigned int blockSize = config.getBlockSize();
    dim3 blockDim(blockSize);
    const R vector_length_inv = (R) 1.0 / vector_length;
    cudaError_t error;

    Profiler profiler;
    Profiler::time_point start = profiler.start();

    //auto timepoint = std::chrono::high_resolution_clock::now();
    //auto start = std::chrono::high_resolution_clock::now();

    /* Alloc memory for results */
    R *d_mutual_information;
    cudaMalloc(&d_mutual_information, num_features * sizeof(R));
    cudaMemset(d_mutual_information, 0, num_features * sizeof(R));

    /* Alloc and compute target feature states */
    uint16_t target_ns = num_states[target_idx];
    auto target_histogram = new unsigned int[target_ns]();
    unsigned int *d_target_histogram;
    cudaMalloc(&d_target_histogram, target_ns * sizeof(unsigned int));
    D *target_vector = &host_data[target_idx * vector_length];
    for (unsigned int i = 0; i < vector_length; i++) {
        target_histogram[target_vector[i]] += 1;
    }
    cudaMemcpy(d_target_histogram, target_histogram, target_ns * sizeof(unsigned int), cudaMemcpyHostToDevice);
    //cudaMemcpyToSymbol(d_target_histogram, target_histogram, target_ns * sizeof(double));

    /* Create streams */
    bool no_async = config.getNumStreams() == 0;
    unsigned int num_streams = (no_async) ? 1 : config.getNumStreams();
    unsigned int stream_id = 0;
    cudaStream_t streams[num_streams];
    for (cudaStream_t &s: streams) {
        cudaStreamCreate(&s);
    }

    /* Calculate batch sizes */
    // max sum of numStates per batch
    unsigned long int max_ns_sum = config.getBatchFeatures() * max_state;
    // max features per batch
    unsigned int max_features = (max_ns_sum + min_state - 1) / min_state;
    if (max_features > num_features) {
        max_features = num_features;
        max_ns_sum = max_features * max_state;
    }

    /* Setup memory pools */
    uint16_t *d_data_histogram_pool, *d_joint_histogram_pool;
    error = cudaMalloc(&d_joint_histogram_pool, max_ns_sum * target_ns * num_streams * sizeof(uint16_t));
    checkCudaError(error);
    error = cudaMalloc(&d_data_histogram_pool, max_ns_sum * num_streams * sizeof(uint16_t));
    checkCudaError(error);

    unsigned int *batch_feature_idx_pool;
    uint16_t *data_ns_pool;
    // pointers to histograms in device memory
    uint16_t **dptr_data_histogram_pool, **dptr_joint_histogram_pool;
    cudaMallocHost(&dptr_data_histogram_pool, max_features * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&dptr_joint_histogram_pool, max_features * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&batch_feature_idx_pool, max_features * num_streams * sizeof(unsigned int));
    cudaMallocHost(&data_ns_pool, max_features * num_streams * sizeof(uint16_t));

    if (profile) {
        times[1] += profiler.lapSeconds();
    }

    // starting with a while loop because block factor could become dynamically adjustable
    // lets treat each iteration as a task, where we calculate the MI for *batch_features* features
    unsigned int feature_idx = 0;
    while (feature_idx < num_features) {
        if (profile && no_async) {
            profiler.start();
        }
        // Setup stream and pointers (to the memory zone of this stream)
        unsigned int stream_pos = stream_id % num_streams;
        cudaStream_t &stream = streams[stream_pos];
        ++stream_id;

        unsigned int *batch_feature_idx_v = &batch_feature_idx_pool[stream_pos * max_features];
        uint16_t *data_ns_v = &data_ns_pool[stream_pos * max_features];

        uint16_t **dptr_data_histogram_v = &dptr_data_histogram_pool[stream_pos * max_features];
        uint16_t **dptr_joint_histogram_v = &dptr_joint_histogram_pool[stream_pos * max_features];
        // first pointer -> start of mem zone
        uint16_t *d_data_histogram_pool_start = &d_data_histogram_pool[stream_pos * max_ns_sum];
        uint16_t *d_joint_histogram_pool_start = &d_joint_histogram_pool[stream_pos * max_ns_sum * target_ns];

        // Get the next *batch_features* features that have no MI computed

        // MANDATORY vars of previous iter could have not been copied to GPU yet
        cudaStreamSynchronize(stream);
        // number of features in this task -> *batch_features* except for last task // TODO must be less than 65535
        unsigned int f_count = 0;
        unsigned int ns_sum = 0;
        while ((ns_sum < max_ns_sum) && (feature_idx < num_features)) {
            if (!selected_features[feature_idx]) {
                unsigned int dns = num_states[feature_idx];

                // assert max_dns is not surpassed
                if (ns_sum + dns > max_ns_sum) {
                    break;
                }

                batch_feature_idx_v[f_count] = feature_idx;
                data_ns_v[f_count] = dns;
                ns_sum += dns;

                ++f_count;
            }
            ++feature_idx;
        }
        if (!f_count) {
            continue;
        }

        /* Set global histogram offsets OR null to use shared memory
         * indexes = [0, 2, 3, 4]
         * ptr_histogram = [0x0, 0x10, nullptr, 0x20]
         *  feature 0 uses global histogram [0x0, ..., 0xf]
         *  feature 2 uses global histogram [0x10, ..., 0x1f]
         *  feature 3 uses shared mem histogram
         *  feature 4 uses global histogram [0x20, ..., 0x2f]
         * */
        size_t data_histogram_offset = 0, joint_histogram_offset = 0;
        bool use_shmem = config.useSharedMem();
        size_t sharedMem = 0;
        for (unsigned int f = 0; f < f_count; ++f) {
            unsigned int ns = data_ns_v[f];

            size_t datahist_size = ns * sizeof(uint16_t);
            size_t jointhist_size = ns * target_ns * sizeof(uint16_t);

            size_t needed_shmem = 0;
            if (use_shmem && (needed_shmem + jointhist_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_joint_histogram_v[f] = nullptr;
                needed_shmem += jointhist_size;
            } else {
                dptr_joint_histogram_v[f] = d_joint_histogram_pool_start + joint_histogram_offset;
                joint_histogram_offset += ns * target_ns;
            }
            if (use_shmem && (needed_shmem + datahist_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_data_histogram_v[f] = nullptr;
                needed_shmem += datahist_size;
            } else {
                dptr_data_histogram_v[f] = d_data_histogram_pool_start + data_histogram_offset;
                data_histogram_offset += ns;
            }

            // Find max needed shared memory
            sharedMem = (needed_shmem > sharedMem) ? needed_shmem : sharedMem;
        }

        if (profile && no_async) {
            times[5] += profiler.lapSeconds();
        }

        // Transfer data and reset memory
        cudaMemsetAsync(d_data_histogram_pool_start, 0, data_histogram_offset * sizeof(uint16_t));
        cudaMemsetAsync(d_joint_histogram_pool_start, 0, joint_histogram_offset * sizeof(uint16_t));


        if (profile && no_async) {
            cudaStreamSynchronize(stream);
            times[6] += profiler.lapSeconds();
        }

        // Launch kernel
        dim3 gridDim(f_count);
        k_hist_and_cMI_batch<D, R><<<gridDim, blockDim, sharedMem, stream>>>(device_data, target_idx,
                                                                             batch_feature_idx_v, vector_length,
                                                                             dptr_data_histogram_v, data_ns_v,
                                                                             d_target_histogram, target_ns,
                                                                             dptr_joint_histogram_v,
                                                                             d_mutual_information, vector_length_inv);

        if (profile && no_async) {
            times[7] += profiler.lapSeconds();
            cudaStreamSynchronize(stream);
            times[8] += profiler.lapSeconds();
        }

    }

    if (profile) {
        times[2] += profiler.lapSeconds();
    }

    // Copy results back
    cudaDeviceSynchronize();
    checkCudaError(cudaGetLastError());
    cudaMemcpy(mutual_information, d_mutual_information, num_features * sizeof(R), cudaMemcpyDeviceToHost);

    if (profile) {
        times[3] += profiler.lapSeconds();
    }

    // Free memory
    for (cudaStream_t &s: streams) {
        cudaStreamDestroy(s);
    }

    cudaFreeHost(data_ns_pool);
    cudaFreeHost(batch_feature_idx_pool);
    cudaFreeHost(dptr_data_histogram_pool);
    cudaFreeHost(dptr_joint_histogram_pool);
    cudaFree(d_data_histogram_pool);
    cudaFree(d_joint_histogram_pool);
    delete[] target_histogram;
    cudaFree(d_target_histogram);
    cudaFree(d_mutual_information);

    if (profile) {
        times[4] += profiler.lapSeconds();
        times[0] += Profiler::secondsFrom(start);
    }
}

template<typename D, typename R>
__host__ void mergeAndCalcMutualInformationAll(D *device_data, unsigned int target_idx, unsigned int vector_length,
                                               unsigned int num_features, double *times, R *mutual_information,
                                               Config const &config, const bool *selected_features,
                                               unsigned int *num_states, unsigned int max_state, bool profile,
                                               unsigned int *d_class_histogram) {

    /***
     * Calc mutual information between the class and the result of merging feature *target_idx* and all the other
    ***/

    const unsigned int blockSize = config.getBlockSize();
    dim3 blockDim(blockSize);
    const R vector_length_inv = (R) 1.0 / vector_length;
    cudaError_t error;
    D *device_class_data = &device_data[num_features * vector_length];

    Profiler profiler;
    Profiler::time_point start = profiler.start();

    /* Alloc memory for results */
    R *d_mutual_information;
    cudaMalloc(&d_mutual_information, num_features * sizeof(R));
    cudaMemset(d_mutual_information, 0, num_features * sizeof(R));

    /* Create streams */
    bool no_async = config.getNumStreams() == 0;
    unsigned int num_streams = (no_async) ? 1 : config.getNumStreams();
    unsigned int stream_id = 0;
    cudaStream_t streams[num_streams];
    for (cudaStream_t &s: streams) {
        cudaStreamCreate(&s);
    }

    /* Calculate batch sizes */
    unsigned int max_features = config.getBatchFeatures() < num_features ? config.getBatchFeatures() : num_features;
    // class histogram -> constant, cached at upper level
    unsigned int class_ns = num_states[num_features];
    // num states of merged vectors
    unsigned int target_ns = num_states[target_idx];
    unsigned int max_mergemap_dim = max_state * target_ns;
    unsigned int max_merged_ns = (max_mergemap_dim < vector_length ? max_mergemap_dim : vector_length);
    unsigned long int max_joint_ns = max_merged_ns * class_ns;

    /* Setup memory pools */
    unsigned int *d_mergemap_pool;
    uint16_t *d_merged_histogram_pool, *d_joint_histogram_pool;
    error = cudaMalloc(&d_mergemap_pool, max_mergemap_dim * max_features * num_streams * sizeof(unsigned int));
    checkCudaError(error);
    error = cudaMalloc(&d_joint_histogram_pool, max_joint_ns * max_features * num_streams * sizeof(uint16_t));
    checkCudaError(error);
    error = cudaMalloc(&d_merged_histogram_pool, max_merged_ns * max_features * num_streams * sizeof(uint16_t));
    checkCudaError(error);

    unsigned int *batch_feature_idx_pool;
    uint16_t *data_ns_pool;
    // pointers to histograms in device memory
    unsigned int **dptr_mergemap_pool;
    uint16_t **dptr_merged_histogram_pool, **dptr_joint_histogram_pool;
    cudaMallocHost(&dptr_mergemap_pool, max_features * num_streams * sizeof(unsigned int *));
    cudaMallocHost(&dptr_merged_histogram_pool, max_features * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&dptr_joint_histogram_pool, max_features * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&batch_feature_idx_pool, max_features * num_streams * sizeof(unsigned int));
    cudaMallocHost(&data_ns_pool, max_features * num_streams * sizeof(uint16_t));

    if (profile) {
        times[1] += profiler.lapSeconds();
    }

    unsigned int feature_idx = 0;
    while (feature_idx < num_features) {
        if (profile && no_async) {
            profiler.start();
        }
        // Setup stream and pointers (to the memory zone of this stream)
        unsigned int stream_pos = stream_id % num_streams;
        cudaStream_t &stream = streams[stream_pos];
        ++stream_id;

        unsigned int *batch_feature_idx_v = &batch_feature_idx_pool[stream_pos * max_features];
        uint16_t *data_ns_v = &data_ns_pool[stream_pos * max_features];

        unsigned int **dptr_mergemap_v = &dptr_mergemap_pool[stream_pos * max_features];
        uint16_t **dptr_merged_histogram_v = &dptr_merged_histogram_pool[stream_pos * max_features];
        uint16_t **dptr_joint_histogram_v = &dptr_joint_histogram_pool[stream_pos * max_features];
        // first pointer -> start of mem zone
        unsigned int *d_mergemap_pool_start = &d_mergemap_pool[stream_pos * max_features * max_mergemap_dim];
        uint16_t *d_merged_histogram_pool_start = &d_merged_histogram_pool[stream_pos * max_features * max_merged_ns];
        uint16_t *d_joint_histogram_pool_start = &d_joint_histogram_pool[stream_pos * max_features * max_joint_ns];

        // Get the next *batch_features* features that have no MI computed

        // MANDATORY vars of previous iter could have not been copied to GPU yet
        cudaStreamSynchronize(stream);

        unsigned int f_count = 0;
        while ((f_count < max_features) && (feature_idx < num_features)) {
            if (!selected_features[feature_idx]) {
                batch_feature_idx_v[f_count] = feature_idx;
                data_ns_v[f_count] = num_states[feature_idx];

                ++f_count;
            }
            ++feature_idx;
        }
        if (!f_count) {
            continue;
        }

        /* Set global histogram offsets OR null to use shared memory
         * indexes = [0, 2, 3, 4]
         * ptr_histogram = [0x0, 0x10, nullptr, 0x20]
         *  feature 0 uses global histogram [0x0, ..., 0xf]
         *  feature 2 uses global histogram [0x10, ..., 0x1f]
         *  feature 3 uses shared mem histogram
         *  feature 4 uses global histogram [0x20, ..., 0x2f]
         * */
        size_t mergemap_offset = 0, merged_histogram_offset = 0, joint_histogram_offset = 0;
        bool use_shmem = config.useSharedMem();
        size_t sharedMem = 0;
        for (unsigned int f = 0; f < f_count; ++f) {
            unsigned int mergemap_dim = data_ns_v[f] * target_ns;
            unsigned int max_merged_ns_f = (mergemap_dim < vector_length ? mergemap_dim : vector_length);

            size_t mergemap_size = mergemap_dim * sizeof(unsigned int);
            size_t datahist_size = max_merged_ns_f * sizeof(uint16_t);
            size_t jointhist_size = max_merged_ns_f * class_ns * sizeof(uint16_t);

            size_t needed_shmem = 0;
            if (use_shmem && (needed_shmem + mergemap_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_mergemap_v[f] = nullptr;
                needed_shmem += mergemap_size;
            } else {
                dptr_mergemap_v[f] = d_mergemap_pool_start + mergemap_offset;
                mergemap_offset += mergemap_dim;
            }
            if (use_shmem && (needed_shmem + jointhist_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_joint_histogram_v[f] = nullptr;
                needed_shmem += jointhist_size;
            } else {
                dptr_joint_histogram_v[f] = d_joint_histogram_pool_start + joint_histogram_offset;
                joint_histogram_offset += max_merged_ns_f * class_ns;
            }
            if (use_shmem && (needed_shmem + datahist_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_merged_histogram_v[f] = nullptr;
                needed_shmem += datahist_size;
            } else {
                dptr_merged_histogram_v[f] = d_merged_histogram_pool_start + merged_histogram_offset;
                merged_histogram_offset += max_merged_ns_f;
            }

            // Find max needed shared memory
            sharedMem = (needed_shmem > sharedMem) ? needed_shmem : sharedMem;
        }

        if (profile && no_async) {
            times[5] += profiler.lapSeconds();
        }

        // Transfer data and reset memory
        cudaMemsetAsync(d_mergemap_pool_start, 0, mergemap_offset * sizeof(unsigned int), stream);
        cudaMemsetAsync(d_merged_histogram_pool_start, 0, merged_histogram_offset * sizeof(uint16_t), stream);
        cudaMemsetAsync(d_joint_histogram_pool_start, 0, joint_histogram_offset * sizeof(uint16_t), stream);

        if (profile && no_async) {
            cudaStreamSynchronize(stream);
            times[7] += profiler.lapSeconds();
        }

        /* Merge arrays for features in batch with the target and create histograms* */
        dim3 gridDim(f_count); // 1 block per feature
        k_merge_and_hist_and_cMI_batch<<<gridDim, blockDim, sharedMem, stream>>>(device_data, device_class_data,
                                                                                 target_idx, batch_feature_idx_v,
                                                                                 vector_length, dptr_mergemap_v,
                                                                                 dptr_merged_histogram_v,
                                                                                 data_ns_v, d_class_histogram,
                                                                                 class_ns, dptr_joint_histogram_v,
                                                                                 target_ns, d_mutual_information,
                                                                                 vector_length_inv);

        if (profile && no_async) {
            cudaStreamSynchronize(stream);
            times[8] += profiler.lapSeconds();
        }
    }

    if (profile) {
        times[2] += profiler.lapSeconds();
    }

    /* Copy results back */
    cudaDeviceSynchronize();
    checkCudaError(cudaGetLastError());
    cudaMemcpy(mutual_information, d_mutual_information, num_features * sizeof(R), cudaMemcpyDeviceToHost);

    if (profile) {
        times[3] += profiler.lapSeconds();
    }

    /* Free memory */
    for (cudaStream_t &s: streams) {
        cudaStreamDestroy(s);
    }

    cudaFreeHost(data_ns_pool);
    cudaFreeHost(batch_feature_idx_pool);
    cudaFreeHost(dptr_joint_histogram_pool);
    cudaFreeHost(dptr_merged_histogram_pool);
    cudaFreeHost(dptr_mergemap_pool);
    cudaFree(d_merged_histogram_pool);
    cudaFree(d_joint_histogram_pool);
    cudaFree(d_mergemap_pool);
    cudaFree(d_mutual_information);

    if (profile) {
        times[4] += profiler.lapSeconds();
        times[0] += Profiler::secondsFrom(start);
    }
}

template<typename D, typename R>
__host__ void
mergeAndCalcMutualInformationAndJointEntropyAll(D *device_data, unsigned int target_idx, unsigned int vector_length,
                                                unsigned int num_features, double *times, R *mutual_information,
                                                Config const &config, const bool *selected_features,
                                                unsigned int *num_states, unsigned int max_state, bool profile,
                                                unsigned int *d_class_histogram) {

    /***
     * Calc mutual information and joint entropy between the class and the result of merging feature *target_id* and all the other
    ***/

    const unsigned int blockSize = config.getBlockSize();
    dim3 blockDim(blockSize);
    const R vector_length_inv = (R) 1.0 / vector_length;
    cudaError_t error;
    D *device_class_data = &device_data[num_features * vector_length];

    Profiler profiler;
    Profiler::time_point start = profiler.start();

    /* Alloc memory for results */
    R *d_mutual_information;
    cudaMalloc(&d_mutual_information, num_features * sizeof(R));
    cudaMemset(d_mutual_information, 0, num_features * sizeof(R));

    /* Create streams */
    bool no_async = config.getNumStreams() == 0;
    unsigned int num_streams = (no_async) ? 1 : config.getNumStreams();
    unsigned int stream_id = 0;
    cudaStream_t streams[num_streams];
    for (cudaStream_t &s: streams) {
        cudaStreamCreate(&s);
    }

    /* Calculate batch sizes */
    unsigned int max_features = config.getBatchFeatures() < num_features ? config.getBatchFeatures() : num_features;
    // class histogram -> constant, cached at upper level
    unsigned int class_ns = num_states[num_features];
    // num states of merged vectors
    unsigned int target_ns = num_states[target_idx];
    unsigned int max_mergemap_dim = max_state * target_ns;
    unsigned int max_merged_ns = (max_mergemap_dim < vector_length ? max_mergemap_dim : vector_length);
    unsigned long int max_joint_ns = max_merged_ns * class_ns;

    /* Setup memory pools */
    unsigned int *d_mergemap_pool;
    uint16_t *d_merged_histogram_pool, *d_joint_histogram_pool;
    error = cudaMalloc(&d_mergemap_pool, max_mergemap_dim * max_features * num_streams * sizeof(unsigned int));
    checkCudaError(error);
    error = cudaMalloc(&d_joint_histogram_pool, max_joint_ns * max_features * num_streams * sizeof(uint16_t));
    checkCudaError(error);
    error = cudaMalloc(&d_merged_histogram_pool, max_merged_ns * max_features * num_streams * sizeof(uint16_t));
    checkCudaError(error);

    unsigned int *batch_feature_idx_pool;
    uint16_t *data_ns_pool;
    // pointers to histograms in device memory
    unsigned int **dptr_mergemap_pool;
    uint16_t **dptr_merged_histogram_pool, **dptr_joint_histogram_pool;
    cudaMallocHost(&dptr_mergemap_pool, max_features * num_streams * sizeof(unsigned int *));
    cudaMallocHost(&dptr_merged_histogram_pool, max_features * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&dptr_joint_histogram_pool, max_features * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&batch_feature_idx_pool, max_features * num_streams * sizeof(unsigned int));
    cudaMallocHost(&data_ns_pool, max_features * num_streams * sizeof(uint16_t));

    if (profile) {
        times[1] += profiler.lapSeconds();
    }

    unsigned int feature_idx = 0;
    while (feature_idx < num_features) {
        if (profile && no_async) {
            profiler.start();
        }
        // Setup stream and pointers (to the memory zone of this stream)
        unsigned int stream_pos = stream_id % num_streams;
        cudaStream_t &stream = streams[stream_pos];
        ++stream_id;

        unsigned int *batch_feature_idx_v = &batch_feature_idx_pool[stream_pos * max_features];
        uint16_t *data_ns_v = &data_ns_pool[stream_pos * max_features];

        unsigned int **dptr_mergemap_v = &dptr_mergemap_pool[stream_pos * max_features];
        uint16_t **dptr_merged_histogram_v = &dptr_merged_histogram_pool[stream_pos * max_features];
        uint16_t **dptr_joint_histogram_v = &dptr_joint_histogram_pool[stream_pos * max_features];
        // first pointer -> start of mem zone
        unsigned int *d_mergemap_pool_start = &d_mergemap_pool[stream_pos * max_features * max_mergemap_dim];
        uint16_t *d_merged_histogram_pool_start = &d_merged_histogram_pool[stream_pos * max_features * max_merged_ns];
        uint16_t *d_joint_histogram_pool_start = &d_joint_histogram_pool[stream_pos * max_features * max_joint_ns];

        // Get the next *batch_features* features that have no MI computed

        // MANDATORY vars of previous iter could have not been copied to GPU yet
        cudaStreamSynchronize(stream);

        unsigned int f_count = 0;
        while ((f_count < max_features) && (feature_idx < num_features)) {
            if (!selected_features[feature_idx]) {
                batch_feature_idx_v[f_count] = feature_idx;
                data_ns_v[f_count] = num_states[feature_idx];

                ++f_count;
            }
            ++feature_idx;
        }
        if (!f_count) {
            continue;
        }

        /* Set global histogram offsets OR null to use shared memory
         * indexes = [0, 2, 3, 4]
         * ptr_histogram = [0x0, 0x10, nullptr, 0x20]
         *  feature 0 uses global histogram [0x0, ..., 0xf]
         *  feature 2 uses global histogram [0x10, ..., 0x1f]
         *  feature 3 uses shared mem histogram
         *  feature 4 uses global histogram [0x20, ..., 0x2f]
         * */
        size_t mergemap_offset = 0, merged_histogram_offset = 0, joint_histogram_offset = 0;
        bool use_shmem = config.useSharedMem();
        size_t sharedMem = 0;
        for (unsigned int f = 0; f < f_count; ++f) {
            unsigned int mergemap_dim = data_ns_v[f] * target_ns;
            unsigned int max_merged_ns_f = (mergemap_dim < vector_length ? mergemap_dim : vector_length);

            size_t mergemap_size = mergemap_dim * sizeof(unsigned int);
            size_t datahist_size = max_merged_ns_f * sizeof(uint16_t);
            size_t jointhist_size = max_merged_ns_f * class_ns * sizeof(uint16_t);

            size_t needed_shmem = 0;
            if (use_shmem && (needed_shmem + mergemap_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_mergemap_v[f] = nullptr;
                needed_shmem += mergemap_size;
            } else {
                dptr_mergemap_v[f] = d_mergemap_pool_start + mergemap_offset;
                mergemap_offset += mergemap_dim;
            }
            if (use_shmem && (needed_shmem + jointhist_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_joint_histogram_v[f] = nullptr;
                needed_shmem += jointhist_size;
            } else {
                dptr_joint_histogram_v[f] = d_joint_histogram_pool_start + joint_histogram_offset;
                joint_histogram_offset += max_merged_ns_f * class_ns;
            }
            if (use_shmem && (needed_shmem + datahist_size < MAX_SHARED_MEMORY_PER_BLOCK)) {
                dptr_merged_histogram_v[f] = nullptr;
                needed_shmem += datahist_size;
            } else {
                dptr_merged_histogram_v[f] = d_merged_histogram_pool_start + merged_histogram_offset;
                merged_histogram_offset += max_merged_ns_f;
            }

            // Find max needed shared memory
            sharedMem = (needed_shmem > sharedMem) ? needed_shmem : sharedMem;
        }

        if (profile && no_async) {
            times[5] += profiler.lapSeconds();
        }

        // Transfer data and reset memory
        cudaMemsetAsync(d_mergemap_pool_start, 0, mergemap_offset * sizeof(unsigned int), stream);
        cudaMemsetAsync(d_merged_histogram_pool_start, 0, merged_histogram_offset * sizeof(uint16_t), stream);
        cudaMemsetAsync(d_joint_histogram_pool_start, 0, joint_histogram_offset * sizeof(uint16_t), stream);

        if (profile && no_async) {
            cudaStreamSynchronize(stream);
            times[7] += profiler.lapSeconds();
        }

        /* Merge arrays for features in batch with the target and create histograms* */
        dim3 gridDim(f_count); // 1 block per feature
        k_merge_and_hist_and_cMIJE_batch<<<gridDim, blockDim, sharedMem, stream>>>(device_data, device_class_data,
                                                                                   target_idx, batch_feature_idx_v,
                                                                                   vector_length, dptr_mergemap_v,
                                                                                   dptr_merged_histogram_v,
                                                                                   data_ns_v, d_class_histogram,
                                                                                   class_ns, dptr_joint_histogram_v,
                                                                                   target_ns, d_mutual_information,
                                                                                   vector_length_inv);

        if (profile && no_async) {
            cudaStreamSynchronize(stream);
            times[8] += profiler.lapSeconds();
        }
    }

    if (profile) {
        times[2] += profiler.lapSeconds();
    }

    /* Copy results back */
    cudaDeviceSynchronize();
    checkCudaError(cudaGetLastError());
    cudaMemcpy(mutual_information, d_mutual_information, num_features * sizeof(R), cudaMemcpyDeviceToHost);

    if (profile) {
        times[3] += profiler.lapSeconds();
    }

    /* Free memory */
    for (cudaStream_t &s: streams) {
        cudaStreamDestroy(s);
    }

    cudaFreeHost(data_ns_pool);
    cudaFreeHost(batch_feature_idx_pool);
    cudaFreeHost(dptr_joint_histogram_pool);
    cudaFreeHost(dptr_merged_histogram_pool);
    cudaFreeHost(dptr_mergemap_pool);
    cudaFree(d_merged_histogram_pool);
    cudaFree(d_joint_histogram_pool);
    cudaFree(d_mergemap_pool);
    cudaFree(d_mutual_information);

    if (profile) {
        times[4] += profiler.lapSeconds();
        times[0] += Profiler::secondsFrom(start);
    }
}

#endif //CUFEAST_MITOOLBOX_H
