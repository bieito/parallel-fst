#ifndef CUFEAST_LUT_H
#define CUFEAST_LUT_H

#include <cmath>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <string>
#include <stdexcept>
#include <bitset>


template<typename F = int32_t>
class LUT {
public:
    enum Rounding {
        ROUND, TRUNC, FLOOR, CEIL
    };

private:
    double compute(unsigned int i, unsigned int j) {
        return round_fun(log2((double) i / j) * (1 << frac_bits));
    }

public:
    static double toDouble(int fixed, unsigned int frac_bits) {
        double result = (double) fixed / (1 << frac_bits);

        return -result;// div 2 correction
    }

    LUT(unsigned int numSamples, Rounding rounding = ROUND) {
        const unsigned int F_bits = sizeof(F) * 8;

        num_samples = numSamples;
        switch (rounding) {
            case TRUNC:
                round_fun = trunc;
                break;
            case FLOOR:
                round_fun = floor;
                break;
            case CEIL:
                round_fun = ceil;
                break;
            default:
                round_fun = round;
        }

        //frac_bits = floor(log2(((1 << (F_bits)) - 1) / log2(numSamples)));
        frac_bits = F_bits;
        while (std::ceil(std::log2(-compute(1, numSamples))) > F_bits) {
            frac_bits -= 1;
        }
        if (frac_bits <= 0) {
            throw std::invalid_argument("Datatype has not enough precision");
        }

        //this->bi = F_bits - frac_bits;

        // num_samples will be halved if surpasses MAX_FIXED, so the second dimension of the table will be log_2(num_samples)-log_2(MAX_FIXED)
        /*this->MAX_FIXED = ((long unsigned int) 1 << F_bits);
        this->num_rows = 1;
        this->factor = 0;

        //std::cout<< this->num_samples << " " << this->factor << " " << this->MAX_FIXED << " " <<std::endl;
        while ((this->num_samples >> this->factor) >= this->MAX_FIXED) {
            //std::cout << (this->num_samples >> this->num_rows) << std::endl;
            this->factor += 1;
            //this->num_rows += 1;
        }
        this->num_samples >>= this->factor;

        double max_value = -this->compute(1, this->num_samples); // using double to avoid (unsigned) int overflow
        unsigned int needed_bits = (unsigned int) ceil(log2(max_value)) + 1;
        if (needed_bits > F_bits) {
            throw std::invalid_argument(
                    "Invalid configuration for LUT and given dataset");//Selected type for LUT has " + std::to_string(F_bits) + " bits with " + std::to_string(this->frac_bits) + " for the fractional part, but at least " + std::to_string(needed_bits) + " are needed");
        }*/
        /*bool stop = false;
        while (!stop) {
            double max_value = -this->compute(1, this->num_samples); // using double to avoid (unsigned) int overflow
            unsigned int needed_bits = (unsigned int) ceil(log2(max_value)) + 1;
            if (needed_bits <= F_bits) {
                stop = true;
            } else {
                this->factor += 1;
                this->num_samples >>= 1;
            }
        }*/

        //std::cout << F_bits << " bits for fixed type (" << (needed_bits - this->frac_bits) << " + " << this->frac_bits << ")" << std::endl;
        data = new F[num_samples];

        std::cout << "LUT: " << num_samples << " elements of " << F_bits << " bits (fixed point datatype) with "
                  << frac_bits << " bits for the fractional part"
                  << std::endl; // - algorithm iterations: " << this->factor << std::endl;

        // LUT[i,j] = round(log_2(i/j)/q) * q
        //unsigned int inv_q = 1 << this->frac_bits; // q = 2 ** -frac_bits
        //for (unsigned int j = 0; j < this->num_rows; ++j) {
        for (unsigned int i = 1; i <= num_samples; ++i) {
            data[i - 1] = -compute(i, num_samples);
        }
        //}

    }

    ~LUT() {
        delete[] data;
    }

    F get(unsigned int i) const {
        if (i == 0) {
            return data[0];
        }
        return data[i - 1];
    }

    double toDouble(int fixed) const {
        return toDouble(fixed, frac_bits);
    }

    void print() const {
        for (int i = 0; i < num_samples; ++i) {
            std::cout << std::setprecision(2) << toDouble(data[i]) << " ";
        }
        std::cout << std::endl;
    }

    unsigned int getNumSamples() const {
        return num_samples;
    }

    size_t getSize() const {
        return num_samples * sizeof(F);
    }

    F *getRawData() const {
        return this->data;
    }

    unsigned int getBF() const {
        return frac_bits;
    }

private:
    //unsigned int bi;  // number of bits for integer part
    unsigned int frac_bits;  // number of bits for fractional part
    //long unsigned int MAX_FIXED;
    unsigned int num_samples;
    //unsigned int num_rows; // second dimension of LUT (number of rows)
    //unsigned int factor;

    double (*round_fun)(double) = round; // rounding strategy

    F *data;
};


#endif //CUFEAST_LUT_H
