#include "CliDataset.cuh"

CliDataset::~CliDataset() {
    cudaFreeHost(raw_data);
    raw_data = nullptr;
}

void CliDataset::allocate() {
    if (raw_data != nullptr) {
        throw std::runtime_error("Error: Dataset: Memory already allocated");
    }
    cudaMallocHost(&raw_data, numElements() * sizeof(unsigned int));
}

CliDataset::CliDataset(const std::string &file, unsigned int disc_bins) {

    FileParser *file_parser = FileParser::getFileParser(file);
    file_parser->readInput(*this, disc_bins);

    classes = &raw_data[num_feat * num_samp];

    delete file_parser;
}

CliDataset::operator std::string() const {
    std::stringstream sstr;

    // get human readable size
    std::string hr_size;
    {
        double total = numBytes();
        std::string prefix[] = {"", "ki", "Mi", "Gi", "Ti", "Pi"};
        unsigned int i = 0;
        while (total >= 1024 && i <= 5) {
            total /= 1024;
            ++i;
        }

        std::stringstream priv_sstr;
        priv_sstr << std::fixed << std::setprecision(3) << total << " " << prefix[i];
        hr_size = priv_sstr.str();
    }
    sstr << this->num_samp << " samples | " << this->num_feat << " features | "
         << this->num_clas << " classes (" << hr_size << "B)" << std::endl;

    /*if (numElements() < 16) {
        for (unsigned int i = 0; i < this->num_feat + this->num_clas; ++i) {

            if (i == this->num_feat) {
                sstr << std::endl;
            }

            sstr << " ";

            for (unsigned int j = 0; j < this->num_samp; ++j) {
                sstr << this->raw_data[i * this->num_samp + j];
                if (j != this->num_samp - 1) {
                    sstr << " ";
                } else {
                    sstr << std::endl;
                }
            }

        }
    }*/

    return sstr.str();
}

std::ostream &operator<<(std::ostream &strm, CliDataset const &obj) {
    return strm << (std::string) obj;
}