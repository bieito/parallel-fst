#include <cmath>
#include <iostream>
#include <thread>

#include <ArgParser/ArgParser.h>
#include <Profiler/Profiler.h>

#include <cuFEAST/FSAlgorithm.h>

namespace FEAST {
    extern "C" {

#include <FEAST/FSAlgorithms.h>

    }
}

#include "CudaWrappers/CudaWrappers.cuh"
#include "CLI/CliDataset.cuh"
#include "CLI/CliResults.h"
#include "CLI/CliConfig.h"

#include "ProjectConfig.h"

void setArgParserOptions(ArgParser &options);

bool checkAndConvertOptions(ArgParser &options, CliConfig &config);

void writeResults(Results const &results, std::string const &path);

void progressBarTh(const Results &results, const CliConfig &config);

void spinnerTh(const Results &results, const CliConfig &config);

void printValidationTable(const Results &feast_results, const Results &gpu_results, const CliConfig &config);

void runGPUBenchmark(cuFEAST::FSAlgorithm<unsigned int> &algorithm, CliDataset **dataset, ArgParser &arg_parser,
                     CliConfig &config);

void runCPUBenchmark(CliDataset &dataset, ArgParser &arg_parser, CliConfig &config, bool is_condmi, bool is_betagamma,
                     const std::pair<double, double> &betagamma,
                     uint *(*feast_alg)(uint, uint, uint, uint **, uint *, uint *, double *),
                     unsigned int **feature_matrix, unsigned int *u_result_indexes, int *result_indexes,
                     double *result_scores);

void
getFEASTAlgorithm(const std::string &method, bool *is_condmi, bool *is_betagamma, std::pair<double, double> *betagamma,
                  uint *(**feast_alg)(uint, uint, uint, uint **, uint *, uint *, double *));


int main(int argc, char *argv[]) {

    /* 1 Parse configuration */
    ArgParser arg_parser("parallel-fst --cuda",
                         "Run CUDA-accelerated versions of Feature Selection methods");
    setArgParserOptions(arg_parser);
    arg_parser.parse(argc - 1, &argv[1]);
    CliConfig config;
    if (!checkAndConvertOptions(arg_parser, config)) {
        // Graceful exit
        return 0;
    }

    /* 2 Load data */
    Profiler profiler;
    profiler.start();
    auto dataset = new CliDataset(config.getDatasetPath(), config.getDiscBins());
    if (config.doProfile()) {
        std::cout << "Profile: " << std::setprecision(6) << std::fixed << profiler.stopSeconds() << "s  Load dataset"
                  << std::endl;
    }
    if (config.getK() > dataset->num_feat) {
        std::cerr
                << "WARNING: Tried to select '" + std::to_string(config.getK()) + "' features, but dataset has only '" +
                   std::to_string(dataset->num_feat) + "'\n           -> using k=" + std::to_string(dataset->num_feat)
                << std::endl;
        config.setK(dataset->num_feat);
    }
    if (config.getVerboseLevel() == CliConfig::FULL) {
        std::cout << "Dataset: " << *dataset;
    }

    /* 3 Run algorithm */
    cuFEAST::FSAlgorithm<unsigned int> &algorithm = config.getAlgorithm();
    Results gpu_results, feast_results;
    double feast_time = 1, gpu_time = 1;
    std::thread ui_th;
    // CUDA
    if (config.getImplementation() == CliConfig::CUDA) {
        if (config.getVerboseLevel() == CliConfig::FULL) {
            char dev_name[256];
            getCudaDefaultDeviceInfo(dev_name);
            std::cout << "Device: " << dev_name << std::endl;
        }

        if (config.doBenchmark()) {
            runGPUBenchmark(algorithm, &dataset, arg_parser, config);
        } else {
            if (config.doProfile()) {
                // Dont show spinner if profiling, to avoid messing up stdout
                profiler.start();
            } else {
                // This is only a thread to update an UI indicator while the master computes the algorithm
                ui_th = std::thread(progressBarTh, std::ref(gpu_results), std::ref(config));
            }

            try {
                algorithm.run(*dataset, gpu_results, config.getAlgorithmConfig());
            } catch (std::runtime_error &e) {
                std::cerr << "ERROR: " << e.what() << std::endl;

                delete dataset;
                return -1;
            }

            if (config.doProfile()) {
                gpu_time = profiler.stopSeconds();
                if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
                    std::cout << "Profile:\n";
                    for (const auto &[ms, what]: config.getAlgorithmConfig().getProfileData()) {
                        std::cout << "         " << std::setprecision(6) << std::fixed << ms << "s  " << what << "\n";
                    }
                    std::cout << "         " << gpu_time << "s" << std::endl;
                } else {
                    std::cout << "Profile: " << gpu_time << "s  GPU execution" << std::endl;
                }
            } else {
                ui_th.join();  // wait for ui thread to end before writing to stdout
            }
        }

    }

    // CPU
    // FEAST or VALIDATE and (CUDA or FEAST)
    if (config.getImplementation() == CliConfig::FEAST || config.doValidate()) {
        if (config.getVerboseLevel() == CliConfig::FULL) {
            std::cout << "Device: CPU" << std::endl;
            std::cout << "Implementation: FEAST" << std::endl;
        }

        // FEAST needs bi-dimensional data
        unsigned int *feature_matrix[dataset->num_feat];
        dataset->get2DRawData(feature_matrix);
        int result_indexes[config.getK()];
        unsigned int u_result_indexes[config.getK()];
        double result_scores[config.getK()];

        // Find algorithm
        bool is_betagamma = false;
        bool is_condmi = false;
        std::pair<double, double> betagamma;
        FEAST::uint *(*feast_alg)(uint, uint, uint, uint **, uint *, uint *, double *) = nullptr;
        getFEASTAlgorithm(arg_parser.getOptionArg("algorithm"), &is_condmi, &is_betagamma, &betagamma, &feast_alg);

        if (config.doBenchmark()) {
            runCPUBenchmark(*dataset, arg_parser, config, is_condmi, is_betagamma, betagamma, feast_alg, feature_matrix,
                            u_result_indexes, result_indexes, result_scores);
        } else {
            // Start profiling OR UI thread
            if (config.doProfile()) {
                profiler.start();
            } else {
                ui_th = std::thread(spinnerTh, std::ref(feast_results), std::ref(config));
            }

            // Run algorithm
            if (is_betagamma) {
                FEAST::BetaGamma(config.getK(), dataset->num_samp, dataset->num_feat, feature_matrix, dataset->classes,
                                 u_result_indexes, result_scores, betagamma.first, betagamma.second);
            } else if (is_condmi) {
                FEAST::CondMI(config.getK(), dataset->num_samp, dataset->num_feat, feature_matrix, dataset->classes,
                              result_indexes, result_scores);
            } else {
                feast_alg(config.getK(), dataset->num_samp, dataset->num_feat, feature_matrix, dataset->classes,
                          u_result_indexes, result_scores);
            }

            // Stop profiling
            if (config.doProfile()) {
                feast_time = profiler.stopSeconds();
            }

            // Copy FEAST results to vector
            for (unsigned int i = 0; i < config.getK(); ++i) {
                if (is_condmi) {
                    feast_results.emplace_back(result_indexes[i], result_scores[i]);
                } else {
                    feast_results.emplace_back((int) u_result_indexes[i], result_scores[i]);
                }
            }

            if (config.doProfile()) {
                std::cout << "Profile: " << feast_time << "s  CPU (FEAST) execution" << std::endl;
                if (config.doValidate()) {
                    std::cout << "Profile: x" << feast_time / gpu_time << "  speedup" << std::endl;
                }
            } else {
                ui_th.join();
            }
        }

        if (config.getImplementation() != CliConfig::FEAST) {
            unsigned int num_errors = 0;
            unsigned int num_hard_errors = 0;
            double dev = 0;

            for (unsigned int i = 0; i < config.getK(); ++i) {
                if (feast_results[i].first != gpu_results[i].first) {
                    num_errors += 1;
                }

                // Find if gpu[i] is in feast[] but in other place
                unsigned int j = 0;
                while (j < config.getK() && feast_results[j].first != gpu_results[i].first) {
                    ++j;
                }
                if (j == config.getK()) {
                    num_hard_errors += 1;
                }

                dev += std::abs(feast_results[i].second - gpu_results[i].second);
            }

            std::cout << "Validation: " << std::endl <<
                      "  " << (num_errors == 0 ? "no" : std::to_string(num_errors)) << " errors (with order) out of "
                      << config.getK() << " results - total deviation " << std::scientific << dev << std::endl <<
                      "  " << (num_hard_errors == 0 ? "no" : std::to_string(num_hard_errors))
                      << " errors (without order)"
                      << std::endl;

            if (config.getVerboseLevel() == CliConfig::FULL) {
                printValidationTable(feast_results, gpu_results, config);
            }
        }
    }

    /* 4 Output results */
    writeResults((config.getImplementation() == CliConfig::FEAST ? feast_results : gpu_results),
                 config.getResultsPath());

    delete dataset;

}

void writeResults(Results const &results, std::string const &path) {

    if (path == "-") { // stdout
        std::cout << "Results: " << results.size() << " feature" << (results.size() != 1 ? "s" : "") << std::endl;
        std::cout << results;
    } else {
        std::ofstream file(path);

        if (file.is_open()) {
            file << results;
            file.flush();
        } else {
            throw std::invalid_argument("Error opening output file");
        }
    }
}

bool checkAndConvertOptions(ArgParser &options, CliConfig &config) {

    if (options.noArgs()) {
        std::cout << options.helpStr() << std::endl;
        return false;
    }

    /* First, check flags that invalidate execution */
    if (options.isFlagPresent("help")) {
        std::cout << options.helpStr() << std::endl;
        return false;
    }

    if (options.isFlagPresent("print_version")) {
        std::stringstream sstr;
        sstr << "cuFEAST-cli " << BUILD_VERSION << " (" << BUILD_TAG;
        if (!std::string(BUILD_TYPE).empty()) {
            sstr << "[" << BUILD_TYPE << "]";
        }
        sstr << " " << BUILD_TIME << ")" << std::endl;
        sstr << "[" << BUILD_CXX << ", " << BUILD_CUDA << "] on " << BUILD_PLATFORM << std::endl;
        std::cout << sstr.str();
        return false;
    }

    if (options.isFlagPresent("list_gpu")) {
        listCudaDevices();
        return false;
    }

    config.setImplementation(options.getOptionArgOrDefault("implementation"));

    config.setPrecision(options.getOptionArgOrDefault("precision"));

    if (config.getImplementation() == CliConfig::CUDA) {
        if (getCudaNumDevices() == 0) {
            throw std::runtime_error("No GPUs available, program cannot be run");
        } else {
            /* Now, check CUDA options */
            config.setBlockSize(std::stoul(options.getOptionArgOrDefault("blk")));
            config.setNumStreams(std::stoul(options.getOptionArgOrDefault("streams")));
            config.setBatchFeatures(std::stoul(options.getOptionArgOrDefault("batch_size")));

            int gpu_idx = std::stoi(options.getOptionArgOrDefault("gpu_index"));
            if (gpu_idx < 0 || gpu_idx > (getCudaNumDevices() - 1)) {
                throw std::invalid_argument("GPU index must be one of the listed with --list-gpus");
            }
            config.setGpuIndex(gpu_idx);

            config.setSharedMem(!options.isFlagPresent("no_shared_mem"));
        }
    }

    config.setProfile(options.isFlagPresent("profile"));

    /* Check other options for algorithm */
    if (options.isOptionPresent("input")) {
        config.setInputPath(options.getOptionArg("input"));
    } else {
        throw std::invalid_argument("Mandatory argument 'input path' not provided");
    }

    if (options.isOptionPresent("output")) {
        config.setOutputPath(options.getOptionArg("output"));
    } else {
        config.setOutputPath("-");
    }

    unsigned int k;
    if (options.isOptionPresent("select")) {
        k = std::stoul(options.getOptionArg("select"));
    } else {
        throw std::invalid_argument("Mandatory argument 'number of features to select' not provided");
    }

    if (options.isOptionPresent("algorithm")) {
        config.setAlgorithm(options.getOptionArg("algorithm"), k);
    } else {
        throw std::invalid_argument("Mandatory argument 'algorithm' not provided");
    }

    if (options.isOptionPresent("disc_bins")) {
        config.setDiscBins(std::stoul(options.getOptionArg("disc_bins")));
    } else {
        config.setDiscBins(0);
    }

    /* Check last configurations */
    config.setVerboseLevel(std::stoul(options.getOptionArgOrDefault("verbose")));

    config.setDoValidate(options.isFlagPresent("test"));

    config.setBenchmark(options.isFlagPresent("benchmark"));

    if (config.doBenchmark() && config.doValidate()) {
        config.setDoValidate(false);
        std::cerr << "WARNING: Validation is not compatible with benchmarks\n"
                     "           -> Validation disabled" << std::endl;
    }

    if (config.doBenchmark() && !config.doProfile()) {
        config.setProfile(true);
        std::cerr << "WARNING: Benchmarks require profiling\n"
                     "           -> Profiling enabled" << std::endl;
    }

    if (config.getImplementation() == CliConfig::FEAST && config.doValidate()) {
        config.setDoValidate(false);
        std::cerr << "WARNING: Validation is useless when running the original FEAST implementation\n"
                     "           -> Validation disabled" << std::endl;
    }

    if (config.getImplementation() == CliConfig::FEAST &&
        config.getAlgorithmConfig().getPrecision() != Config::DOUBLE) {
        config.setPrecision("double");
        std::cerr << "WARNING: Original FEAST implementation is only compatible with double precision data types\n"
                     "           -> Switched to double precision" << std::endl;
    }

    /*if (config.doValidate() && config.getDataType() == CliConfig::) {
        config.setDoValidate(false);
        std::cerr << "WARNING: CPU algorithm is not compatible with half precision datatypes\n"
                     "           -> Validation disabled" << std::endl;
    }*/

    return true;
}

void setArgParserOptions(ArgParser &options) {
    options.addOptionArg("algorithm", {'a'}, {"algorithm"}, "ALG", true,
                         {},
                         "algorithm to run - ALG = (MIM|CondMI|BetaGammma[:<float>:<float>]|JMI|mRMR|CMIM|ICAP|DISR)\n"
                         "MIM     Mutual Information Maximisation\n"
                         //                    "CondMI  Conditional Mutual Information              [2012 - G. Brown, A. Pocock, M.-J. Zhao, M. Luján]\n"
                         "JMI     Joint Mutual Information                    [1999 - H. H. Yang, J. Moody]\n"
                         "mRMR    Minimum-Redundancy Max-Relevance            [2005 - H. Peng, F. Long, C. Ding]\n"
                         /*                    "CMIM    Conditional Mutual Information Maximisation [2004 - F. Fleuret]\n"
                                             "ICAP    Interaction Capping                         [2005 - A. Jakulin]\n"*/
                         "DISR    Double Input Symmetrical Relevance          [2006 - P. E. Meyer, G. Bontempi]\n"
            /*                    "BetaGamma[:<b>:<g>]  BetaGamma Space\n"
                                "    values for Beta <b> (weight of redundancy) and Gamma <g> (weight of conditional redundancy) can be provided\n"
                                "    some well-known configurations are:\n"
                                "        beta    | gamma | method\n"
                                "    ------------+-------+--------\n"
                                "         0.0    |  0.0  |  MIM\n"
                                "     (0.0, 1.0] |  0.0  |  MIFS  Mutual Information Feature Selection  [1994 - R. Battiti]\n"
                                "         1.0    |  1.0  |  CIFE  Conditional Infomax Feature Selection [2006 - D. Lin, X. Tang]"*/
    );

    options.addOptionArg("input", {'i'}, {"input"}, "path", true,
                         {}, "input file to read from\nallowed formats are arff, csv and libsvm");
    options.addOptionArg("output", {'o'}, {"output"}, "path", false,
                         {}, "output file to write results to\n"
                             "if not specified, write to stdout");
    options.addOptionArg("select", {'k'}, {"select"}, "int", true,
                         {}, "number of features to select");
    options.addOptionArg("disc_bins", {'d'}, {"disc"}, "n_bins", false,
                         {},
                         "discretize the input dataset with the binning technique using the specified number of bins");

    options.addOptionArg("implementation", {'r'}, {"implementation"}, "cuda"/*|cpu*/"|feast", false,
                         "cuda", "which implementation of FEAST to use\n"
                                 "cuda   CUDA version for NVIDIA GPUs - can be tuned with block-size, streams, batch, no-shmem, precision\n"
                                 //"cpu    custom CPU version derived from FEAST implementation, but allows for further configuration (sequential)\n"
                                 "feast  original CPU version of algorithm using the FEAST library (sequential)");

    options.addOptionArg("precision", {'p'}, {"precision"}, "double|single|fixed", false,
                         {"double"}, "precision of the datatype used for the computation of scores:\n"
                                     "double        floating-point double precision (" +
                                     std::to_string(sizeof(double)) +
                                     " B), as in the original FEAST implementation\n"
                                     "single        floating-point single precision (" +
                                     std::to_string(sizeof(float)) +
                                     " B), quicker execution but lower precision in results\n"
                                     "fixed         fixed-point half precision (2 B), from [2020 - L. Morán-Fernández, K. Sechidis, V. Bolón-Canedo, A. Alonso-Betanzos, G. Brown]");

    options.addOptionArg("blk", {'b'}, {"block-size", "tpb"}, "int", false,
                         {"512"}, "number of threads per block for CUDA kernels\n"
                                  "when shared memory usage is possible, a block of 1024 threads works best");
    options.addOptionArg("streams", {'s'}, {"streams"}, "int", false,
                         {"2"}, "number of streams to use to overlap memory transfers and kernels\n"
                                "recommended 2 streams for Tesla T4 or GTX 1650\n"
                                "use 0 (zero) to disable asynchronism and serialize operations");
    options.addOptionArg("batch_size", {}, {"batch"}, "int", false,
                         {"512"}, "approximate number of features to send in each batch of each stream\n"
                                  "higher values increase memory consumption, but lower values introduce higher overhead");
    //"works best when proportionally inverse to number of streams following the formula: batch_features = (4096 / (disc_bins/128) / num_streams^2)");
    options.addFlagArg("no_shared_mem", {}, {"no-shmem"},
                       "disable usage of shared memory (when possible) in GPU\n"
                       "this option has only interest for testing since it degrades performance");
    /*options.addOptionArg("data_type", {'t'}, {"type"}, "single|half",
                         {"single"}, "(NOT YET SUPPORTED) precision of datatype to use for dataset:\n"
                                     "single  C++ default 'unsigned int'\n"
                                     "half    half precision 'uint16_t'");*/

    options.addFlagArg("list_gpu", {}, {"list-gpu", "devices"},
                       "list CUDA enabled available GPUs");
    // TODO merge gpu and gpus options
    options.addOptionArg("gpu_index", {}, {"gpu"}, "int", false,
                         {std::to_string(getCudaDefaultDevice())},
            /*"(Will be deprecated in favour of --gpus) "*/"select index of GPU to use - default is system default");
    /*options.addOptionArg("gpu_indexes", {}, {"gpus"}, "all | int[,int[..]]",
                         {"all"},
                         "(NOT YET SUPPORTED) select indexes of GPUs to use for multi-gpu kernels");*/

    options.addFlagArg("test", {}, {"test", "validate"},
                       "in addition to other execution, run original FEAST implementation and validate results");
    options.addFlagArg("profile", {}, {"profile"},
                       "measure times for any active execution");
    options.addFlagArg("benchmark", {}, {"benchmark"},
                       "run 500 distinct combinations of parameters and measure times - "
                       "each configuration is repeated 5 times, which makes a total of 2500 runs\n"
                       " precision [double, fixed]\n"
                       "shared-mem [disabled, enabled]\n"
                       "   streams [0, 1, 2, 4, 8]\n"
                       "     batch [64, 256, 512, 1024, 4096]\n"
                       "block-size [64, 128, 256, 512, 1024]");

    options.addOptionArg("verbose", {'v'}, {"verbose"}, "0|1|2", false,
                         {"0"}, "verbosity level\n"
                                "0 - error and warning (to stderr) and explicitly wanted info as validation, profiling, etc (to stdout)\n"
                                "1 - minimal\n"
                                "2 - extra info, as dataset dimensions or device characteristics");
    options.addFlagArg("print_version",
                       {'V'}, {"version"},
                       "show the version of the program and other build info");
    options.addFlagArg("help",
                       {'h'}, {"help", "usage"},
                       "show this help and exit");
}


void progressBarTh(const Results &results, const CliConfig &config) {
    if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
        const unsigned int width = 40;
        int current = -1;
        auto start_time = std::chrono::high_resolution_clock::now();

        while (current < (int) config.getK()) {
            std::stringstream sstr;
            if ((int) results.size() > current) {
                current = results.size();
            }
            sstr << "\rProgress: [";
            for (unsigned int i = 0; i < width; ++i) {
                sstr << ((i < (current * width) / config.getK()) ? "#" : " ");
            }
            auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(
                    std::chrono::high_resolution_clock::now() - start_time);
            sstr << "] (" << current << "/" << config.getK() << ") " << (current * 100) / config.getK() << "%" << " "
                 << elapsed.count() << "s";
            std::cout << sstr.str() << std::flush;
            std::this_thread::sleep_for(std::chrono::milliseconds(100)); // update 10 times/sec
        }
        std::cout << std::endl;
    }
}

std::string drawSpinner(unsigned int pos, unsigned int width, unsigned int barsize) {
    std::stringstream sstr;
    sstr << "\rProgress: [";
    for (unsigned int i = 0; i < width; ++i) {
        if ((i < pos) || (i > (pos + barsize))) {
            sstr << " ";
        } else {
            sstr << "#";
        }
    }
    sstr << "]";
    return sstr.str();
}

void spinnerTh(const Results &results, const CliConfig &config) {
    if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
        const unsigned int width = 40;
        unsigned int barsize = width / 4;
        int current = 0;
        int direction = -1;
        auto start_time = std::chrono::high_resolution_clock::now();

        while (results.size() < config.getK()) {
            auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(
                    std::chrono::high_resolution_clock::now() - start_time);
            std::cout << drawSpinner(current, width, barsize) << " " << elapsed.count() << "s"
                      << std::flush;
            if (current < 1 || current > (int) (width - barsize - 2)) {
                direction *= -1;
            }
            current += direction;
            std::this_thread::sleep_for(std::chrono::milliseconds(100)); // update 10 times/sec
        }

        auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::high_resolution_clock::now() - start_time);
        std::stringstream sstr;
        sstr << drawSpinner(0, width, width) << " (" << config.getK() << "/" << config.getK() << ") 100% "
             << elapsed.count() << "s";
        std::cout << sstr.str() << std::flush;

        std::cout << std::endl;
    }
}

void printValidationTable(const Results &feast_results, const Results &gpu_results, const CliConfig &config) {
    std::stringstream sstr;

    int lens[6] = {std::max(4, (int) log10(config.getK() - 1) + 1), 9, 3, std::max(11, 9), std::max(5, 9),
                   std::max(6, 9)};
    for (unsigned int i = 0; i < config.getK(); ++i) {
        lens[1] = std::max(lens[1], (int) log10(feast_results[i].first) + 1);
        lens[2] = std::max(lens[2], (int) log10(gpu_results[i].first) + 1);
    }

    sstr <<
         " | " << std::setw(lens[0]) << "rank" <<
         " | " << std::setw(lens[1]) << "FEAST idx" <<
         " | " << std::setw(lens[2]) << "idx" <<
         " | " << std::setw(lens[3]) << "FEAST score" <<
         " | " << std::setw(lens[4]) << "score  " <<
         " | " << std::setw(lens[5]) << "|diff| " <<
         " | " << std::endl;

    sstr << " ";
    for (int len: lens) {
        sstr << "+";
        for (int i = 0; i < len + 2; ++i) {
            sstr << "-";
        }
    }
    sstr << "+" << std::endl;

    for (unsigned int i = 0; i < config.getK(); ++i) {
        sstr <<
             " | " << std::setw(lens[0]) << i <<
             " | " << std::setw(lens[1]) << feast_results[i].first <<
             " | " << std::setw(lens[2]) << gpu_results[i].first <<
             " | " << std::setw(lens[3]) << std::fixed << feast_results[i].second <<
             " | " << std::setw(lens[4]) << std::fixed << gpu_results[i].second <<
             " | " << std::setw(lens[5]) << std::fixed << std::abs(feast_results[i].second - gpu_results[i].second) <<
             " | " << std::endl;
    }

    std::cout << sstr.str();
}

void
getFEASTAlgorithm(const std::string &method, bool *is_condmi, bool *is_betagamma, std::pair<double, double> *betagamma,
                  uint *(**feast_alg)(uint, uint, uint, uint **, uint *, uint *, double *)) {
    if (method == "MIM" || method == "mim") {
        *feast_alg = FEAST::MIM;
    }
    if (method == "CondMI" || method == "condmi") {
        *is_condmi = true;
    }
    if (method == "JMI" || method == "jmi") {
        *feast_alg = FEAST::JMI;
    }
    if (method == "mRMR" || method == "mrmr") {
        *feast_alg = FEAST::mRMR_D;
    }
    if (method == "CMIM" || method == "cmim") {
        *feast_alg = FEAST::CMIM;
    }
    if (method == "ICAP" || method == "icap") {
        *feast_alg = FEAST::ICAP;
    }
    if (method == "DISR" || method == "disr") {
        *feast_alg = FEAST::DISR;
    }
    if (method.rfind("BetaGamma", 0) == 0 || method.rfind("betagamma", 0) == 0) {
        // Check for the presence of two ':'
        if (std::count(method.begin(), method.end(), ':') != 2) {
            throw std::invalid_argument(
                    "FSAlgorithm: Invalid argument for BetaGamma parameters - format must be 'BetaGamma:<float>:<float>'");
        }

        // Parse beta and gamma
        std::string numbers = method.substr(method.find(':') + 1);
        try {
            betagamma->first = std::stod(numbers.substr(0, numbers.find(':')));
            betagamma->second = std::stod(numbers.substr(numbers.find(':') + 1));
        } catch (std::invalid_argument &e) {
            throw std::invalid_argument(
                    "FSAlgorithm: Invalid argument for BetaGamma parameters - format must be 'BetaGamma:<float>:<float>'");
        }
        *is_betagamma = true;
    }

    if (!(*feast_alg) && !is_condmi && !is_betagamma) {
        throw std::invalid_argument("main: Algorithm '" + method + "' unknown. Must be one of" +
                                    " MIM, CondMI, BetaGammma:<float>:<float>, JMI, mRMR, CMIM, ICAP or DISR.");
    }
}

void runGPUBenchmark(cuFEAST::FSAlgorithm<unsigned int> &algorithm, CliDataset **dataset, ArgParser &arg_parser,
                     CliConfig &config) {

    Results results;
    Profiler profiler;
    double time;

    // keep a copy of the dataset in pageable memory as cache
    // if an execution fails and the CUDA context is reset, the copy in pinned memory will be also wiped
    Dataset<unsigned int> datcpy;
    datcpy.copy(**dataset);

    const std::string dat_name = config.getDatasetPath().substr(config.getDatasetPath().rfind('/') + 1);

    if (config.getVerboseLevel() < CliConfig::MINIMAL) {
        std::cout << "dataset,disc,algorithm,precision,shared_mem,streams,batch,block,time" << std::endl;
    }

    for (const std::string& precision: {std::string("double"), std::string("fixed")})
    for (bool shared_memory: {false, true})
    for (unsigned int num_streams : {0, 1, 2, 4, 8})
    for (unsigned int batch_features : {64, 256, 512, 1024, 4096}) {
        bool bad_config = false;
        for (unsigned int block_size: {64, 128, 256, 512, 1024})
        for (unsigned int r = 0; r < 5; ++r) {

            //size_t fm, tm;
            //getMemInfo(&fm, &tm);
            //std::cout << "MEMORY " << fm / 1024.0/ 1024.0 << " FREE | " << (tm-fm)/ 1024.0/ 1024.0 << " USED | " << tm/ 1024.0/ 1024.0 << " TOTAL" << std::endl;

            config.setSharedMem(shared_memory);
            config.setPrecision(precision);
            config.setNumStreams(num_streams);
            config.setBatchFeatures(batch_features);
            config.setBlockSize(block_size);

            if (bad_config) {
                time = -1.0;
            } else {
                try {
                    profiler.start();

                    algorithm.run(**dataset, results, config.getAlgorithmConfig());

                    time = profiler.stopSeconds();
                } catch (std::runtime_error &e) {
                    // if fails for first time, clear GPU and reload dataset
                    bad_config = true;
                    std::cerr << "ERROR " << e.what() << std::endl;
                    time = -1.0; // TODO maybe should be NaN

                    // delete dataset from pinned memory and reset CUDA context
                    delete *dataset;
                    resetCudaDevice();

                    // create new dataset in pinned memory from cached copy
                    *dataset = new CliDataset();
                    (*dataset)->copy(datcpy);
                }
            }

            if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
                std::cout << "DAT:" << config.getDatasetPath() <<
                          " DSC:" << config.getDiscBins() <<
                          " ALG:" << arg_parser.getOptionArg("algorithm") <<
                          " PRC:" << precision <<
                          " SHR:" << (config.getAlgorithmConfig().useSharedMem() ? "true" : "false") <<
                          " STM:" << config.getAlgorithmConfig().getNumStreams() <<
                          " BAT:" << config.getAlgorithmConfig().getBatchFeatures() <<
                          " BLK:" << config.getAlgorithmConfig().getBlockSize() << std::endl;

                std::cout << "Profile:\n";
                for (const auto &[ms, what]: config.getAlgorithmConfig().getProfileData()) {
                    std::cout << "         " << std::setprecision(6) << std::fixed << ms << "s  " << what << "\n";
                }
                std::cout << "         " << time << "s" << std::endl;
            } else {
                std::cout << dat_name <<
                          "," << config.getDiscBins() <<
                          "," << arg_parser.getOptionArg("algorithm") <<
                          "," << precision <<
                          "," << (config.getAlgorithmConfig().useSharedMem() ? "true" : "false") <<
                          "," << config.getAlgorithmConfig().getNumStreams() <<
                          "," << config.getAlgorithmConfig().getBatchFeatures() <<
                          "," << config.getAlgorithmConfig().getBlockSize() <<
                          "," << time << std::endl;
            }

            // clear result vector and times
            results.clear();
            config.getAlgorithmConfig().clearProfileEntries();
        }
    }
}

void runCPUBenchmark(CliDataset &dataset, ArgParser &arg_parser, CliConfig &config, bool is_condmi, bool is_betagamma,
                     const std::pair<double, double> &betagamma,
                     uint *(*feast_alg)(uint, uint, uint, uint **, uint *, uint *, double *),
                     unsigned int **feature_matrix, unsigned int *u_result_indexes, int *result_indexes,
                     double *result_scores) {
    Results results;
    Profiler profiler;
    double time;

    const std::string dat_name = config.getDatasetPath().substr(config.getDatasetPath().rfind('/') + 1);

    if (config.getVerboseLevel() < CliConfig::MINIMAL) {
        std::cout << "dataset,disc,algorithm,time" << std::endl;
    }

    for (unsigned int r = 0; r < 5; ++r) {
        profiler.start();

        // Run algorithm
        if (is_betagamma) {
            FEAST::BetaGamma(config.getK(), dataset.num_samp, dataset.num_feat, feature_matrix, dataset.classes,
                             u_result_indexes, result_scores, betagamma.first, betagamma.second);
        } else if (is_condmi) {
            FEAST::CondMI(config.getK(), dataset.num_samp, dataset.num_feat, feature_matrix, dataset.classes,
                          result_indexes, result_scores);
        } else {
            feast_alg(config.getK(), dataset.num_samp, dataset.num_feat, feature_matrix, dataset.classes,
                      u_result_indexes, result_scores);
        }

        if (config.doProfile()) {
            // Stop profiling
            time = profiler.stopSeconds();

            if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
                std::cout << "DAT:" << config.getDatasetPath() <<
                          " DSC:" << config.getDiscBins() <<
                          " ALG:" << arg_parser.getOptionArg("algorithm") << std::endl;
                std::cout << "Profile: " << time << "s  CPU (FEAST) execution" << std::endl;
            } else {
                std::cout << dat_name <<
                          "," << config.getDiscBins() <<
                          "," << arg_parser.getOptionArg("algorithm") <<
                          "," << time << std::endl;
            }
        }
    }
}