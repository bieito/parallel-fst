#ifndef CUFEAST_CLIDATASET_CUH
#define CUFEAST_CLIDATASET_CUH

#include <utility>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>

#include "cuFEAST/Dataset.h"
#include "FileParser/FileParser.h"


class CliDataset : public Dataset<unsigned int> {
public:

    CliDataset() {}

    CliDataset(const std::string &file, unsigned int disc_bins);

    ~CliDataset() override;

    void allocate() override;

    inline unsigned int **get2DRawData(unsigned int **dst) const {
        for (unsigned int i = 0; i < this->num_feat; ++i) {
            dst[i] = &this->raw_data[i * this->num_samp];
        }
        return dst;
    }

    explicit operator std::string() const;
};


std::ostream &operator<<(std::ostream &strm, CliDataset const &obj);

#endif //CUFEAST_CLIDATASET_CUH
