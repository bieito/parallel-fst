#ifndef CUFEAST_CLIRESULTS_H
#define CUFEAST_CLIRESULTS_H

// CSV output
inline std::ostream &operator<<(std::ostream &strm, Results const &obj) {
    //strm << "Results: " << obj.size() << " feature" << (obj.size() != 1 ? "s" : "") << std::endl;
    strm << std::setprecision(6) << std::fixed;
    strm << "#feature_index,feature_score" << std::endl;
    for (auto pair : obj) {
        strm << pair.first << "," << pair.second << std::endl;
    }
    return strm;
}

#endif //CUFEAST_CLIRESULTS_H
