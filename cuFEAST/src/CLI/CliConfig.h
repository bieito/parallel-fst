#ifndef CUFEAST_CLICONFIG_H
#define CUFEAST_CLICONFIG_H

#include <cuFEAST/Config.h>
#include <string>

class CliConfig {
public:
    enum VerboseLevel {
        QUIET, MINIMAL, FULL
    };

    enum Implementation {
        CUDA, CPU, FEAST
    };

    CliConfig() {
        algorithm_config = Config();
    }

    ~CliConfig() {
        if (algorithm) {
            delete algorithm;
        }
    };

    unsigned int getK() const {
        if (algorithm) {
            return algorithm->getK();
        }

        throw std::invalid_argument("K value cannot be retrieved until algorithm is set");
    }

    std::string getDatasetPath() const {
        return input_path;
    }

    std::string getResultsPath() const {
        return output_path;
    }

    unsigned int getDiscBins() const {
        return disc_bins;
    }

    Implementation getImplementation() const {
        return implementation;
    }

    bool doValidate() const {
        return do_validate;
    }

    bool doBenchmark() const {
        return do_benchmark;
    }

    bool doProfile() const {
        return algorithm_config.doProfile();
    }

    VerboseLevel getVerboseLevel() const {
        return verbose_level;
    }

    Config &getAlgorithmConfig() {
        return algorithm_config;
    }

    cuFEAST::FSAlgorithm<unsigned int> &getAlgorithm() const {
        return *algorithm;
    }

    void setK(unsigned int k) {
        if (!algorithm)  {
            throw std::invalid_argument("K value cannot be set until algorithm is set");
        }
        algorithm->setK(k);
    }

    void setInputPath(const std::string &input_path) {
        this->input_path = input_path;
    }

    void setOutputPath(const std::string &output_path) {
        this->output_path = output_path;
    }

    void setDoValidate(bool do_validate) {
        this->do_validate = do_validate;
    }

    void setBenchmark(bool benchmark) {
        this->do_benchmark = benchmark;
    }

    void setVerboseLevel(unsigned int verbose_level) {
        switch (verbose_level) {
            case 0:
                this->verbose_level = QUIET;
                break;
            case 1:
                this->verbose_level = MINIMAL;
                break;
            case 2:
                this->verbose_level = FULL;
                break;
            default:
                throw std::invalid_argument("Allowed verbosity levels are: 0, 1, 2");
        }
    }

    void setPrecision(const std::string &precision) {
        Config::Precision version;
        if (precision == "double") {
            version = Config::DOUBLE;
        } else if (precision == "single") {
            version = Config::SINGLE;
        } else if (precision.rfind("fixed", 0) == 0) {
            version = Config::FIXED;
        } else {
            throw std::invalid_argument("Precision must be one of: double, single, fixed");
        }
        algorithm_config.setPrecision(version);
    }

    void setImplementation(const std::string &implementation) {
        if (implementation == "cuda") {
            this->implementation = CUDA;
        //} else if (implementation == "cpu") {
        //    this->implementation = CPU;
        } else if (implementation == "feast") {
            this->implementation = FEAST;
        } else {
            throw std::invalid_argument("Implementation must be one of: cuda, "/*cpu, */"feast");
        }
    }

    void setSharedMem(bool shared_memory) {
        algorithm_config.setSharedMem(shared_memory);
    }

    void setProfile(bool profile) {
        algorithm_config.setProfile(profile);
    }

    void setGpuIndex(int gpuIndex) {
        algorithm_config.setGpuIndex(gpuIndex);
    }

    void setBlockSize(unsigned int block_size) {
        algorithm_config.setBlockSize(block_size);
    }

    void setNumStreams(unsigned int num_streams) {
        algorithm_config.setNumStreams(num_streams);
    }

    void setBatchFeatures(unsigned int batch_features) {
        algorithm_config.setBatchFeatures(batch_features);
    }

    void setDiscBins(unsigned int disc_bins) {
        this->disc_bins = disc_bins;
    }

    void setAlgorithm(const std::string &method, unsigned int k) {
        this->algorithm = cuFEAST::getFSAlgorithm<unsigned int>(method, k);
    }

private:
    std::string input_path;
    std::string output_path;
    unsigned int disc_bins;
    bool no_gpu = false;
    bool do_validate = false;
    bool do_benchmark = false;
    VerboseLevel verbose_level = QUIET;
    Implementation implementation = CUDA;

    Config algorithm_config; // profile
    cuFEAST::FSAlgorithm<unsigned int> *algorithm = nullptr;
};


#endif //CUFEAST_CLICONFIG_H
