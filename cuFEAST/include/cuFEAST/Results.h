#ifndef CUFEAST_RESULTS_H
#define CUFEAST_RESULTS_H

#include <vector>

typedef std::vector<std::pair<int, double>> Results;

#endif //CUFEAST_RESULTS_H
