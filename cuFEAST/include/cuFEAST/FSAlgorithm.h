#ifndef CUFEAST_FSALGORITHM_H
#define CUFEAST_FSALGORITHM_H

#include <algorithm>
#include "Dataset.h"
#include "Results.h"
#include "Config.h"

#define DEFAULT_K 10

namespace cuFEAST {
    template<typename T>
    class FSAlgorithm {
    public:
        explicit FSAlgorithm(unsigned int k = DEFAULT_K) : k(k) {}

        virtual ~FSAlgorithm() = default;

        virtual void run(Dataset<T> const &dataset, Results &results, Config &config) = 0;

        void run(Dataset<T> const &dataset, Results &results) {
            Config default_config;
            this->run(dataset, results, default_config);
        }

        void setK(unsigned int k) {
            this->k = k;
        }

        unsigned int getK() const {
            return k;
        }

    private:
        unsigned int k = DEFAULT_K;
    };

    template<typename T>
    class MIM : public FSAlgorithm<T> {
    public:
        explicit MIM(unsigned int k = DEFAULT_K) : FSAlgorithm<T>(k) {}

        ~MIM() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset, Results &results, Config &config) override;
    };

    template<typename T>
    class CondMI : public FSAlgorithm<T> {
    public:
        explicit CondMI(unsigned int k = DEFAULT_K) : FSAlgorithm<T>(k) {}

        ~CondMI() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset[[maybe_unused]], Results &results[[maybe_unused]], Config &config[[maybe_unused]]) override {

        }
    };

    template<typename T>
    class BetaGamma : public FSAlgorithm<T> {
    public:
        explicit BetaGamma(unsigned int k = DEFAULT_K, double beta = 1.0, double gamma = 1.0) : FSAlgorithm<T>(k),
                                                                                                beta(beta),
                                                                                                gamma(gamma) {}

        ~BetaGamma() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset[[maybe_unused]], Results &results[[maybe_unused]], Config &config[[maybe_unused]]) override {

        }

    private:
        double beta = 1.0;
        double gamma = 1.0;
    };

    template<typename T>
    class JMI : public FSAlgorithm<T> {
    public:
        explicit JMI(unsigned int k = DEFAULT_K) : FSAlgorithm<T>(k) {}

        ~JMI() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset, Results &results, Config &config) override;
    };

    template<typename T>
    class mRMR : public FSAlgorithm<T> {
    public:
        explicit mRMR(unsigned int k = DEFAULT_K) : FSAlgorithm<T>(k) {}

        ~mRMR() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset, Results &results, Config &config) override;
    };

    template<typename T>
    class CMIM : public FSAlgorithm<T> {
    public:
        explicit CMIM(unsigned int k = DEFAULT_K) : FSAlgorithm<T>(k) {}

        ~CMIM() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset[[maybe_unused]], Results &results[[maybe_unused]], Config &config[[maybe_unused]]) override {

        }
    };

    template<typename T>
    class ICAP : public FSAlgorithm<T> {
    public:
        explicit ICAP(unsigned int k = DEFAULT_K) : FSAlgorithm<T>(k) {}

        ~ICAP() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset[[maybe_unused]], Results &results[[maybe_unused]], Config &config[[maybe_unused]]) override {

        }
    };

    template<typename T>
    class DISR : public FSAlgorithm<T> {
    public:
        explicit DISR(unsigned int k = DEFAULT_K) : FSAlgorithm<T>(k) {}

        ~DISR() override = default;

        using FSAlgorithm<T>::run;

        void run(Dataset<T> const &dataset, Results &results, Config &config) override;
    };

    template<typename T>
    FSAlgorithm<T> *getFSAlgorithm(const std::string &method, unsigned int k = DEFAULT_K) {
        if (method == "MIM" || method == "mim") {
            return new MIM<T>(k);
        }
        if (method == "CondMI" || method == "condmi") {
            return new CondMI<T>(k);
        }
        if (method.rfind("BetaGamma", 0) == 0 || method.rfind("betagamma", 0) == 0) {
            double beta, gamma;

            // Check for the presence of two ':'
            if (std::count(method.begin(), method.end(), ':') != 2) {
                throw std::invalid_argument(
                        "FSAlgorithm: Invalid argument for BetaGamma parameters - format must be 'BetaGamma:<float>:<float>'");
            }

            // Parse beta and gamma
            std::string numbers = method.substr(method.find(':') + 1);
            try {
                beta = std::stod(numbers.substr(0, numbers.find(':')));
                gamma = std::stod(numbers.substr(numbers.find(':') + 1));
            } catch (std::invalid_argument &e) {
                throw std::invalid_argument(
                        "FSAlgorithm: Invalid argument for BetaGamma parameters - format must be 'BetaGamma:<float>:<float>'");
            }
            return new BetaGamma<T>(k, beta, gamma);
        }
        if (method == "JMI" || method == "jmi") {
            return new JMI<T>(k);
        }
        if (method == "mRMR" || method == "mrmr") {
            return new mRMR<T>(k);
        }
        if (method == "CMIM" || method == "cmim") {
            return new CMIM<T>(k);
        }
        if (method == "ICAP" || method == "icap") {
            return new ICAP<T>(k);
        }
        if (method == "DISR" || method == "disr") {
            return new DISR<T>(k);
        }

        throw std::invalid_argument("CliConfig: Algorithm '" + method + "' unknown. Must be one of" +
                                    " MIM, CondMI, BetaGammma:<float>:<float>, JMI, mRMR, CMIM, ICAP or DISR.");
    }

    template class MIM<unsigned int>;
    template class CondMI<unsigned int>;
    template class BetaGamma<unsigned int>;
    template class JMI<unsigned int>;
    template class mRMR<unsigned int>;
    template class CMIM<unsigned int>;
    template class ICAP<unsigned int>;
    template class DISR<unsigned int>;

}

#endif //CUFEAST_FSALGORITHM_H
