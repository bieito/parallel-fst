#!/bin/bash

#SBATCH -J cuda-mrmr
#SBATCH -o /home/bieito/TFM/output/%x.%j
#SBATCH --exclusive
#SBATCH -t 1:00:00
#SBATCH --mem 60G

DATASET_DIR=/home/bieito.beceiro/STORE/datasets
RESULTS_DIR=/home/bieito.beceiro/STORE/results

BREAST=${DATASET_DIR}/Breast.arff
ECML={DATASET_DIR}/ECML.arff
EPSILON={DATASET_DIR}/epsilon.libsvm
RCV1={DATASET_DIR}/rcv1_train.libsvm
NEWS20={DATASET_DIR}/news20.libsvm
SVHN={DATASET_DIR}/SVHN.libsvm
E2006={DATASET_DIR}/E2006.libsvm

DATASETS="${BREAST} ${ECML}" # ${EPSILON} ${RCV1} ${NEWS20} ${SVHN}"
SELECT="20"
DISC="256"
STREAMS="1 2 4 8"
BLK="64 128 256"


for DAT in "${DATASETS}"
do
    CMD_BASE="mrmr-cli -i ${DAT} -n ${SELECT} --profile"

    # CPU
    CMD="${CMD_BASE} --no-gpu -v 2"
    echo "$CMD"
    #eval "$CMD"
    
    # GPU
    for BLK in "${BLKS}"
    do
        for NSTR in "${STREAMS}"
        do
            CMD="${CMD_BASE} --tpb ${BLK} --streams ${NSTR}"
            echo "$CMD"
            #eval "$CMD"
        done    
    done
    echo
done
