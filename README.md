# Parallel-FST: Feature Selection Library for Clusters
Parallel Feature Selection toolbox containing several HPC implementations of FS methods.

Based on [FEAST](https://github.com/Craigacp/FEAST) [G. Brown, A. Pocock, M.-J. Zhao and M. Luján, 2012].

At the moment, Parallel-FST contains three implementations:
 * **CLI-FEAST** a CLI wrapper for the original sequential methods of FEAST
 * **MPI-FEAST** a hybrid MPI/multithreaded implementation for multicore clusters with CLI
 * **cuFEAST** a CUDA implementation for single-GPU nodes, as a CUDA library and also with a CLI (**Implemented**: MIM, mRMR, JMI and DISR. **Working in**: BetaGamma, ICAP, CMIM, CondMI)

Experimental results showed speedups up to x229 for MPI-FEAST in a cluster with 16 nodes and 16 cores each and up to x101 for cuFEAST with a NVIDIA T4 GPU.

### Contents
[[_TOC_]]

## Specific versions
This project is part of an academic research work which is now composed of more than one subproject.
Since it is feasible that more subprojects are added, or that the structure of the code changes over time, it is important to identify the specific versions of the software that have been used to obtain results presented in publications in order to maintain experiment repeatability.

To achieve this, we have decided to give a tag to each of these specific versions, which are listed below:

### [jpdc21](https://gitlab.com/bieito/parallel-fst/-/tree/jpdc21)
Commit: f5600d13
> [1] B. Beceiro, J. González-Domínguez and J. Touriño, "Parallel-FST: a Feature Selection Library for Multicore Clusters", *Journal of Parallel and Distributed Computing*, vol. 169, pp. 106-116, Jul. 2022. [Online]. Available: [doi.org/10.1016/j.jpdc.2022.06.012](https://doi.org/10.1016/j.jpdc.2022.06.012)

## Requirements
WIP.

Versions used in development are between brackets.

General requirements:
* [CMake](https://cmake.org/) [3.22.0]
* C/C++ Compiler with support for OpenMP [[GNU GCC](https://gcc.gnu.org/) 11.1.0]

cuFEAST (CUDA) requirements:
* CUDA Compiler (to build) and Runtime (to execute) [[NVIDIA CUDA](https://developer.nvidia.com/cuda-zone) 11.5.50]

MPI-FEAST (MPI/Threads) requirements:
* MPI Compiler and Libraries [[OpenMPI](https://www.open-mpi.org/) 4.1.2]


## Building
WIP.

Scripts are provided for an easier configuration and building.
```shell
$ scripts/configure [-DCMAKE_INSTALL_PREFIX=<path>] [-DWITH_CUDA=OFF] [-DWITH_MPI=OFF]
$ scripts/build
$ [sudo] scripts/install  # root privileges are needed if installing to system root
```

As said, Parallel-FST contains implementations for several architectures, so not everybody might be interested in all of them. They are all enabled by default, but can be disabled at configuration time adding `-DWITH_CUDA=OFF` and/or `-DWITH_MPI=OFF`.


## Usage
This project ships a shell executable as an entrypoint to the desired implementation. So usage is as simple as launching `parallel-fst` followed by the flag of the implementation:
```shell
$ parallel-fst --feast ARGS
$ parallel-fst --cuda  ARGS  # if CUDA was disabled at configuration, this will fail
$ parallel-fst --mpi   ARGS  # if MPI was disabled at configuration, this will fail

# To show more info about available options:
$ parallel-fst --help 
```
Furthermore, ARGS have been made consistent among all CLIs, so it is easy to switch among versions. However, some implementations have additional architecture-specific options for further configurations that might impact performance.

### Common ARGS
* `-i, --input <path>   ` Input file to read dataset from
* `-o, --output <path>  ` Output file to write results to
* `-w, --weights <path> ` Input file to read weights from (Not yet available for CUDA)
* `-a, --algorithm <ALG>` Algorithm to run (MIM, CondMI, BetaGammma, JMI, mRMR, CMIM, ICAP, DISR) (Not yet available for CUDA)
* `-k, --select <int>   ` Number of features to select
* `-d, --disc <n_bins>  ` Discretize the input dataset with the binning technique using the specified number of bins
* `-c, --rangecomp      ` Apply a range compression before running the algorithm (Not yet available for CUDA)
* `-h, --help           ` Show extended list of acepted ARGS

### Some cuFEAST specific ARGS
* `-p, --precision <double|single|fixed>` Precision of the datatype used for the computation of scores
* `-b, --block-size <int>` Number of threads per block for CUDA kernels
* `-s, --streams <int>   ` Number of streams to use to overlap memory transfers and kernels
* `    --batch <int>     ` Approximate number of features to send in each batch of each stream
* `    --no-shmem        ` Disable usage of shared memory (when possible) in GPU
* `    --list-gpu        ` List CUDA enabled available GPUs
* `    --gpu             ` Select index of GPU to use
* `    --benchmark       ` Run 2500 executions to test out combinations of distinct args (precision, shared memory, streams, batch and block size | 500 combinations * 5 repetitions each)

### Some MPI-FEAST specific ARGS
* `-n, --processes <int>` Number of MPI processes that will be launched
* `-t, --threads <int>  ` Number of threads each MPI process will launch